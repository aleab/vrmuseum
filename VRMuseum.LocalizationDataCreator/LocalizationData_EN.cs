﻿using System.Diagnostics.CodeAnalysis;
using VRMuseum.Collections;
using VRMuseum.Localization;

namespace VRMuseum.LocalizationDataCreator
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    internal class LocalizationData_EN
    {
        public static LocalizationData CreateLocalizationData()
        {
            LocalizationData data = new LocalizationData
            {
                Data = new SerializableDictionary<LocalizationStringID, string>
                {
                    { LocalizationStringID.EntranceInfoTitle, "Degas’ Virtual Gallery" },
                    { LocalizationStringID.EntranceInfoBody, @"This project aims to offer you the
possibility of exploring a selection
of Degas’ works that are conserved
at <b>Metropolitan Museum of Art</b>.
These canvases have been selected
by picking up three points of view
that Degas stressed in his painting
dedicated to <b>Paris’ society</b>;
three topics,
<color=#CADABA>Public Life</color>, <color=#FFE4C4>Ballerinas</color>, <color=#FFC0CB>A Male Gaze</color>,
representative of a modernity
in which public appearance
and private gaze were often
two faces of the same ambiguous coin.

<b>Live the space and explore it.
Use the trackpad on your controller
to teleport around the room.
<size=+6>Enjoy!</size></b>" },

                    { LocalizationStringID.ExitInfoTitle, "Credits" },
                    { LocalizationStringID.ExitInfoBody, @"This project has been developed by
<b>Alessandro Attard Barbini</b> and
<b>Leonardo Nucciarelli</b>
at <i>COMLAB</i>, in the engineering
department of Roma Tre University.

The high resolution images
are powered by <i>MET</i> <size=-6>(www.metmuseum.org)</size>
with Public Domain Dedication
and are used only for didactic purpose.
<size=-6>(https://creativecommons.org/publicdomain/zero/1.0)</size>" },
                }
            };

            return data;
        }
    }
}