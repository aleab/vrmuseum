﻿using System.Diagnostics;
using System.IO;
using System.Reflection;
using VRMuseum.Localization;

namespace VRMuseum.LocalizationDataCreator
{
    internal class Program
    {
        private static void Main()
        {
            var currentDirectoryPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            var serializer = new LocalizationSerializer();

            LocalizationData[] data =
            {
                LocalizationData_EN.CreateLocalizationData()
            };

            foreach (var d in data)
                serializer.Serialize(d, currentDirectoryPath);

            // Open the directory
            if (currentDirectoryPath != null)
                Process.Start(currentDirectoryPath);
        }
    }
}