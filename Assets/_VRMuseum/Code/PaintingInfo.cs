﻿using System;
using System.Collections.Generic;

namespace VRMuseum
{
    /// <summary>
    /// Holds information about a painting in the MET Museum website.
    /// See: http://www.metmuseum.org/art/collection/search/436158.
    /// </summary>
    [Serializable]
    public class PaintingInfo
    {
        private readonly Dictionary<string, string> data;

        public int Id { get; private set; }

        public string Title
        {
            get { return this.data["Title"]; }
            set { this.data["Title"] = value; }
        }

        public string Artist
        {
            get { return this.data["Artist"]; }
            set { this.data["Artist"] = value; }
        }

        public string Date
        {
            get { return this.data["Date"]; }
            set { this.data["Date"] = value; }
        }

        public string Medium
        {
            get { return this.data["Medium"]; }
            set { this.data["Medium"] = value; }
        }

        public string Dimensions
        {
            get { return this.data["Dimensions"]; }
            set { this.data["Dimensions"] = value; }
        }

        public string Classification
        {
            get { return this.data["Classification"]; }
            set { this.data["Classification"] = value; }
        }

        public string CreditLine
        {
            get { return this.data["CreditLine"]; }
            set { this.data["CreditLine"] = value; }
        }

        public string AccessionNumber
        {
            get { return this.data["AccessionNumber"]; }
            set { this.data["AccessionNumber"] = value; }
        }

        public string Description
        {
            get { return this.data["Description"]; }
            set { this.data["Description"] = value; }
        }

        public PaintingInfo()
        {
        }

        public PaintingInfo(int id)
        {
            this.Id = id;
            this.data = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                { "Title", null },
                { "Artist", null },
                { "Date", null },
                { "Medium", null },
                { "Dimensions", null },
                { "Classification", null },
                { "CreditLine", null },
                { "AccessionNumber", null },
                { "Description", null }
            };
        }

        public void Validate(string label, string value)
        {
            label = label.ToLower().Trim().Replace(" ", "");

            foreach (var item in this.data)
            {
                string key = item.Key.ToLower();
                if (label.StartsWith(key))
                {
                    this.data[key] = value;
                    return;
                }
            }

            throw new PaintingInfoException(string.Format("PaintingInfoException: could not find element named \"{0}\".", label));
        }
    }

    public class PaintingInfoException : Exception
    {
        public PaintingInfoException()
        {
        }

        public PaintingInfoException(string message) : base(message)
        {
        }
    }
}