﻿using InteractionTypeFlags = VRMuseum.InteractableObject.InteractionTypeFlags;
using UseTypeFlags = VRMuseum.InteractableObject.UseTypeFlags;

namespace VRMuseum
{
    /// <summary>
    /// Defines the state of an <see cref="InteractableObject"/>, i.e. if it is currently gazed, grabbed, used...
    /// </summary>
    public class InteractableObjectState
    {
        private InteractionTypeFlags _interactionType;
        private UseTypeFlags _useType;

        public bool Gazed { get; set; }
        public bool Grabbed { get; private set; }
        public bool Used { get; private set; }

        public InteractionTypeFlags InteractionType
        {
            get
            {
                return this._interactionType;
            }
            private set
            {
                this._interactionType = value;

                this.Grabbed = (this.InteractionType & InteractionTypeFlags.Grab) != 0;
                this.Used = (this.InteractionType & InteractionTypeFlags.Use) != 0;
            }
        }

        public UseTypeFlags UseType
        {
            get
            {
                return this._useType;
            }
            private set
            {
                this._useType = value;

                this.Used = this.UseType != UseTypeFlags.Nothing;
            }
        }

        public InteractableObjectState()
        {
            this.Gazed = false;
            this.SetInteractionFlags(0u);
        }

        public void SetInteractionFlags(uint interactionFlags)
        {
            this.InteractionType = (InteractionTypeFlags)(interactionFlags & (uint)InteractionTypeFlags.Everything);
            this.UseType = (UseTypeFlags)(interactionFlags & (uint)UseTypeFlags.Everything);
        }
    }
}