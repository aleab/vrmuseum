﻿using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

namespace VRMuseum
{
    /// <summary>
    /// This class is used to serialize/deserialize the backup/fallback paintings' data,
    /// which was previously downloaded from the net (i.e. the MET Museum website).
    /// </summary>
    internal class PaintingInfoSerializer
    {
        protected XmlSerializer serializer = new XmlSerializer(typeof(PaintingInfo), new XmlRootAttribute("PaintingInfo"));

        public virtual void Serialize(PaintingInfo data, string path)
        {
            // TODO: Handle exceptions.

            string xmlFilePath = Path.Combine(path, GetPaintingInfoFilePath(data.Id));
            var dirPath = Path.GetDirectoryName(xmlFilePath);
            if (!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);

            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings()
            {
                Encoding = new UTF8Encoding(false),
                Indent = true
            };
            using (var writer = XmlWriter.Create(xmlFilePath, xmlWriterSettings))
            {
                this.serializer.Serialize(writer, data);
                Debug.LogFormat("Painting #{0}: data serialized to <i>{1}</i>.", data.Id, xmlFilePath);
            }
        }

        public virtual PaintingInfo Deserialize(int id)
        {
            // TODO: Handle exceptions.

            PaintingInfo paintingInfo = null;
            using (var stream = Resources.LoadBinaryResource(GetPaintingInfoFilePath(id)))
                paintingInfo = (PaintingInfo)this.serializer.Deserialize(stream);
            return paintingInfo;
        }

        private static string GetPaintingInfoFilePath(int id)
        {
            // xml files are saved in the 'paintings_info' folder.
            return string.Format("paintings_info/{0}.xml", id);
        }
    }
}