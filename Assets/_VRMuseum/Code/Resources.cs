﻿using System;
using System.IO;
using UnityEngine;

namespace VRMuseum
{
    public static class Resources
    {
        /// <summary>
        /// Load a Unity Resource as a binary stream.
        /// </summary>
        /// <param name="fileName"> The name of the resource file. </param>
        /// <returns> A <see cref="MemoryStream"/>. </returns>
        public static MemoryStream LoadBinaryResource(string fileName)
        {
            string fileNameWithoutExtension = string.Format("{0}/{1}", Path.GetDirectoryName(fileName), Path.GetFileNameWithoutExtension(fileName));
            try
            {
                TextAsset textAsset = UnityEngine.Resources.Load(fileNameWithoutExtension) as TextAsset;
                var stream = new MemoryStream(textAsset.bytes);
                return stream;
            }
            catch (NullReferenceException)
            {
                if (fileName == null)
                    throw;
                throw new FileNotFoundException(string.Format("The specified file (\"{0}\") does not exist or is not contained in a \"Resources\" folder.", fileName), fileName);
            }
        }
    }
}