﻿using UnityEngine;

namespace VRMuseum.Extensions
{
    public static class GameObjectExtensions
    {
        /// <summary>
        /// Removes every component from this GameObject but its Transform.
        /// </summary>
        /// <param name="gameObject"> This GameObject. </param>
        /// <param name="destroyImmediate"> Whether the components should be destroyed immediately (even if in the Editor). </param>
        public static void RemoveComponents(this GameObject gameObject, bool destroyImmediate = false)
        {
            var components = gameObject.GetComponents<Component>();
            foreach (var component in components)
            {
                if (component is Transform)
                    continue;

                if (destroyImmediate)
                    Object.DestroyImmediate(component);
                else
                    Object.Destroy(component);
            }
        }
    }
}