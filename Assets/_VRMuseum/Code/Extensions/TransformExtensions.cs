﻿using UnityEngine;

namespace VRMuseum.Extensions
{
    public enum TransformScaleComponent { X, Y, Z }

    public static class TransformExtensions
    {
        /// <summary>
        /// Sets the global scale (lossyScale) of this Transform.
        /// </summary>
        /// <param name="transform"> This Trasform. </param>
        /// <param name="scale"> The scale. </param>
        public static void SetGlobalScale(this Transform transform, Vector3 scale)
        {
            var parent = transform.parent;
            transform.parent = null;
            transform.localScale = scale;
            transform.parent = parent;
        }

        /// <summary>
        /// Modifies a component of the global scale (lossyScale).
        /// </summary>
        /// <param name="transform"> This Tranform. </param>
        /// <param name="component"> The component to modify (x, y or z). </param>
        /// <param name="scale"> The scale. </param>
        public static void SetGlobalScaleComponent(this Transform transform, TransformScaleComponent component, float scale)
        {
            Vector3 scaleVector = new Vector3(component == TransformScaleComponent.X ? scale : transform.lossyScale.x,
                                              component == TransformScaleComponent.Y ? scale : transform.lossyScale.y,
                                              component == TransformScaleComponent.Z ? scale : transform.lossyScale.z);
            transform.SetGlobalScale(scaleVector);
        }

        /// <summary>
        /// Sets the global scale (lossyScale) of this Transform.
        /// </summary>
        /// <param name="transform"> This transform. </param>
        /// <param name="scale"> The value to assign to every component of the scale vector. </param>
        public static void SetUniformGlobalScale(this Transform transform, float scale)
        {
            transform.SetGlobalScale(new Vector3(scale, scale, scale));
        }
    }
}