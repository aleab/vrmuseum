﻿using System;
using System.Collections;
using UnityEngine;

namespace VRMuseum.Extensions
{
    public static class MonoBehaviourExtensions
    {
        /// <summary>
        /// Invokes the method 'action' after 'time' seconds.
        /// </summary>
        /// <param name="monoBehaviour"> This MonoBehaviour. </param>
        /// <param name="action"> The void-returning function to execute. </param>
        /// <param name="time"> The delay, in seconds. </param>
        public static void Invoke(this MonoBehaviour monoBehaviour, Action action, float time)
        {
            monoBehaviour.Invoke(action.Method.Name, time);
        }

        /// <summary>
        /// Invokes the method 'action' with parameter 'param' of type T after 'time' seconds.
        /// </summary>
        /// <typeparam name="T"> The type of the parameter. </typeparam>
        /// <param name="monoBehaviour"> This MonoBehaviour. </param>
        /// <param name="action"> The void-returning function to execute. </param>
        /// <param name="param"> The parameter of the method. </param>
        /// <param name="time"> The delay, in seconds. </param>
        /// <returns> The started <see cref="Coroutine"/>. </returns>
        public static Coroutine Invoke<T>(this MonoBehaviour monoBehaviour, Action<T> action, T param, float time)
        {
            return monoBehaviour.StartCoroutine(TemplateCoroutine(action, param, time));
        }

        /// <summary> Invokes the method 'action' after 'time' seconds, then repeatedly every 'repeatRate' seconds. </summary>
        /// <param name="monoBehaviour"> This MonoBehaviour. </param>
        /// <param name="action"> The void-returning function to execute. </param>
        /// <param name="time"> The delay for the first execution, in seconds. </param>
        /// <param name="timeRate"> Frequency of subsequent executions, in seconds. </param>
        public static void InvokeRepeating(this MonoBehaviour monoBehaviour, Action action, float time, float repeatRate)
        {
            monoBehaviour.InvokeRepeating(action.Method.Name, time, repeatRate);
        }

        /// <summary> Cancels all the Invoke* calls of the specified method. </summary>
        /// <param name="monoBehaviour"> This MonoBehaviour. </param>
        /// <param name="action"> The void-returning function to cancel. </param>
        public static void CancelInvoke(this MonoBehaviour monoBehaviour, Action action)
        {
            monoBehaviour.CancelInvoke(action.Method.Name);
        }

        private static IEnumerator TemplateCoroutine<T>(Action<T> action, T param, float time)
        {
            yield return new WaitForSeconds(time);
            action(param);
        }
    }
}