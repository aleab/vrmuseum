﻿using System;

namespace VRMuseum
{
    public static class Util
    {
        /// <summary> Returns the number of elements defined in the enumerator T. </summary>
        /// <typeparam name="T"> The enum type. </typeparam>
        /// <returns> The number of elements defined in the enum; if T is not an enum, returns -1. </returns>
        public static int GetEnumLength<T>()
        {
            Type enumType = typeof(T);
            return enumType.IsEnum ? Enum.GetNames(enumType).Length : -1;
        }

        /// <summary> Returns an array of type T containing the values of the enumerator T. </summary>
        /// <typeparam name="T"> The enum type. </typeparam>
        /// <returns> Array containing the enum's values or null if T is not an enum. </returns>
        public static T[] GetEnumValues<T>()
        {
            Type enumType = typeof(T);
            return enumType.IsEnum ? (T[])Enum.GetValues(enumType) : null;
        }
    }
}