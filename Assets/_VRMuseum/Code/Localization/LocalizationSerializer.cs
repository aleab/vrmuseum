﻿using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace VRMuseum.Localization
{
    public class LocalizationSerializer
    {
        protected XmlSerializer serializer = new XmlSerializer(typeof(LocalizationData), new XmlRootAttribute("LocalizationData"));

        public virtual void Serialize(LocalizationData data, string path)
        {
            // TODO: Handle exceptions.

            string xmlFilePath = Path.Combine(path, GetLocalizationFilePath(data.Language));
            var dirPath = Path.GetDirectoryName(xmlFilePath);
            if (!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);

            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings()
            {
                Encoding = new UTF8Encoding(false),
                Indent = true
            };
            using (var writer = XmlWriter.Create(xmlFilePath, xmlWriterSettings))
                this.serializer.Serialize(writer, data);
        }

        public virtual LocalizationData Deserialize(LocalizationLanguage languageCode)
        {
            // TODO: Handle exceptions.

            LocalizationData localizationData = null;
            try
            {
                using (var stream = Resources.LoadBinaryResource(GetLocalizationFilePath(languageCode)))
                    localizationData = (LocalizationData)this.serializer.Deserialize(stream);
            }
            catch (FileNotFoundException)
            {
                if (languageCode != LocalizationLanguage.EN)
                {
                    using (var stream = Resources.LoadBinaryResource(GetLocalizationFilePath(LocalizationLanguage.EN)))
                        localizationData = (LocalizationData)this.serializer.Deserialize(stream);
                }
            }
            return localizationData;
        }

        private static string GetLocalizationFilePath(LocalizationLanguage languageCode)
        {
            return string.Format("locales/{0}.xml", languageCode.ToString().ToLower());
        }
    }
}