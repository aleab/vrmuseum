﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace VRMuseum.Localization
{
    /// <summary>
    /// Attach this component to a GameObject if it is going to display some Text that needs to support localization.
    /// </summary>
    [RequireComponent(typeof(Text))]
    public class LocalizedText : MonoBehaviour
    {
#pragma warning disable 0649

        /// <summary>
        /// The id of the localized text.
        /// </summary>
        [SerializeField]
        private LocalizationStringID id;

#pragma warning restore 0649

        private void Start()
        {
            this.StartCoroutine(this.LoadLocalizedText());
        }

        private IEnumerator LoadLocalizedText()
        {
            while (!LocalizationManager.IsReady)
                yield return new WaitForSeconds(0.1f);

            this.gameObject.GetComponent<Text>().text = LocalizationManager.LocalizationData.GetValue(this.id);
        }
    }
}