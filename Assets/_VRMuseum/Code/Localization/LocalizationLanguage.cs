﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace VRMuseum.Localization
{
    public enum LocalizationLanguage
    {
        EN,
        IT,
    }

    public static class LocalizationLanguageExtensions
    {
        private static Dictionary<SystemLanguage, LocalizationLanguage> dictionary = new Dictionary<SystemLanguage, LocalizationLanguage>()
        {
            { SystemLanguage.English, LocalizationLanguage.EN },
            { SystemLanguage.Italian, LocalizationLanguage.IT },

            { SystemLanguage.Unknown, LocalizationLanguage.EN }
        };

        public static LocalizationLanguage ToLocalizationLanguage(this SystemLanguage sysLang)
        {
            if (dictionary.ContainsKey(sysLang))
                return dictionary[sysLang];
            return LocalizationLanguage.EN;
        }

        public static SystemLanguage ToSystemLanguage(this LocalizationLanguage locLang)
        {
            return dictionary.ContainsValue(locLang) ? dictionary.FirstOrDefault(kvp => kvp.Value == locLang).Key : SystemLanguage.Unknown;
        }
    }
}