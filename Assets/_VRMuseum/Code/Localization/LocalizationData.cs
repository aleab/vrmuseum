﻿using System;
using VRMuseum.Collections;

namespace VRMuseum.Localization
{
    [Serializable]
    public class LocalizationData
    {
        public LocalizationLanguage Language { get; set; }

        public SerializableDictionary<LocalizationStringID, string> Data { get; set; }

        public LocalizationData()
        {
            this.Data = new SerializableDictionary<LocalizationStringID, string>();
        }

        public LocalizationData(LocalizationLanguage language) : this()
        {
            this.Language = language;
        }

        public string GetValue(LocalizationStringID key)
        {
            return this.Data.ContainsKey(key) ? this.Data[key] : null;
        }
    }
}