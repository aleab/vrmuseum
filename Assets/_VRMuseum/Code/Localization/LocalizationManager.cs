﻿using UnityEngine;

namespace VRMuseum.Localization
{
    public class LocalizationManager : MonoBehaviour
    {
        private static LocalizationManager instance;

        public static LocalizationData LocalizationData { get; protected set; }

        public static bool IsReady { get; private set; }

        private LocalizationSerializer serializer;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                instance.serializer = new LocalizationSerializer();
            }
            else if (instance != this)
                Destroy(this.gameObject);

            //DontDestroyOnLoad(this.gameObject);
        }

        private void Start()
        {
            SystemLanguage systemLanguage = Application.systemLanguage;
            LocalizationLanguage localizationLanguage = systemLanguage.ToLocalizationLanguage();
            LocalizationData = this.serializer.Deserialize(localizationLanguage);
            IsReady = true;
            
            Debug.LogFormat("[LocalizationManager]\nSystemLanguage: {0}; LocalizationLanguage: {1}, {2} entries.", systemLanguage, LocalizationData.Language.ToSystemLanguage(), LocalizationData.Data.Count);
        }
    }
}