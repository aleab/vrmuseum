﻿namespace VRMuseum.Localization
{
    public enum LocalizationStringID
    {
        EntranceInfoTitle,
        EntranceInfoBody,
        ExitInfoTitle,
        ExitInfoBody,
    }
}