﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace VRMuseum.Collections
{
    /// <summary>
    /// Represents an XML serializable collection of keys and values.
    /// </summary>
    /// <typeparam name="TKey"> The type of the keys in the dictionary. </typeparam>
    /// <typeparam name="TValue"> The type of the values in the dictionary. </typeparam>
    /// <remarks> http://stackoverflow.com/questions/3671259/how-to-xml-serialize-a-dictionary </remarks>
    [Serializable]
    [XmlRoot("Dictionary")]
    public class SerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, IXmlSerializable
    {
        /// <summary>
        /// The default XML tag name for an item.
        /// </summary>
        private const string DEFAULT_ITEM_TAG = "Item";

        /// <summary>
        /// The default XML tag name for a key.
        /// </summary>
        private const string DEFAULT_KEY_TAG = "key";

        /// <summary>
        /// The default XML tag name for a value.
        /// </summary>
        private const string DEFAULT_VALUE_TAG = "value";

        /// <summary>
        /// The XML serializer for the key type.
        /// </summary>
        private static readonly XmlSerializer keySerializer;

        /// <summary>
        /// The XML serializer for the value type.
        /// </summary>
        private static readonly XmlSerializer valueSerializer;

        static SerializableDictionary()
        {
            keySerializer = new XmlSerializer(typeof(TKey), new XmlRootAttribute(DEFAULT_KEY_TAG));
            valueSerializer = new XmlSerializer(typeof(TValue), new XmlRootAttribute(DEFAULT_VALUE_TAG));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SerializableDictionary{TKey, TValue}"/>
        /// </summary>
        public SerializableDictionary() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SerializableDictionary{TKey, TValue}"/> class.
        /// </summary>
        /// <param name="info">
        ///     A <see cref="SerializationInfo"/> object containing the information required to serialize
        ///     the <see cref="Dictionary{TKey, TValue}"/>.
        /// </param>
        /// <param name="context">
        ///     A <see cref="StreamingContext"/> structure containing the source and destination of the serialized
        ///     stream associated with the <see cref="Dictionary{TKey, TValue}"/>.
        /// </param>
        protected SerializableDictionary(SerializationInfo info, StreamingContext context) : base(info, context) { }

        /// <summary>
        /// Gets the XML schema for the XML serialization.
        /// </summary>
        /// <returns>An XML schema for the serialized object.</returns>
        public XmlSchema GetSchema()
        {
            return null;
        }

        /// <summary>
        /// Deserializes the object from XML.
        /// </summary>
        /// <param name="reader"> The XML representation of the object. </param>
        public void ReadXml(XmlReader reader)
        {
            reader.Read();
            if (reader.IsEmptyElement && reader.AttributeCount == 0)
                return;

            try
            {
                while (reader.NodeType != XmlNodeType.EndElement)
                {
                    this.ReadItem(reader);
                    reader.MoveToContent();
                }
            }
            finally
            {
                if (!reader.IsEmptyElement)
                    reader.ReadEndElement();
                else
                    reader.Skip();
            }
        }

        /// <summary>
        /// Serializes this instance to XML.
        /// </summary>
        /// <param name="writer"> The XML writer to serialize to. </param>
        public void WriteXml(XmlWriter writer)
        {
            foreach (var keyValuePair in this)
                this.WriteItem(writer, keyValuePair);
        }

        /// <summary>
        /// Deserializes the dictionary item.
        /// </summary>
        /// <param name="reader"> The XML representation of the object. </param>
        private void ReadItem(XmlReader reader)
        {
            try
            {
                TKey key = default(TKey);
                TValue value = default(TValue);
                if (reader.IsStartElement(DEFAULT_ITEM_TAG))
                {
                    // If both TKey and TValue are primitive types, enums or strings, then they have been serialized as attributes.
                    // Otherwise, they are both elements.
                    bool isKeySerializableAsAttribute = typeof(TKey).IsPrimitive || typeof(TKey).IsEnum || typeof(TKey) == typeof(string);
                    bool isValueSerializableAsAttribute = typeof(TValue).IsPrimitive || typeof(TValue).IsEnum || typeof(TValue) == typeof(string);

                    if (isKeySerializableAsAttribute && isValueSerializableAsAttribute)
                    {
                        reader.MoveToAttribute(DEFAULT_KEY_TAG);
                        key = (TKey)(typeof(TKey).IsEnum ? Enum.Parse(typeof(TKey), reader.Value) : Convert.ChangeType(reader.Value, typeof(TKey)));
                        reader.MoveToElement();

                        reader.MoveToAttribute(DEFAULT_VALUE_TAG);
                        value = (TValue)(typeof(TValue).IsEnum ? Enum.Parse(typeof(TValue), reader.Value) : Convert.ChangeType(reader.Value, typeof(TValue)));
                        reader.MoveToElement();
                    }
                    else
                    {
                        reader.Read();
                        key = (TKey)keySerializer.Deserialize(reader);

                        reader.Read();
                        value = (TValue)valueSerializer.Deserialize(reader);
                    }
                }
                this.Add(key, value);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                throw;
            }
            finally
            {
                if (!reader.IsEmptyElement)
                    reader.ReadEndElement();
                else
                    reader.Skip();
            }
        }

        /// <summary>
        /// Serializes the dictionary item.
        /// </summary>
        /// <param name="writer"> The XML writer to serialize to. </param>
        /// <param name="keyValuePair"> The key/value pair. </param>
        private void WriteItem(XmlWriter writer, KeyValuePair<TKey, TValue> keyValuePair)
        {
            writer.WriteStartElement(DEFAULT_ITEM_TAG);
            try
            {
                // If both TKey and TValue are primitive types, enums or strings, then they can be serialized as attributes.
                // Otherwise, it's better to serialize both of them as elements.
                bool isKeySerializableAsAttribute = typeof(TKey).IsPrimitive || typeof(TKey).IsEnum || typeof(TKey) == typeof(string);
                bool isValueSerializableAsAttribute = typeof(TValue).IsPrimitive || typeof(TValue).IsEnum || typeof(TValue) == typeof(string);

                if (isKeySerializableAsAttribute && isValueSerializableAsAttribute)
                {
                    writer.WriteAttributeString(DEFAULT_KEY_TAG, keyValuePair.Key.ToString());
                    writer.WriteAttributeString(DEFAULT_VALUE_TAG, keyValuePair.Value.ToString());
                }
                else
                {
                    keySerializer.Serialize(writer, keyValuePair.Key);
                    valueSerializer.Serialize(writer, keyValuePair.Value);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                throw;
            }
            finally
            {
                writer.WriteEndElement();
            }
        }
    }
}