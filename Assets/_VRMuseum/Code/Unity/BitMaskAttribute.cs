﻿using System;
using UnityEngine;

namespace VRMuseum.Unity
{
    [AttributeUsage(AttributeTargets.Field)]
    public class BitMaskAttribute : PropertyAttribute
    {
        public Type PropertyType { get; private set; }

        public BitMaskAttribute(Type type)
        {
            this.PropertyType = type;
        }
    }
}