﻿#if UNITY_EDITOR

using UnityEditor;

namespace VRMuseum.Unity.UnityEditor
{
    [CustomEditor(typeof(Teleporting))]
    public class TeleportingEditor : Editor
    {
        private SerializedProperty isTeleporting;
        private SerializedProperty teleportType;

        private SerializedProperty pointerPrefab;
        private SerializedProperty pointerLinePrefab;
        private SerializedProperty actualTeleportPositionIndicatorPrefab;

        private SerializedProperty blinkPlanesPrefab;
        private SerializedProperty blinkDuration;

        private void OnEnable()
        {
            this.isTeleporting = this.serializedObject.FindProperty("isTeleporting");
            this.teleportType = this.serializedObject.FindProperty("teleportType");

            this.pointerPrefab = this.serializedObject.FindProperty("pointerPrefab");
            this.pointerLinePrefab = this.serializedObject.FindProperty("pointerLinePrefab");
            this.actualTeleportPositionIndicatorPrefab = this.serializedObject.FindProperty("actualTeleportPositionIndicatorPrefab");

            this.blinkPlanesPrefab = this.serializedObject.FindProperty("blinkPlanesPrefab");
            this.blinkDuration = this.serializedObject.FindProperty("blinkDuration");
        }

        public override void OnInspectorGUI()
        {
            this.serializedObject.Update();

            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.PropertyField(this.serializedObject.FindProperty("m_Script"));
            EditorGUILayout.Space();
            EditorExtension.DrawPropertyFieldSafe(this.isTeleporting, "isTeleporting");
            EditorGUI.EndDisabledGroup();

            EditorExtension.DrawPropertyFieldSafe(this.pointerPrefab, "pointerPrefab");
            EditorExtension.DrawPropertyFieldSafe(this.pointerLinePrefab, "pointerLinePrefab");
            EditorExtension.DrawPropertyFieldSafe(this.actualTeleportPositionIndicatorPrefab, "actualTeleportPositionIndicatorPrefab");

            EditorGUILayout.Space();
            if (EditorExtension.DrawPropertyFieldSafe(this.teleportType, "teleportType"))
            {
                if ((TeleportType)this.teleportType.intValue == TeleportType.BlinkEmulation)
                {
                    EditorGUI.indentLevel++;
                    EditorExtension.DrawPropertyFieldSafe(this.blinkPlanesPrefab, "blinkPlanesPrefab");
                    EditorExtension.DrawPropertyFieldSafe(this.blinkDuration, "blinkDuration");
                    EditorGUI.indentLevel--;
                }
            }

            this.serializedObject.ApplyModifiedProperties();
        }
    }
}

#endif