﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;
using VRMuseum.Extensions;

namespace VRMuseum.Unity.UnityEditor
{
    [CustomEditor(typeof(Room))]
    public class RoomEditor : Editor
    {
        private Room targetObject;

        #region Serialized properties

        private SerializedProperty roomSize;

        // Room walls

        private SerializedProperty ceiling;
        private SerializedProperty leftWall;
        private SerializedProperty rightWall;
        private SerializedProperty middleWall;
        private SerializedProperty entranceWall;
        private SerializedProperty backWall;
        private SerializedProperty floor;
        private SerializedProperty middleWallLeft;
        private SerializedProperty middleWallRight;

        // Info sections

        private SerializedProperty infoSectionsHeight;
        private SerializedProperty infoTextResolutionMultiplier;
        private SerializedProperty leftInfoSection;
        private SerializedProperty rightInfoSection;

        // Physical dimensions

        private SerializedProperty ceilingHeight;
        private SerializedProperty paintingsHeight;

        private SerializedProperty middleWallThickness;
        private SerializedProperty corridorsWidth;

        private SerializedProperty infoSectionCornerDistance;
        private SerializedProperty entranceInfoWidth;
        private SerializedProperty leftPaintingsDistance;
        private SerializedProperty leftCornerDistance;

        private SerializedProperty exitInfoWidth;
        private SerializedProperty minRightCornerDistance;

        // -- Right

        private SerializedProperty rightPaintingsDistance;
        private SerializedProperty rightCornerDistance;
        private SerializedProperty rightInfoPaintingsDistance;

        private SerializedProperty k;
        private SerializedProperty maxK;

        #endregion Serialized properties

        private bool RoomComponentsFoldedOut
        {
            get
            {
                return this.targetObject != null ? this.targetObject.roomComponentsFoldedOut : false;
            }
            set
            {
                if (this.targetObject != null)
                    this.targetObject.roomComponentsFoldedOut = value;
            }
        }

        private void OnEnable()
        {
            this.targetObject = this.target as Room;

            this.roomSize = this.serializedObject.FindProperty("roomSize");

            this.ceiling = this.serializedObject.FindProperty("ceiling");
            this.leftWall = this.serializedObject.FindProperty("leftWall");
            this.rightWall = this.serializedObject.FindProperty("rightWall");
            this.middleWall = this.serializedObject.FindProperty("middleWall");
            this.entranceWall = this.serializedObject.FindProperty("entranceWall");
            this.backWall = this.serializedObject.FindProperty("backWall");
            this.floor = this.serializedObject.FindProperty("floor");
            this.middleWallLeft = this.serializedObject.FindProperty("middleWallLeft");
            this.middleWallRight = this.serializedObject.FindProperty("middleWallRight");

            this.infoSectionsHeight = this.serializedObject.FindProperty("infoSectionsHeight");
            this.infoTextResolutionMultiplier = this.serializedObject.FindProperty("infoTextResolutionMultiplier");
            this.leftInfoSection = this.serializedObject.FindProperty("leftInfoSection");
            this.rightInfoSection = this.serializedObject.FindProperty("rightInfoSection");


            this.ceilingHeight = this.serializedObject.FindProperty("ceilingHeight");
            this.paintingsHeight = this.serializedObject.FindProperty("paintingsHeight");
            this.middleWallThickness = this.serializedObject.FindProperty("middleWallThickness");
            this.corridorsWidth = this.serializedObject.FindProperty("corridorsWidth");
            this.infoSectionCornerDistance = this.serializedObject.FindProperty("infoSectionCornerDistance");
            this.entranceInfoWidth = this.serializedObject.FindProperty("entranceInfoWidth");
            this.leftPaintingsDistance = this.serializedObject.FindProperty("leftPaintingsDistance");
            this.leftCornerDistance = this.serializedObject.FindProperty("leftCornerDistance");
            this.exitInfoWidth = this.serializedObject.FindProperty("exitInfoWidth");
            this.minRightCornerDistance = this.serializedObject.FindProperty("minRightCornerDistance");

            this.rightPaintingsDistance = this.serializedObject.FindProperty("rightPaintingsDistance");
            this.rightCornerDistance = this.serializedObject.FindProperty("rightCornerDistance");
            this.rightInfoPaintingsDistance = this.serializedObject.FindProperty("rightInfoPaintingsDistance");

            this.k = this.serializedObject.FindProperty("k");
            this.maxK = this.serializedObject.FindProperty("maxK");
        }

        public override void OnInspectorGUI()
        {
            this.serializedObject.Update();

            bool allGood = true;

            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.PropertyField(this.serializedObject.FindProperty("m_Script"));
            allGood &= EditorExtension.DrawPropertyFieldSafe(this.roomSize, "roomSize", new GUIContent("Room Size (cm)", "X: width\nY: height\nZ: length"));
            EditorGUI.EndDisabledGroup();


            // [ Room Walls ]–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
            EditorGUILayout.Space();
            this.RoomComponentsFoldedOut = !EditorGUILayout.Foldout(!this.RoomComponentsFoldedOut, "Room Walls", true, new GUIStyle(EditorStyles.foldout) { fontStyle = FontStyle.Bold });
            if (!this.RoomComponentsFoldedOut)
            {
                EditorGUI.indentLevel++;
                allGood &= EditorExtension.DrawPropertyFieldSafe(this.ceiling, "ceiling");
                allGood &= EditorExtension.DrawPropertyFieldSafe(this.leftWall, "leftWall");
                allGood &= EditorExtension.DrawPropertyFieldSafe(this.rightWall, "rightWall");
                allGood &= EditorExtension.DrawPropertyFieldSafe(this.middleWall, "middleWall");
                EditorGUI.indentLevel++;
                allGood &= EditorExtension.DrawPropertyFieldSafe(this.middleWallLeft, "middleWallLeft");
                allGood &= EditorExtension.DrawPropertyFieldSafe(this.middleWallRight, "middleWallRight");
                EditorGUI.indentLevel--;
                allGood &= EditorExtension.DrawPropertyFieldSafe(this.entranceWall, "entranceWall");
                allGood &= EditorExtension.DrawPropertyFieldSafe(this.backWall, "backWall");
                allGood &= EditorExtension.DrawPropertyFieldSafe(this.floor, "floor");
                EditorGUI.indentLevel--;
            }

            // [ Info Sections ]––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
            EditorGUILayout.Space();
            allGood &= EditorExtension.DrawRangeFieldSafe(this.infoSectionsHeight, "infoSectionsHeight", 1.5f, this.ceilingHeight.floatValue, new GUIContent("Info Sections Height", "The height of the info sections (in centimeters)"));
            allGood &= EditorExtension.DrawPropertyFieldSafe(this.infoTextResolutionMultiplier, "infoTextResolutionMultiplier");
            allGood &= EditorExtension.DrawPropertyFieldSafe(this.leftInfoSection, "leftInfoSection");
            allGood &= EditorExtension.DrawPropertyFieldSafe(this.rightInfoSection, "rightInfoSection");

            //   –– verify that all the components have been selected.
            if (!allGood)
                EditorGUILayout.HelpBox("Ooooops! Something's wrong. One (or more) of the serialized properties in the CustomEditor is null.", MessageType.Error, true);
            else if (!this.VerifyComponents())
                EditorGUILayout.HelpBox("Assign all the components to the attributes above.", MessageType.Error, true);
            else
            {
                // [ Physical Dimensions ]––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Physical Dimensions", EditorStyles.boldLabel);
                allGood &= EditorExtension.DrawPropertyFieldSafe(this.ceilingHeight, "ceilingHeight", new GUIContent("Ceiling Height", "The ceiling height (in centimeters)"));
                allGood &= EditorExtension.DrawPropertyFieldSafe(this.paintingsHeight, "paintingsHeight", new GUIContent("Paintings Height", "The height of the paintings (in centimeters)\nThis is actually the height of the center/pivot point of the painting."));

                allGood &= EditorExtension.DrawPropertyFieldSafe(this.middleWallThickness, "middleWallThickness", new GUIContent("Middle Wall Thickness", "The thickness of the middle wall (in centimeters)"));
                allGood &= EditorExtension.DrawPropertyFieldSafe(this.corridorsWidth, "corridorsWidth", new GUIContent("Corridors Width", "The width of the two \"corridors\" (in centimeters)"));

                allGood &= EditorExtension.DrawPropertyFieldSafe(this.infoSectionCornerDistance, "infoSectionCornerDistance", new GUIContent("[LR] Entrance Corner Distance", "The distance between the info sections and the entrance wall (in centimeters)"));
                allGood &= EditorExtension.DrawPropertyFieldSafe(this.entranceInfoWidth, "entranceInfoWidth", new GUIContent("[L] Info Width", "The width of the section reserved for intro information (in centimeters)"));
                allGood &= EditorExtension.DrawPropertyFieldSafe(this.leftPaintingsDistance, "leftPaintingsDistance", new GUIContent("[L] Paintings Distance", "The distance between each painting on the left wall (in centimeters)"));
                allGood &= EditorExtension.DrawPropertyFieldSafe(this.leftCornerDistance, "leftCornerDistance", new GUIContent("[L] Back Corner Distance", "The distance between the last painting on the left wall and the back wall (in centimeters)"));

                allGood &= EditorExtension.DrawPropertyFieldSafe(this.exitInfoWidth, "exitInfoWidth", new GUIContent("[R] Info Width", "The width of the section reserved for credits (in centimeters)"));
                allGood &= EditorExtension.DrawPropertyFieldSafe(this.minRightCornerDistance, "minRightCornerDistance", new GUIContent("[R] Min Back Corner Distance", "The minimum distance between the last painting on the right wall and the back wall (in centimeters)"));


                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Right wall", EditorStyles.boldLabel);
                this.DrawKSlider();
                EditorGUI.BeginDisabledGroup(true);
                allGood &= EditorExtension.DrawPropertyFieldSafe(this.rightPaintingsDistance, "rightPaintingsDistance", new GUIContent("[R] Paintings Distance", "The distance between each painting on the right wall (in centimeters)"));
                allGood &= EditorExtension.DrawPropertyFieldSafe(this.rightCornerDistance, "rightCornerDistance", new GUIContent("[R] Back Corner Distance", "The distance between the last painting on the right wall and the back wall (in centimeters)"));
                allGood &= EditorExtension.DrawPropertyFieldSafe(this.rightInfoPaintingsDistance, "rightInfoPaintingsDistance", new GUIContent("[R] Info-Paintings Distance", "The distance between the right info section and the first painting (in centimeters)"));
                EditorGUI.EndDisabledGroup();

                if (!allGood)
                    EditorGUILayout.HelpBox("Ooooops! Something's wrong. One (or more) of the serialized properties in the CustomEditor is null.", MessageType.Error, true);


                // [ Create Button ]––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
                EditorGUILayout.Space();
                EditorGUI.BeginDisabledGroup(!allGood);
                if (GUILayout.Button("Create Room"))
                {
                    this.CreateRoom();
                    this.MovePaintings();
                    this.UpdateInfoSections();
                }
                EditorGUI.EndDisabledGroup();
            }

            this.UpdateMaxK();
            this.serializedObject.ApplyModifiedProperties();
        }

        private bool VerifyComponents()
        {
            return this.ceiling != null && this.ceiling.objectReferenceValue != null &&
                   this.leftWall != null && this.leftWall.objectReferenceValue != null &&
                   this.rightWall != null && this.rightWall.objectReferenceValue != null &&
                   this.middleWall != null && this.middleWall.objectReferenceValue != null &&
                   this.middleWallLeft != null && this.middleWallLeft.objectReferenceValue != null &&
                   this.middleWallRight != null && this.middleWallRight.objectReferenceValue != null &&
                   this.entranceWall != null && this.entranceWall.objectReferenceValue != null &&
                   this.backWall != null && this.backWall.objectReferenceValue != null &&
                   this.floor != null && this.floor.objectReferenceValue != null &&

                   this.leftInfoSection != null && this.leftInfoSection.objectReferenceValue != null &&
                   this.rightInfoSection != null && this.rightInfoSection.objectReferenceValue != null;
        }

        private void CreateRoom()
        {
            Transform ceiling = (this.ceiling.objectReferenceValue as GameObject).transform;
            Transform leftWall = (this.leftWall.objectReferenceValue as GameObject).transform;
            Transform rightWall = (this.rightWall.objectReferenceValue as GameObject).transform;
            Transform middleWall = (this.middleWall.objectReferenceValue as GameObject).transform;
            Transform entranceWall = (this.entranceWall.objectReferenceValue as GameObject).transform;
            Transform backWall = (this.backWall.objectReferenceValue as GameObject).transform;
            Transform floor = (this.floor.objectReferenceValue as GameObject).transform;
            Transform middleWallLeft = (this.middleWallLeft.objectReferenceValue as GameObject).transform;
            Transform middleWallRight = (this.middleWallRight.objectReferenceValue as GameObject).transform;

            float ceilingHeight = this.ceilingHeight.floatValue;
            float middleWallThickness = this.middleWallThickness.floatValue;
            float corridorsWidth = this.corridorsWidth.floatValue;
            float infoSectionCornerDistance = this.infoSectionCornerDistance.floatValue;
            float entranceInfoWidth = this.entranceInfoWidth.floatValue;
            float leftPaintingsDistance = this.leftPaintingsDistance.floatValue;
            float leftCornerDistance = this.leftCornerDistance.floatValue;
            float exitInfoWidth = this.exitInfoWidth.floatValue;
            float minRightCornerDistance = this.minRightCornerDistance.floatValue;

            // Calculate the room dimensions (the length is equal to the left wall length)
            float roomLength = Room.LEFT_PAINTINGS_TOTAL_WIDTH +    // Total width of the paintings.
                               4 * leftPaintingsDistance +          // Sum of the distances between the 5 paintings.
                               infoSectionCornerDistance +          // Blank space before the info section.
                               entranceInfoWidth +                  // The entrance info section.
                               leftPaintingsDistance +              // Distance between the end of the info section and the first painting.
                               leftCornerDistance;                  // Blank space after the last painting.

            float roomWidth = 2 * corridorsWidth + middleWallThickness;
            float roomHeight = ceilingHeight;
            this.roomSize.vector3Value = new Vector3(roomWidth, roomHeight, roomLength);


            // CEILING & FLOOR (both are planes)
            var ceilingParent = ceiling.parent;
            ceiling.parent = null;
            ceiling.position = new Vector3(0.0f, roomHeight / 100.0f, 0.0f);
            ceiling.rotation = Quaternion.Euler(0.0f, 0.0f, 180.0f);
            ceiling.localScale = new Vector3(roomWidth, 1000.0f, roomLength) / 1000.0f;
            ceiling.parent = ceilingParent;

            var floorParent = floor.parent;
            floor.parent = null;
            floor.position = Vector3.zero;
            floor.rotation = Quaternion.identity;
            floor.localScale = new Vector3(roomWidth, 1000.0f, roomLength) / 1000.0f;
            floor.parent = floorParent;


            // LEFT & RIGHT WALLS (both are planes)
            var leftWallParent = leftWall.parent;
            leftWall.parent = null;
            leftWall.position = new Vector3((floor.lossyScale.x * 10) / 2, (roomHeight / 100.0f) / 2, 0.0f);
            leftWall.rotation = Quaternion.Euler(0.0f, 0.0f, 90.0f);
            leftWall.localScale = new Vector3(roomHeight, 1000.0f, roomLength) / 1000.0f;
            leftWall.parent = leftWallParent;

            var rightWallParent = rightWall.parent;
            rightWall.parent = null;
            rightWall.position = new Vector3(-(floor.lossyScale.x * 10) / 2, (roomHeight / 100.0f) / 2, 0.0f);
            rightWall.rotation = Quaternion.Euler(0.0f, 0.0f, -90.0f);
            rightWall.localScale = new Vector3(roomHeight, 1000.0f, roomLength) / 1000.0f;
            rightWall.parent = rightWallParent;


            // MIDDLE WALL (cube)
            var middleWallParent = middleWall.parent;
            middleWall.parent = null;
            //middleWall.position = new Vector3(0.0f, roomHeight / 2, 2 * Room.PASSAGE_WIDTH / 10.0f) / 100.0f;   // passages widths are 3/5 and 2/5 of (2 * PASSAGE_WIDTH);
            middleWall.position = new Vector3(0.0f, roomHeight / 2, 0.0f) / 100.0f;
            middleWall.rotation = Quaternion.identity;
            middleWall.localScale = new Vector3(middleWallThickness, roomHeight, roomLength - 2 * Room.PASSAGE_WIDTH) / 100.0f;
            middleWall.parent = middleWallParent;

            middleWallLeft.gameObject.RemoveComponents(true);
            middleWallLeft.parent = middleWall;
            middleWallLeft.localPosition = new Vector3(0.5f, 0.0f, 0.0f);
            middleWallLeft.localRotation = Quaternion.identity;
            middleWallLeft.localScale = Vector3.one;
            middleWallRight.gameObject.RemoveComponents(true);
            middleWallRight.parent = middleWall;
            middleWallRight.localPosition = new Vector3(-0.5f, 0.0f, 0.0f);
            middleWallRight.localRotation = Quaternion.identity;
            middleWallRight.localScale = Vector3.one;


            // ENTRANCE & BACK WALLS (both are planes?)
            var entranceWallParent = entranceWall.parent;
            entranceWall.parent = null;
            entranceWall.position = new Vector3(0.0f, (roomHeight / 100) / 2, (floor.lossyScale.z * 10) / 2);
            entranceWall.rotation = Quaternion.Euler(-90.0f, 0.0f, 0.0f);
            entranceWall.localScale = new Vector3(roomWidth, 1000.0f, roomHeight) / 1000.0f;
            entranceWall.parent = entranceWallParent;

            var backWallParent = backWall.parent;
            backWall.parent = null;
            backWall.position = new Vector3(0.0f, (roomHeight / 100.0f) / 2, -(floor.lossyScale.z * 10) / 2);
            backWall.rotation = Quaternion.Euler(90.0f, 0.0f, 0.0f);
            backWall.localScale = new Vector3(roomWidth, 1000.0f, roomHeight) / 1000.0f;
            backWall.parent = backWallParent;


            // Calculate max value for the parameter k.
            this.UpdateMaxK();

            // Update paintings' transforms.
            foreach (var p in FindObjectsOfType<Painting>())
                p.UpdatePaintingTransform();
        }

        private void MovePaintings()
        {
            Transform leftWall = (this.leftWall.objectReferenceValue as GameObject).transform;
            Transform rightWall = (this.rightWall.objectReferenceValue as GameObject).transform;
            Transform middleWall = (this.middleWall.objectReferenceValue as GameObject).transform;
            Transform middleWallLeft = (this.middleWallLeft.objectReferenceValue as GameObject).transform;
            Transform middleWallRight = (this.middleWallRight.objectReferenceValue as GameObject).transform;

            float paintingsHeightMeters = this.paintingsHeight.floatValue / 100.0f;
            float infoSectionCornerDistanceMeters = this.infoSectionCornerDistance.floatValue / 100.0f;
            float entranceInfoWidthMeters = this.entranceInfoWidth.floatValue / 100.0f;
            float leftPaintingsDistanceMeters = this.leftPaintingsDistance.floatValue / 100.0f;
            float rightPaintingsDistanceMeters = this.rightPaintingsDistance.floatValue / 100.0f;
            float rightCornerDistanceMeters = this.rightCornerDistance.floatValue / 100.0f;

            // Move all to the correct height.
            foreach (Painting p in FindObjectsOfType<Painting>())
                p.gameObject.transform.position = new Vector3(p.gameObject.transform.position.x, paintingsHeightMeters, p.gameObject.transform.position.z);


            float currentZWorld;

            // Left wall
            currentZWorld = leftWall.transform.lossyScale.z * 10.0f / 2 - (infoSectionCornerDistanceMeters + entranceInfoWidthMeters);
            for (int i = 0; i < leftWall.transform.childCount; ++i)
            {
                Transform child = leftWall.transform.GetChild(i);
                Painting p = child.gameObject.GetComponent<Painting>();
                if (p == null)
                    continue;

                currentZWorld -= (p.Width / 200.0f + leftPaintingsDistanceMeters);
                p.transform.position = new Vector3(leftWall.transform.position.x, paintingsHeightMeters, currentZWorld);
                p.transform.rotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);

                currentZWorld -= p.Width / 200.0f;
            }


            // Right wall
            currentZWorld = -rightWall.transform.lossyScale.z * 10.0f / 2 + (rightCornerDistanceMeters);
            for (int i = 0; i < rightWall.transform.childCount; ++i)
            {
                Transform child = rightWall.transform.GetChild(i);
                Painting p = child.gameObject.GetComponent<Painting>();
                if (p == null)
                    continue;

                currentZWorld += p.Width / 200.0f;
                p.transform.position = new Vector3(rightWall.transform.position.x, paintingsHeightMeters, currentZWorld);
                p.transform.rotation = Quaternion.Euler(0.0f, -90.0f, 0.0f);

                currentZWorld += (p.Width / 200.0f + rightPaintingsDistanceMeters);
            }
        }

        private void UpdateInfoSections()
        {
            Transform leftWall = (this.leftWall.objectReferenceValue as GameObject).transform;
            Transform rightWall = (this.rightWall.objectReferenceValue as GameObject).transform;
            RectTransform leftInfoSection = (this.leftInfoSection.objectReferenceValue as InfoSection).gameObject.transform as RectTransform;
            RectTransform rightInfoSection = (this.rightInfoSection.objectReferenceValue as InfoSection).gameObject.transform as RectTransform;

            float infoSectionsHeight = this.infoSectionsHeight.floatValue;
            float infoTextResolutionMultiplier = this.infoTextResolutionMultiplier.floatValue;
            float infoSectionCornerDistanceMeters = this.infoSectionCornerDistance.floatValue / 100.0f;
            float entranceInfoWidth = this.entranceInfoWidth.floatValue;
            float exitInfoWidth = this.exitInfoWidth.floatValue;

            // Left InfoSection
            float leftInfoSectionStartingZWorld = leftWall.transform.lossyScale.z * 10.0f / 2 - infoSectionCornerDistanceMeters;
            Canvas leftInfoSectionCanvas = leftInfoSection.gameObject.GetComponent<Canvas>();
            leftInfoSectionCanvas.renderMode = RenderMode.WorldSpace;
            var leftInfoSectionParent = leftInfoSection.parent;
            leftInfoSection.SetParent(null, false);
            leftInfoSection.pivot = Vector2.up;
            leftInfoSection.anchorMin = Vector2.zero;
            leftInfoSection.anchorMax = Vector2.zero;
            leftInfoSection.sizeDelta = new Vector2(entranceInfoWidth, infoSectionsHeight) * infoTextResolutionMultiplier;
            leftInfoSection.rotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);
            leftInfoSection.localScale = new Vector3(0.01f, 0.01f, infoTextResolutionMultiplier) / infoTextResolutionMultiplier;
            leftInfoSection.position = new Vector3(leftWall.position.x - 0.005f, infoSectionsHeight / 100.0f, leftInfoSectionStartingZWorld);
            leftInfoSection.SetParent(leftInfoSectionParent, true);

            // Right InfoSection
            float rightInfoSectionStartingZWorld = rightWall.transform.lossyScale.z * 10.0f / 2 - infoSectionCornerDistanceMeters;
            Canvas rightInfoSectionCanvas = leftInfoSection.gameObject.GetComponent<Canvas>();
            rightInfoSectionCanvas.renderMode = RenderMode.WorldSpace;
            var rightInfoSectionParent = rightInfoSection.parent;
            rightInfoSection.SetParent(null, false);
            rightInfoSection.pivot = Vector2.one;
            rightInfoSection.anchorMin = Vector2.zero;
            rightInfoSection.anchorMax = Vector2.zero;
            rightInfoSection.sizeDelta = new Vector2(exitInfoWidth, infoSectionsHeight) * infoTextResolutionMultiplier;
            rightInfoSection.rotation = Quaternion.Euler(0.0f, -90.0f, 0.0f);
            rightInfoSection.localScale = new Vector3(0.01f, 0.01f, infoTextResolutionMultiplier) / infoTextResolutionMultiplier;
            rightInfoSection.position = new Vector3(rightWall.position.x + 0.005f, infoSectionsHeight / 100.0f, rightInfoSectionStartingZWorld);
            rightInfoSection.SetParent(rightInfoSectionParent, true);
        }

        private void UpdateMaxK()
        {
            float minRightCornerDistance = this.minRightCornerDistance.floatValue;

            /*float supLim1 = Room.PASSAGE_WIDTH - leftCornerDistance;
            float supLim2 = (roomLength - Room.RIGHT_PAINTINGS_TOTAL_WIDTH - minRightCornerDistance - 4 * leftPaintingsDistance - exitInfoWidth - infoSectionCornerDistance) / 2;
            this.maxK.floatValue = Mathf.Clamp(Mathf.Min(supLim1, supLim2), 0.0f, float.PositiveInfinity);
            this.k.floatValue = Mathf.Clamp(this.k.floatValue, 0.0f, this.maxK.floatValue);*/
            this.maxK.floatValue = Mathf.Clamp(Room.PASSAGE_WIDTH - minRightCornerDistance, 0.0f, float.PositiveInfinity);
            this.k.floatValue = Mathf.Clamp(this.k.floatValue, 0.0f, this.maxK.floatValue);
        }

        private void DrawKSlider()
        {
            /* { roomLength = RIGHT_PAINTINGS_TOTAL_WIDTH + 3⨯rightPaintingsDistance + infoSectionCornerDistance +
             *                + exitInfoWidth + (leftPaintingsDistance + k) + (minRightCornerDistance + k)
             * { minRightCornerDistance + k < PASSAGE_WIDTH
             * { rightPaintingsDistance ≥ leftPaintingsDistance
             * { k ≥ 0
             * –––––––––––––––––––––––––––––––––––––––––––––––––
             * { rightPaintingsDistance = f(k)
             * { k < PASSAGE_WIDTH - leftCornerDistance
             * { k ≤ (roomLength - RIGHT_PAINTINGS_TOTAL_WIDTH - minRightCornerDistance - 4⨯leftPaintingsDistance - exitInfoWidth - infoSectionCornerDistance) / 2
             * { k ≥ 0
             * –––––––––––––––––––––––––––––––––––––––––––––––––
             * => 0 < k < min({2},{3})
             * => rightPaintingsDistance = f(k) */

            float infoSectionCornerDistance = this.infoSectionCornerDistance.floatValue;
            float leftPaintingsDistance = this.leftPaintingsDistance.floatValue;
            float leftCornerDistance = this.leftCornerDistance.floatValue;
            float exitInfoWidth = this.exitInfoWidth.floatValue;
            float minRightCornerDistance = this.minRightCornerDistance.floatValue;

            this.k.floatValue = EditorGUILayout.Slider(new GUIContent("k", "This parameter drives the right paintings' distance."), this.k.floatValue, 0.0f, this.maxK.floatValue);
            this.rightPaintingsDistance.floatValue = (this.roomSize.vector3Value.z - Room.RIGHT_PAINTINGS_TOTAL_WIDTH - infoSectionCornerDistance
                                                      - exitInfoWidth - leftPaintingsDistance - minRightCornerDistance - 2 * this.k.floatValue) / 3;
            this.rightCornerDistance.floatValue = minRightCornerDistance + this.k.floatValue;
            this.rightInfoPaintingsDistance.floatValue = leftPaintingsDistance + this.k.floatValue;
        }
    }
}

#endif