﻿#if UNITY_EDITOR

using UnityEditor;
using InteractionTypeFlags = VRMuseum.InteractableObject.InteractionTypeFlags;
using UseTypeFlags = VRMuseum.InteractableObject.UseTypeFlags;

namespace VRMuseum.Unity.UnityEditor
{
    [CustomEditor(typeof(InteractableObject))]
    public class InteractableObjectEditor : Editor
    {
        private InteractableObject targetObject;

        private SerializedProperty canReactToGaze;
        private SerializedProperty canReactToInteraction;
        private SerializedProperty allowedInteractions;
        private SerializedProperty allowedUseTypes;

        private SerializedProperty gazeEvents;
        private SerializedProperty grabInteractionEvents;
        private SerializedProperty useInteractionEvents;

        private bool ShowEvents
        {
            get
            {
                return this.targetObject != null ? this.targetObject.showEvents : true;
            }
            set
            {
                if (this.targetObject != null)
                    this.targetObject.showEvents = value;
            }
        }

        private void OnEnable()
        {
            this.targetObject = this.target as InteractableObject;

            this.canReactToGaze = this.serializedObject.FindProperty("_canReactToGaze");
            this.canReactToInteraction = this.serializedObject.FindProperty("_canReactToInteraction");
            this.allowedInteractions = this.serializedObject.FindProperty("_allowedInteractions");
            this.allowedUseTypes = this.serializedObject.FindProperty("_allowedUseTypes");

            this.gazeEvents = this.serializedObject.FindProperty("gazeEvents");
            this.grabInteractionEvents = this.serializedObject.FindProperty("grabInteractionEvents");
            this.useInteractionEvents = this.serializedObject.FindProperty("useInteractionEvents");
        }

        public override void OnInspectorGUI()
        {
            this.serializedObject.Update();

            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.PropertyField(this.serializedObject.FindProperty("m_Script"));
            EditorGUI.EndDisabledGroup();


            // [ Behaviour ]––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
            EditorExtension.DrawPropertyFieldSafe(this.canReactToGaze, "CanReactToGaze");
            EditorExtension.DrawPropertyFieldSafe(this.canReactToInteraction, "CanReactToInteraction");


            // [ Interaction types ]––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
            if (this.targetObject.CanReactToInteraction)
            {
                EditorExtension.DrawPropertyFieldSafe(this.allowedInteractions, "InteractionTypes");

                if ((this.targetObject.AllowedInteractions & InteractionTypeFlags.Use) != 0)
                {
                    EditorGUI.indentLevel++;
                    EditorExtension.DrawPropertyFieldSafe(this.allowedUseTypes, "UseTypes");
                    EditorGUI.indentLevel--;
                }
            }


            // [ Private events ]–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
            bool canBeGazed = this.targetObject.CanReactToGaze;
            bool canBeInteracted = this.targetObject.CanReactToInteraction && this.targetObject.AllowedInteractions != InteractionTypeFlags.Nothing;

            if (canBeGazed || canBeInteracted)
            {
                EditorGUILayout.Space();
                this.ShowEvents = EditorGUILayout.Foldout(this.ShowEvents, "Events", true);

                if (this.ShowEvents)
                {
                    EditorGUI.indentLevel++;
                    if (canBeGazed)
                        EditorExtension.DrawPropertyFieldSafe(this.gazeEvents, "GazeEvents", true);

                    if (canBeInteracted)
                    {
                        // Grab
                        if ((this.targetObject.AllowedInteractions & InteractionTypeFlags.Grab) != 0)
                        {
                            int indentLevel = EditorGUI.indentLevel;
                            EditorGUI.indentLevel = 0;
                            EditorGUILayout.HelpBox("Do not implement the method to attach the object to the hand.", MessageType.Warning, true);
                            EditorExtension.DrawPropertyFieldSafe(this.grabInteractionEvents, "GrabInteractionEvents", true);
                            EditorGUI.indentLevel = indentLevel;
                        }

                        // Use
                        if ((this.targetObject.AllowedInteractions & InteractionTypeFlags.Use) != 0 && this.targetObject.AllowedUseTypes != UseTypeFlags.Nothing)
                        {
                            EditorExtension.DrawPropertyFieldSafe(this.useInteractionEvents, "UseInteractionEvents", true);
                        }
                    }
                    EditorGUI.indentLevel--;
                }
            }

            this.serializedObject.ApplyModifiedProperties();
        }
    }
}

#endif