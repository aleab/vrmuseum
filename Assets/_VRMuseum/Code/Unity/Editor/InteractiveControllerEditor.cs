﻿#if UNITY_EDITOR

using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using ActionFlags = VRMuseum.InteractiveController.ActionFlags;

namespace VRMuseum.Unity.UnityEditor
{
    [CustomEditor(typeof(InteractiveController))]
    public class InteractiveControllerEditor : Editor
    {
        private InteractiveController targetObject;

        private SerializedProperty gripAction;
        private SerializedProperty triggerAction;
        private SerializedProperty padAction;

        private SerializedProperty tip;

        private SerializedProperty hoverSphereRadius;

        private SerializedProperty debug;

        private SerializedProperty hoverSphereMaterial;
        private SerializedProperty hoverSphereObject;

        private Material defaultMaterial;


        private Dictionary<ActionFlags, string> actionFlagsValueNames;
        private HashSet<ActionFlags> availableActions;

        private void OnEnable()
        {
            this.targetObject = this.target as InteractiveController;

            this.gripAction = this.serializedObject.FindProperty("_gripAction");
            this.triggerAction = this.serializedObject.FindProperty("_triggerAction");
            this.padAction = this.serializedObject.FindProperty("_padAction");

            this.tip = this.serializedObject.FindProperty("tip");
            this.hoverSphereRadius = this.serializedObject.FindProperty("hoverSphereRadius");

            this.debug = this.serializedObject.FindProperty("debug");

            this.hoverSphereMaterial = this.serializedObject.FindProperty("hoverSphereMaterial");
            this.hoverSphereObject = this.serializedObject.FindProperty("hoverSphereObject");

            this.defaultMaterial = AssetDatabase.GetBuiltinExtraResource<Material>("Default-Material.mat");


            var actionFlagsValues = Util.GetEnumValues<ActionFlags>();
            var actionFlagsNames = actionFlagsValues.Select(v => v.ToString()).ToArray();
            this.actionFlagsValueNames = new Dictionary<ActionFlags, string>();
            for (int i = 0; i < actionFlagsValues.Length; ++i)
                this.actionFlagsValueNames.Add(actionFlagsValues[i], actionFlagsNames[i]);

            this.availableActions = new HashSet<ActionFlags>(actionFlagsValues);
        }

        public override void OnInspectorGUI()
        {
            this.serializedObject.Update();

            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.PropertyField(this.serializedObject.FindProperty("m_Script"));
            EditorGUI.EndDisabledGroup();

            // [ Button mapping ]––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Button mapping", EditorStyles.boldLabel);
            this.UpdateAvailableActions();
            EditorExtension.DrawExclusiveEnumPopup(this.gripAction, "GripAction", this.actionFlagsValueNames, this.availableActions, ActionFlags.Nothing, "Grip");
            EditorExtension.DrawExclusiveEnumPopup(this.triggerAction, "TriggerAction", this.actionFlagsValueNames, this.availableActions, ActionFlags.Nothing, "Trigger");
            EditorExtension.DrawExclusiveEnumPopup(this.padAction, "PadAction", this.actionFlagsValueNames, this.availableActions, ActionFlags.Nothing, "Pad (press)");

            // [ HoverSphere ]–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
            EditorGUILayout.Space();
            EditorExtension.DrawPropertyFieldSafe(this.hoverSphereRadius, "hoverSphereRadius");

            // [ Debug ]–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Debug", EditorStyles.boldLabel);
            EditorExtension.DrawPropertyFieldSafe(this.debug, "debug");
            EditorExtension.DrawPropertyFieldSafe(this.hoverSphereMaterial, "hoverSphereMaterial");

            GameObject debugSphere = this.hoverSphereObject.objectReferenceValue as GameObject;
            if (this.debug.boolValue == false)
            {
                if (debugSphere != null)
                {
                    if (Application.isPlaying)
                        Destroy(debugSphere);
                    else
                        DestroyImmediate(debugSphere);
                }
            }
            else
            {
                if (debugSphere == null)
                {
                    debugSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                    debugSphere.name = "hoverSphereObject";
                }
                this.UpdateDebugSphere(debugSphere);
            }

            this.serializedObject.ApplyModifiedProperties();
        }

        private void UpdateAvailableActions()
        {
            var usedActions = new List<int>() { this.gripAction.intValue, this.triggerAction.intValue, this.padAction.intValue };
            this.availableActions = new HashSet<ActionFlags>(this.availableActions.Except(usedActions.Select(i => (ActionFlags)i).Where(a => a != ActionFlags.Nothing)));
        }

        private void UpdateDebugSphere(GameObject debugSphere)
        {
            if (debugSphere != null)
            {
                debugSphere.transform.localScale = new Vector3(2 * this.hoverSphereRadius.floatValue, 2 * this.hoverSphereRadius.floatValue, 2 * this.hoverSphereRadius.floatValue);
                debugSphere.transform.parent = (this.tip.objectReferenceValue as Transform) ?? this.targetObject.transform;
                debugSphere.transform.localPosition = Vector3.zero;

                MeshRenderer mr = debugSphere.GetComponent<MeshRenderer>();
                if (mr != null)
                    mr.sharedMaterial = (this.hoverSphereMaterial.objectReferenceValue as Material) ?? this.defaultMaterial;
            }
            this.hoverSphereObject.objectReferenceValue = debugSphere;
        }
    }
}

#endif