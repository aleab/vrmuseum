﻿#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace VRMuseum.Unity.UnityEditor
{
    public static class EditorExtension
    {
        #region DrawPropertyFieldSafe

        public static bool DrawPropertyFieldSafe(SerializedProperty property, string propertyName, bool includeChildren = true, params GUILayoutOption[] options)
        {
            if (property != null)
            {
                EditorGUILayout.PropertyField(property, includeChildren, options);
                return true;
            }
            else
                return DrawErrorLabel(propertyName);
        }

        public static bool DrawPropertyFieldSafe(SerializedProperty property, string propertyName, GUIContent label, bool includeChildren = true, params GUILayoutOption[] options)
        {
            if (property != null)
            {
                EditorGUILayout.PropertyField(property, label, includeChildren, options);
                return true;
            }
            else
                return DrawErrorLabel(propertyName, string.Format("{0}\n{1}", label.text, label.tooltip));
        }

        #endregion DrawPropertyFieldSafe

        public static bool DrawExclusiveEnumPopup<TEnum>(SerializedProperty property, string propertyName, Dictionary<TEnum, string> enumValueNames, HashSet<TEnum> availableEnumValues, TEnum defaultValue, string fieldLabel)
            where TEnum : struct, IConvertible, IComparable, IFormattable
        {
            if (!typeof(TEnum).IsEnum)
                return false;

            if (property == null)
                return DrawErrorLabel(propertyName);

            int defaultIntValue = defaultValue.ToInt32(CultureInfo.InvariantCulture);
            int previousValue = property.intValue;
            TEnum previousEnumValue = (TEnum)Enum.Parse(typeof(TEnum), previousValue.ToString());

            // Add current value, so that this field can show it.
            if (previousValue != defaultIntValue)
                availableEnumValues.Add(previousEnumValue);

            var availableValueNames = enumValueNames.Join(availableEnumValues,
                                                          outer => outer.Key,
                                                          inner => inner,
                                                          (outer, inner) => new KeyValuePair<TEnum, string>(outer.Key, outer.Value))
                                                    .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

            property.intValue = EditorGUILayout.IntPopup(fieldLabel,
                                                                   property.intValue,
                                                                   availableValueNames.Values.ToArray(),
                                                                   availableValueNames.Keys.Cast<int>().ToArray());

            int newValue = property.intValue;
            TEnum newEnumValue = (TEnum)Enum.Parse(typeof(TEnum), newValue.ToString());

            if (newValue != defaultIntValue)
                availableEnumValues.Remove(newEnumValue);

            return true;
        }

        #region DrawRangeFieldSafe

        public static bool DrawRangeFieldSafe(SerializedProperty property, string propertyName, float min, float max, params GUILayoutOption[] options)
        {
            if (property != null)
            {
                EditorGUILayout.Slider(property, min, max, options);
                return true;
            }
            else
                return DrawErrorLabel(propertyName);
        }

        public static bool DrawRangeFieldSafe(SerializedProperty property, string propertyName, float min, float max, GUIContent label, params GUILayoutOption[] options)
        {
            if (property != null)
            {
                EditorGUILayout.Slider(property, min, max, label, options);
                return true;
            }
            else
                return DrawErrorLabel(propertyName, string.Format("{0}\n{1}", label.text, label.tooltip));
        }

        #endregion DrawRangeFieldSafe

        private static bool DrawErrorLabel(string propertyName, string tooltip = "")
        {
            GUIContent content = new GUIContent()
            {
                text = string.Format("[Error in laying out \"{0}\"]", propertyName),
                tooltip = tooltip
            };
            GUIStyle style = new GUIStyle(EditorStyles.label);
            style.normal.textColor = Color.Lerp(Color.red, Color.black, 0.25f);
            GUILayout.Label(content, style);
            return false;
        }

        #region DrawBitMaskField

        /// <remarks>
        /// See: http://answers.unity3d.com/questions/393992/custom-inspector-multi-select-enum-dropdown.html
        /// </remarks>
        public static T DrawBitMaskField<T>(Rect position, T mask, string label, string tooltip = "") where T : struct, IConvertible, IComparable, IFormattable
        {
            Type type = typeof(T);
            int intValue = Convert.ToInt32(mask);
            int bitMask = DrawBitMaskField(position, intValue, type, label);
            return (T)Convert.ChangeType(bitMask, type);
        }

        /// <remarks>
        /// See: http://answers.unity3d.com/questions/393992/custom-inspector-multi-select-enum-dropdown.html
        /// </remarks>
        public static int DrawBitMaskField(Rect position, int mask, Type enumType, string label, string tooltip = "")
        {
            if (!enumType.IsEnum)
                throw new ArgumentException("T must be an enumerated type");

            Array array = Enum.GetValues(enumType);
            int[] itemValues = new int[array.Length];
            string[] itemNames = Enum.GetNames(enumType);

            var en = array.GetEnumerator();
            for (int i = 0; en.MoveNext(); ++i)
                itemValues[i] = Convert.ToInt32(en.Current);

            int val = mask;
            int maskVal = 0;
            for (int i = 0; i < itemValues.Length; i++)
            {
                if (itemValues[i] != 0)
                {
                    if ((val & itemValues[i]) == itemValues[i])
                        maskVal |= 1 << i;
                }
                else if (val == 0)
                    maskVal |= 1 << i;
            }
            int newMaskVal = EditorGUI.MaskField(position, new GUIContent(label, tooltip), maskVal, itemNames);
            int changes = maskVal ^ newMaskVal;

            for (int i = 0; i < itemValues.Length; i++)
            {
                if ((changes & (1 << i)) != 0)            // Has this list item changed?
                {
                    if ((newMaskVal & (1 << i)) != 0)     // Has it been set?
                    {
                        if (itemValues[i] == 0)           // Special case: if "0" is set, just set the val to 0.
                        {
                            val = 0;
                            break;
                        }
                        else
                            val |= itemValues[i];
                    }
                    else                                  // It has been reset.
                        val &= ~itemValues[i];
                }
            }
            return val;
        }

        #endregion DrawBitMaskField
    }
}

#endif