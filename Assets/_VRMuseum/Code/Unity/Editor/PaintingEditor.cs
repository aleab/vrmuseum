﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace VRMuseum.Unity.UnityEditor
{
    [CustomEditor(typeof(Painting))]
    public class PaintingEditor : Editor
    {
        private Painting targetObject;

        private SerializedProperty metId;

        private SerializedProperty frameSize;
        private SerializedProperty pictureSize;

        private SerializedProperty frameBaseMaterial;
        private SerializedProperty pictureBaseMaterial;
        private SerializedProperty pictureTexture;

        private SerializedProperty frameMesh;
        private SerializedProperty pictureMesh;

        private Material defaultMaterial;
        private Material frameMaterial;
        private Material pictureMaterial;

        private void OnEnable()
        {
            this.targetObject = this.target as Painting;

            this.metId = this.serializedObject.FindProperty("_metId");

            this.frameSize = this.serializedObject.FindProperty("frameSize");
            this.pictureSize = this.serializedObject.FindProperty("pictureSize");

            this.frameBaseMaterial = this.serializedObject.FindProperty("frameBaseMaterial");
            this.pictureBaseMaterial = this.serializedObject.FindProperty("pictureBaseMaterial");
            this.pictureTexture = this.serializedObject.FindProperty("pictureTexture");

            this.frameMesh = this.serializedObject.FindProperty("frameMesh");
            this.pictureMesh = this.serializedObject.FindProperty("pictureMesh");

            this.defaultMaterial = AssetDatabase.GetBuiltinExtraResource<Material>("Default-Material.mat");
        }

        public override void OnInspectorGUI()
        {
            this.serializedObject.Update();

            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.PropertyField(this.serializedObject.FindProperty("m_Script"));
            EditorGUI.EndDisabledGroup();

            // [ Id ]–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
            EditorGUILayout.Space();
            EditorExtension.DrawPropertyFieldSafe(this.metId, "metId", new GUIContent("MET ID", this.metId.tooltip));

            // [ Dimensions ]–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Dimensions", EditorStyles.boldLabel);
            Vector2 ps = Vector2.one;
            Vector3 fs = Vector3.one;
            if (EditorExtension.DrawPropertyFieldSafe(this.pictureSize, "pictureSize", new GUIContent("Picture Size (cm)", "The width and height of the painting's picture in centimeters.")))
            {
                ps = this.pictureSize.vector2Value;
                ps.Set(Mathf.Abs(ps.x), Mathf.Abs(ps.y));
                this.pictureSize.vector2Value = ps;
            }
            if (EditorExtension.DrawPropertyFieldSafe(this.frameSize, "frameSize", new GUIContent("Frame Size (cm)", "The frame borders' dimensions: X = left & right borders, Y = top & bottom borders, Z = frame depth.")))
            {
                fs = this.frameSize.vector3Value;
                fs.Set(Mathf.Abs(fs.x), Mathf.Abs(fs.y), Mathf.Abs(fs.z));
                this.frameSize.vector3Value = fs;
            }

            //   –– update meshes' dimensions
            this.targetObject.UpdatePaintingTransform(fs, ps);

            // [ Materials ]––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

            #region Materials

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Materials", EditorStyles.boldLabel);
            if (EditorExtension.DrawPropertyFieldSafe(this.frameBaseMaterial, "frameBaseMaterial"))
            {
                MeshFilter frame = this.frameMesh != null ? this.frameMesh.objectReferenceValue as MeshFilter : null;
                if (frame != null)
                {
                    if (this.frameMaterial == null)
                        this.frameMaterial = new Material(this.frameBaseMaterial.objectReferenceValue as Material ?? this.defaultMaterial)
                        {
                            name = string.Format("{0}_Frame-Material", this.targetObject.gameObject.name)
                        };

                    var frameRenderer = frame.gameObject.GetComponent<MeshRenderer>();
                    frameRenderer.sharedMaterial = this.frameMaterial;
                }
            }
            if (EditorExtension.DrawPropertyFieldSafe(this.pictureBaseMaterial, "pictureBaseMaterial"))
            {
                MeshFilter picture = this.pictureMesh != null ? this.pictureMesh.objectReferenceValue as MeshFilter : null;
                if (picture != null)
                {
                    if (this.pictureMaterial == null)
                        this.pictureMaterial = new Material(this.pictureBaseMaterial.objectReferenceValue as Material ?? this.defaultMaterial)
                        {
                            name = string.Format("{0}_Picture-Material", this.targetObject.gameObject.name)
                        };

                    var pictureRenderer = picture.gameObject.GetComponent<MeshRenderer>();
                    pictureRenderer.sharedMaterial = this.pictureMaterial;
                }
            }
            if (EditorExtension.DrawPropertyFieldSafe(this.pictureTexture, "pictureTexture"))
            {
                MeshFilter picture = this.pictureMesh != null ? this.pictureMesh.objectReferenceValue as MeshFilter : null;
                if (picture != null)
                {
                    var pictureRenderer = picture.gameObject.GetComponent<MeshRenderer>();
                    pictureRenderer.sharedMaterial.mainTexture = this.pictureTexture.objectReferenceValue as Texture2D;
                }
            }

            #endregion Materials

            // [ Meshes ]–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Meshes", EditorStyles.boldLabel);
            EditorGUI.BeginDisabledGroup(true);
            EditorExtension.DrawPropertyFieldSafe(this.frameMesh, "frameMesh");
            EditorExtension.DrawPropertyFieldSafe(this.pictureMesh, "pictureMesh");
            EditorGUI.EndDisabledGroup();

            // [ Create Button ]––––––––––––––––––––––––––––––––––––––––––––––––––––––––––

            #region Create Button

            EditorGUILayout.Space();
            if (GUILayout.Button("Create Painting"))
            {
                // Destroy old frame & picture.
                Transform[] children = new Transform[this.targetObject.gameObject.transform.childCount];
                for (int i = 0; i < children.Length; ++i)
                    children[i] = this.targetObject.gameObject.transform.GetChild(i);
                for (int i = 0; i < children.Length; ++i)
                {
                    DestroyImmediate(children[i].gameObject);
                    children[i] = null;
                }

                // Create new frame & picture.
                GameObject frame = GameObject.CreatePrimitive(PrimitiveType.Cube);
                frame.name = "frame";
                GameObject picture = GameObject.CreatePrimitive(PrimitiveType.Quad);
                picture.name = "picture";

                // Parent the objects to the Painting's GameObject.
                frame.transform.SetParent(this.targetObject.gameObject.transform);
                picture.transform.SetParent(this.targetObject.gameObject.transform);

                // Move the objects to the center of their parent.
                frame.transform.localPosition = Vector3.zero;
                picture.transform.localPosition = Vector3.zero;

                // Update scales.
                this.targetObject.UpdatePaintingTransform(fs, ps);

                this.pictureMesh.objectReferenceValue = picture.GetComponent<MeshFilter>();
                this.frameMesh.objectReferenceValue = frame.GetComponent<MeshFilter>();
            }

            #endregion Create Button

            this.serializedObject.ApplyModifiedProperties();
        }
    }
}

#endif