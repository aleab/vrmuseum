﻿#if UNITY_EDITOR

using System.Linq;
using UnityEditor;
using UnityEngine;

namespace VRMuseum.Unity.UnityEditor
{
    /// <summary>
    /// Shows a field for enum-based masks in the Unity Editor.
    /// </summary>
    /// <remarks>
    /// See: http://answers.unity3d.com/questions/393992/custom-inspector-multi-select-enum-dropdown.html
    /// </remarks>
    [CustomPropertyDrawer(typeof(BitMaskAttribute))]
    public class BitMaskAttributeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var typeAttribute = this.attribute as BitMaskAttribute;
            //label.text = string.Format("{0} ({1})", label.text, property.intValue);

            // Get the Tooltip attribute, if exists.
            TooltipAttribute tooltipAttribute = (TooltipAttribute)this.fieldInfo.GetCustomAttributes(typeof(TooltipAttribute), false).FirstOrDefault();
            string tooltip = tooltipAttribute != null ? tooltipAttribute.tooltip : "";

            // Change check is needed to prevent values being overwritten during multiple-selection.
            EditorGUI.BeginChangeCheck();
            int newValue = EditorExtension.DrawBitMaskField(position, property.intValue, typeAttribute.PropertyType, label.text, tooltip);
            if (EditorGUI.EndChangeCheck())
                property.intValue = newValue;
        }
    }
}

#endif