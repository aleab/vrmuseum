﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRMuseum.Extensions;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace VRMuseum.Unity
{
    public class MonoBehaviourExtended : MonoBehaviour
    {
        protected virtual void Awake()
        {
            if (!this.CheckIfRequiredComponentsExist())
                Debug.LogError("Not all required components exist!");
        }

        private bool CheckIfRequiredComponentsExist()
        {
            bool allComponentsFound = true;

            var requiredComponents = this.GetType()
                .GetAttributeValue(
                    (RequireComponentInChildren rcic) => new[] { rcic.m_Type0, rcic.m_Type1, rcic.m_Type2 });
            foreach (var requiredComponent in requiredComponents)
            {
                if (requiredComponent == null)
                    continue;

                bool componentFound = this.GetComponent(requiredComponent) != null || this.transform.Cast<Transform>()
                                          .Any(child => child.gameObject.GetComponent(requiredComponent) != null);
                allComponentsFound &= componentFound;
            }

            return allComponentsFound;
        }

#if UNITY_EDITOR

        private void Reset()
        {
            this.ValidateRequiredComponentsInChildren();
        }

        private void ValidateRequiredComponentsInChildren()
        {
            var requiredComponents = this.GetType().GetAttributeValue((RequireComponentInChildren rcic) => new[] { rcic.m_Type0, rcic.m_Type1, rcic.m_Type2 });

            bool allComponentsFound = true;
            var componentsNotFound = new List<string>();

            foreach (var requiredComponent in requiredComponents)
            {
                if (requiredComponent == null)
                    continue;

                bool componentFound = this.GetComponent(requiredComponent) != null || this.transform.Cast<Transform>().Any(child => child.gameObject.GetComponent(requiredComponent) != null);
                allComponentsFound &= componentFound;

                if (!componentFound)
                    componentsNotFound.Add(string.Format("'{0}'", requiredComponent.Name));
            }

            if (!allComponentsFound)
            {
                string components = string.Join(", ", componentsNotFound.ToArray());
                EditorUtility.DisplayDialog(
                    "Can't add script",
                    string.Format("Adding component failed. Add required components of type {0} to this object or to a child object first.", components),
                    "Ok");

                this.StartCoroutine(this.SelfDestroyImmediate());
            }
        }

        private IEnumerator SelfDestroyImmediate()
        {
            yield return new WaitForSeconds(0.1f);
            DestroyImmediate(this);
        }

#endif
    }
}