﻿using UnityEngine;

namespace VRMuseum
{
    /// <summary>
    /// This class does nothing. It is only used to discern between areas where we can teleport to and areas where we cannot.
    /// </summary>
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer), typeof(Collider))]
    public class TeleportArea : MonoBehaviour { }
}