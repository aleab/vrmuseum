﻿using HtmlAgilityPack;
using System;
using System.Collections;
using System.Text;
using UnityEngine;

namespace VRMuseum
{
    /// <summary>
    /// Handles the download of a painting's data from the web (i.e. the MET Museum website) or
    /// the loading of its fallback/backup data from a previously serialized xml file.
    /// </summary>
    [RequireComponent(typeof(Painting))]
    public class PaintingDataLoader : MonoBehaviour
    {
        public static string BaseUrl { get { return "http://www.metmuseum.org/art/collection/search"; } }

        [Space]
        [SerializeField]
        private Tablet _tablet;

        /// <summary>
        /// The Tablet object the data loaded by this object is going to be displayed on.
        /// </summary>
        public Tablet Tablet { get { return this._tablet; } }

        private Painting Painting { get { return this.gameObject.GetComponent<Painting>(); } }

        public LoadingState State { get; private set; }

        public event EventHandler<PaintingDataEventArgs> DataLoaded;

        private void Start()
        {
            this.State = LoadingState.NA;
        }

        public IEnumerator LoadData()
        {
            // Try downloading from the web.
            yield return this.TryDownloadFromWeb();

            // If unsuccessful, look for the fallback serialized data in the resources.
            if (this.State == LoadingState.Error)
                yield return this.TryLoadFallback();

            // If unsuccessful again, show an error message [and re-try downloading after some time].
            if (this.State == LoadingState.Error)
                Debug.LogErrorFormat("Information loading for painting #{0} failed.", this.Painting.MetId);
        }

        private IEnumerator TryDownloadFromWeb()
        {
            this.State = LoadingState.Downloading;

            string url = string.Format("{0}/{1}", BaseUrl, this.Painting.MetId);
            WWW www = new WWW(url);
            yield return www;

            if (www.error != null)
            {
                this.State = LoadingState.Error;
                Debug.LogErrorFormat("Could not get url content at \"{0}\": {1}", url, www.error);
            }
            else
            {
                // We got something from the web. We need to parse the web page we retrieved to get the info we need.
                PaintingInfo paintingInfo = this.ParseMETHtmlPage(www.text);
                if (this.State != LoadingState.Error)
                {
                    this.State = LoadingState.Loaded;
                    this.OnDataLoaded(paintingInfo, LoadMode.Web);
                }
            }
        }

        private IEnumerator TryLoadFallback()
        {
            this.State = LoadingState.LoadingFallback;

            try
            {
                var serializer = new PaintingInfoSerializer();
                PaintingInfo paintingInfo = serializer.Deserialize(this.Painting.MetId);
                this.State = LoadingState.Loaded;
                this.OnDataLoaded(paintingInfo, LoadMode.Fallback);
            }
            catch (Exception e)
            {
                this.State = LoadingState.Error;
                Debug.LogErrorFormat("Error deserializing the fallback file: {0}", e.Message);
            }

            yield return null;
        }

        /// <summary>
        /// Parse an html page of the MET Museum website looking for a painting's data.
        /// </summary>
        /// <param name="html"> The html page content. </param>
        /// <returns> A <see cref="PaintingInfo"/> object. </returns>
        private PaintingInfo ParseMETHtmlPage(string html)
        {
            // TODO: Add parsing of relevant html tags.
            //       The Description (and potentially other elements) can contain relevant inline html tags (i, b, strong...)
            //       Some of these can be used by Unity's RichText and should not be discarded: we can use InnerHtml, instead of InnerText, and parse that.

            HtmlDocument htmlDocument = new HtmlDocument()
            {
                OptionFixNestedTags = true,
                OptionDefaultStreamEncoding = Encoding.UTF8
            };
            htmlDocument.LoadHtml(html);

            PaintingInfo paintingInfo = new PaintingInfo(this.Painting.MetId);
            try
            {
                // Now, we search the html page and select what we need: the 'tombstone' node, containing all the juicy info about the painting. ;)

                HtmlNode bodyNode = htmlDocument.DocumentNode.SelectSingleNode(".//body");
                HtmlNode tombstoneNode = bodyNode.SelectSingleNode(".//div[@class=\"collection-details__tombstone\"]");
                HtmlNode descriptionNode = tombstoneNode.SelectSingleNode("../div[@class=\"collection-details__label\"]");
                HtmlNode titleNode = tombstoneNode.SelectSingleNode("../h1[@class=\"collection-details__object-title\"]");

                // Title
                string title = HtmlEntity.DeEntitize(titleNode.InnerText);
                paintingInfo.Title = title;

                // Tombstone
                var tombstoneChildren = tombstoneNode.ChildNodes;
                foreach (var child in tombstoneChildren)
                {
                    if (!child.GetAttributeValue("class", "").Equals("collection-details__tombstone--row"))
                        continue;

                    // This is an actual tombstone element.
                    HtmlNode labelNode = child.SelectSingleNode(".//*[@class=\"collection-details__tombstone--label\"]");
                    string label = HtmlEntity.DeEntitize(labelNode.InnerText);

                    HtmlNode valueNode = child.SelectSingleNode(".//*[@class=\"collection-details__tombstone--value\"]");
                    string value = HtmlEntity.DeEntitize(valueNode.InnerText);

                    paintingInfo.Validate(label, value);
                }

                // Description
                string description = HtmlEntity.DeEntitize(descriptionNode.InnerText).Trim();
                paintingInfo.Description = description;
            }
            catch (Exception e)
            {
                this.State = LoadingState.Error;
                Debug.LogErrorFormat("Error parsing the MET html page: {0}", e.Message);
            }

            return paintingInfo;
        }

        private void OnDataLoaded(PaintingInfo paintingInfo, LoadMode mode)
        {
            if (this.DataLoaded != null)
                this.DataLoaded(this, new PaintingDataEventArgs(paintingInfo, mode));
        }

        public enum LoadingState { Loaded, LoadingFallback, Downloading, Error, NA }

        public enum LoadMode { Web, Fallback }
    }

    public class PaintingDataEventArgs : EventArgs
    {
        public PaintingInfo PaintingData { get; private set; }
        
        public PaintingDataLoader.LoadMode LoadMode { get; private set; }

        public PaintingDataEventArgs(PaintingInfo paintingData, PaintingDataLoader.LoadMode mode)
        {
            this.PaintingData = paintingData;
            this.LoadMode = mode;
        }
    }
}