﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace VRMuseum
{
    [RequireComponent(typeof(RectTransform), typeof(Canvas), typeof(CanvasScaler))]
    public class InfoSection : MonoBehaviour
    {
#pragma warning disable 0649

        [SerializeField]
        private TextMeshProUGUI _title;

        [SerializeField]
        private TextMeshProUGUI _body;

#pragma warning restore 0649

        public TextMeshProUGUI Title { get { return this._title; } }
        public TextMeshProUGUI Body { get { return this._body; } }
    }
}