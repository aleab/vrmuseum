﻿using System.Collections;
using UnityEngine;
using VRMuseum.Extensions;

namespace VRMuseum
{
    public enum TeleportType { FadeToBlack, BlinkEmulation }

    /// <summary>
    /// Handles most of the teleport things.
    /// </summary>
    public class Teleporting : MonoBehaviour
    {
        #region Singleton

        private static Teleporting _instance;

        private static Teleporting Instance
        {
            get
            {
                if (_instance == null)
                {
                    var go = new GameObject("TeleportingController");
                    var player = GameObject.FindGameObjectWithTag("Player");
                    go.transform.parent = player != null ? player.transform : null;
                    _instance = go.AddComponent<Teleporting>();
                }
                return _instance;
            }
        }

        #endregion Singleton

        #region Private fields

#pragma warning disable 0649

        [SerializeField]
        private bool isTeleporting;

        [SerializeField]
        private TeleportType teleportType = TeleportType.FadeToBlack;

        private InteractiveController controller;

        #region Pointer

        [SerializeField]
        private GameObject pointerPrefab;

        private bool isPointerVisible;

        private GameObject pointer;

#if UNITY_EDITOR

        [SerializeField]
        private GameObject pointerLinePrefab;

        private GameObject pointerLine;

        [SerializeField]
        private GameObject actualTeleportPositionIndicatorPrefab;

        private GameObject actualTeleportPositionIndicator;

#endif

#pragma warning disable 0649

        #endregion Pointer

        #region Blink animation

        [SerializeField]
        private GameObject blinkPlanesPrefab;

        [SerializeField]
        [Tooltip("The duration of a complete eye blink, in milliseconds.")]
        [Range(50.0f, 500.0f)]
        private int blinkDuration = 250;

        private Animator blinkAnimator;

        #endregion Blink animation

        #endregion Private fields

        #region Static accessors

        public static bool IsTeleporting
        {
            get { return Instance.isTeleporting; }
            set { Instance.isTeleporting = value; }
        }

        public static TeleportType TeleportType
        {
            get { return Instance.teleportType; }
            private set { Instance.teleportType = value; }
        }

        public static InteractiveController UsedController
        {
            get { return Instance.controller; }
            set { Instance.controller = value; }
        }

        /// <summary>
        /// The duration in milliseconds of a complete eye blink.
        /// </summary>
        public static float BlinkDuration
        {
            get { return Instance.blinkDuration; }
        }

        public static bool IsPointerVisible
        {
            get
            {
                return _instance.isPointerVisible;
            }
            set
            {
                _instance.isPointerVisible = value;
                _instance.pointer.SetActive(value);

#if UNITY_EDITOR
                _instance.pointerLine.SetActive(value);
                _instance.actualTeleportPositionIndicator.SetActive(value);
#endif
            }
        }

        #endregion Static accessors

        #region Pointer

        public static void MovePointerTo(Vector3 position, Vector3 normal)
        {
            var rotation = Quaternion.FromToRotation(Vector3.up, normal);

#if UNITY_5_6_OR_NEWER
            _instance.pointer.transform.SetPositionAndRotation(position, rotation);
#else
            _instance.pointer.transform.rotation = rotation;
            _instance.pointer.transform.position = position;
#endif

#if UNITY_EDITOR
            _instance.UpdatePointerLine(position);
            _instance.UpdateActualTeleportPositionIndicator(position);
#endif
        }

        public static void ResetPointer()
        {
            MovePointerTo(_instance.transform.TransformPoint(Vector3.zero), Vector3.up);
            IsPointerVisible = false;

#if UNITY_EDITOR
            _instance.ResetPointerLine();
            _instance.ResetActualTeleportPositionIndicator();
#endif
        }

#if UNITY_EDITOR

        private void UpdatePointerLine(Vector3 destination)
        {
            if (this.controller == null)
                this.ResetPointerLine();
            else
            {
                Vector3 origin = this.controller.TipPosition;
                float distance = Vector3.Distance(origin, destination);
                var rotation = Quaternion.FromToRotation(Vector3.right, destination - origin);

                this.pointerLine.transform.SetGlobalScaleComponent(TransformScaleComponent.X, distance);

#if UNITY_5_6_OR_NEWER
                this.pointerLine.transform.SetPositionAndRotation(origin, rotation);
#else
                this.pointerLine.transform.rotation = rotation;
                this.pointerLine.transform.position = origin;
#endif
            }
        }

        private void UpdateActualTeleportPositionIndicator(Vector3 position)
        {
            Vector3 actualPosition = Player.TryMoveTo(position);
            actualPosition.y = position.y;

            if (this.controller == null)
                this.ResetActualTeleportPositionIndicator();
            else
                _instance.actualTeleportPositionIndicator.transform.position = actualPosition;
        }

        private void ResetPointerLine()
        {
            this.pointerLine.transform.SetGlobalScaleComponent(TransformScaleComponent.X, 0.0f);
            this.pointerLine.transform.localPosition = Vector3.zero;
            this.pointerLine.transform.rotation = Quaternion.identity;
            this.pointerLine.SetActive(false);
        }

        private void ResetActualTeleportPositionIndicator()
        {
            this.actualTeleportPositionIndicator.transform.localPosition = Vector3.zero;
            this.actualTeleportPositionIndicator.SetActive(false);
        }

#endif

        #endregion Pointer

        #region Fade/Blink transitions

        public static void StartFadeToBlackAnimation(float halfFadeDuration)
        {
            Instance.StartCoroutine(FadeToBlack(halfFadeDuration));
        }

        private static IEnumerator FadeToBlack(float halfFadeDuration)
        {
            SteamVR_Fade.Start(Color.clear, 0);
            SteamVR_Fade.Start(Color.black, halfFadeDuration);
            yield return new WaitForSeconds(halfFadeDuration);
            SteamVR_Fade.Start(Color.clear, 0.1f);
        }

        public static void StartBlinkAnimation()
        {
            //Instance.blinkAnimator.speed = 1.0f / (Instance.blinkDuration / 1000.0f);
            Instance.blinkAnimator.SetFloat("BlinkSpeed", 1.0f / (Instance.blinkDuration / 1000.0f));
            Instance.blinkAnimator.SetTrigger("Blink");
        }

        #endregion Fade/Blink transitions

        private void Awake()
        {
            if (_instance == null)
                _instance = this;
            else if (_instance != this)
                Destroy(this.gameObject);
        }

        private void Start()
        {
            // Create the blink planes if needed.
            if (_instance.teleportType == TeleportType.BlinkEmulation && this.blinkPlanesPrefab != null)
            {
                var blinkPlanes = Instantiate(this.blinkPlanesPrefab, Player.NonDiegeticUI.transform);
                blinkPlanes.name = "BlinkPlanes";
                _instance.blinkAnimator = blinkPlanes.GetComponent<Animator>();
            }

            // Create the teleport pointer.
            if (_instance.pointerPrefab != null && _instance.pointer == null)
            {
                _instance.pointer = Instantiate(_instance.pointerPrefab, _instance.transform);
                _instance.pointer.name = "pointer";
                _instance.pointer.transform.localPosition = Vector3.zero;
                _instance.pointer.transform.rotation = Quaternion.identity;
                _instance.isPointerVisible = false;
                _instance.pointer.SetActive(false);
            }

#if UNITY_EDITOR

            // Create the pointer line.
            if (_instance.pointerLinePrefab != null && _instance.pointerLine == null)
            {
                _instance.pointerLine = Instantiate(_instance.pointerLinePrefab, _instance.transform);
                _instance.pointerLine.name = "pointer_line";
                _instance.pointerLine.transform.SetGlobalScaleComponent(TransformScaleComponent.X, 0.0f);
                _instance.pointerLine.transform.localPosition = Vector3.zero;
                _instance.pointerLine.transform.rotation = Quaternion.identity;
                _instance.pointerLine.SetActive(false);
            }

            // Create pointer's center point.
            if (_instance.actualTeleportPositionIndicatorPrefab != null && _instance.actualTeleportPositionIndicator == null)
            {
                _instance.actualTeleportPositionIndicator = Instantiate(_instance.actualTeleportPositionIndicatorPrefab, _instance.transform);
                _instance.actualTeleportPositionIndicator.name = "pointer_actualTeleportPosition";
                _instance.actualTeleportPositionIndicator.transform.SetUniformGlobalScale(0.01f);
                _instance.actualTeleportPositionIndicator.transform.localPosition = Vector3.zero;
                _instance.actualTeleportPositionIndicator.transform.rotation = Quaternion.identity;
                _instance.actualTeleportPositionIndicator.SetActive(false);
            }

#endif
        }

        private void OnDestroy()
        {
            if (_instance == this)
                _instance = null;
        }
    }
}