﻿using System;
using System.Collections;
using UnityEngine;
using VRMuseum.Extensions;

namespace VRMuseum
{
    /// <summary>
    /// This class acts as an intermediary between an <see cref="InteractableObject"/> and a <see cref="SteamVR_TrackedController"/>.
    /// Its purpose is to "translate" the controller's physical events (button presses) to in-game actions on an interactable object.
    /// </summary>
    [RequireComponent(typeof(SteamVR_TrackedController))]
    public class InteractiveController : MonoBehaviour
    {
        // TODO: Custom Editor for SteamVR_TrackedController.

        #region Private fields

        #region Unity Inspector

#pragma warning disable 0649

        [SerializeField]
        private ActionFlags _gripAction;

        [SerializeField]
        private ActionFlags _triggerAction;

        [SerializeField]
        private ActionFlags _padAction;

        [SerializeField]
        [HideInInspector]
        private Transform tip;

        [SerializeField]
        [Range(0.0f, 1.0f)]
        private float hoverSphereRadius = 0.07f;

        [SerializeField]
        private bool debug;

#if UNITY_EDITOR

#pragma warning disable 1635, 0169, 0414

        [SerializeField]
        private Material hoverSphereMaterial;

        [SerializeField]
        [HideInInspector]
        private GameObject hoverSphereObject;

#pragma warning restore 0414, 0169, 1635

#endif

#pragma warning restore 0649

        #endregion Unity Inspector

        // Used for GRAB
        private Collider[] overlappingCollidersBuffer;

        // Used for TELEPORT
        private Coroutine teleportCoroutine;

        private bool isHandlingTeleport;

        private Vector3? currentTeleportDestination;

        #endregion Private fields

        #region Public properties

        /// <summary>
        /// The action mapped to the 'grip' button.
        /// </summary>
        public ActionFlags GripAction { get { return this._gripAction; } }

        /// <summary>
        /// The action mapped to the 'trigger' button.
        /// </summary>
        public ActionFlags TriggerAction { get { return this._triggerAction; } }

        /// <summary>
        /// The action mapped to the 'pad' button.
        /// </summary>
        public ActionFlags PadAction { get { return this._padAction; } }

        /// <summary>
        /// The <see cref="SteamVR_TrackedController"/> object.
        /// </summary>
        public SteamVR_TrackedController TrackedController { get; private set; }

        /// <summary>
        /// The currently grabbed object; only one object can be grabbed at once.
        /// </summary>
        public InteractableObject GrabbedObject { get; private set; }

        /// <summary>
        /// The tip of the controller; used to nicely attach objects to it.
        /// </summary>
        public Vector3 TipPosition { get { return this.tip != null ? this.tip.position : this.transform.position; } }

        #endregion Public properties

        private void Awake()
        {
            this.TrackedController = this.GetComponent<SteamVR_TrackedController>();

            this.tip = this.TrackedController.FindComponent(ViveControllerComponents.Tip) ?? this.transform;

            this.overlappingCollidersBuffer = new Collider[16];

#if UNITY_EDITOR
            if (this.hoverSphereMaterial == null)
                this.hoverSphereMaterial = UnityEditor.AssetDatabase.GetBuiltinExtraResource<Material>("Default-Material.mat");
#endif
        }

        private void OnEnable()
        {
            // Subscribe to SteamVR_TrackedController's events
            this.TrackedController.Gripped += this.TrackedController_Gripped;
            this.TrackedController.Ungripped += this.TrackedController_Ungripped;

            this.TrackedController.TriggerClicked += this.TrackedController_TriggerClicked;
            this.TrackedController.TriggerUnclicked += this.TrackedController_TriggerUnclicked;

            this.TrackedController.PadClicked += this.TrackedController_PadClicked;
            this.TrackedController.PadUnclicked += this.TrackedController_PadUnclicked;
        }

        private void LateUpdate()
        {
            // TODO: Put this in a coroutine.
            // Store colliders inside the 'hovering sphere' in an array, for a possible grab or use action in the next update.
            for (int i = 0; i < this.overlappingCollidersBuffer.Length; ++i)
                this.overlappingCollidersBuffer[i] = null;
            Physics.OverlapSphereNonAlloc(this.tip.position, this.hoverSphereRadius, this.overlappingCollidersBuffer);
        }

        private void OnDisable()
        {
            // Unsubscribe from SteamVR_TrackedController's events
            this.TrackedController.Gripped -= this.TrackedController_Gripped;
            this.TrackedController.Ungripped -= this.TrackedController_Ungripped;

            this.TrackedController.TriggerClicked -= this.TrackedController_TriggerClicked;
            this.TrackedController.TriggerUnclicked -= this.TrackedController_TriggerUnclicked;

            this.TrackedController.PadClicked -= this.TrackedController_PadClicked;
            this.TrackedController.PadUnclicked -= this.TrackedController_PadUnclicked;
        }

        /// <summary>
        /// Attach an <see cref="InteractableObject"/> to this controller/hand.
        /// </summary>
        /// <param name="interactableObject"> The interactable object to attach. </param>
        public void AttachObject(InteractableObject interactableObject)
        {
            /* TODO: Fix the Grab action.
             *       This doesn't really work as expected! Currently, there's a problem with rigidbodies
             *       that have gravity enabled. **Should** be easy to fix, either:
             *       - disable their reaction to gravity when they are grabbed
             *       - create an opposite force
             */

            if (interactableObject == null)
                return;

            this.GrabbedObject = interactableObject;
            interactableObject.gameObject.transform.parent = this.gameObject.transform;
            interactableObject.gameObject.transform.position = this.tip.position;
            interactableObject.gameObject.transform.localRotation = Quaternion.identity;
        }

        /// <summary>
        /// Detach an <see cref="InteractableObject"/> from this controller/hand.
        /// </summary>
        /// <param name="interactableObject"> The interactable object to detach. </param>
        public void DetachObject(InteractableObject interactableObject)
        {
            if (interactableObject == null)
                return;

            if (this.GrabbedObject == interactableObject)
            {
                this.GrabbedObject = null;
                // TODO: [DetachObject] Restore previous parent or drop in the Environment.
                interactableObject.gameObject.transform.parent = null;
            }
        }

        #region Action handlers

        /* TODO: HandleActionStarted and HandleActionEnded are ugly as hell!
         *       Can the whole action handling system benefit from Polymorphism? */

        private void HandleActionStarted(ActionFlags actionFlag)
        {
            // TODO: [HandleActionStarted] Handle Use action.
            switch (actionFlag)
            {
                case ActionFlags.Grab:
                    this.HandleGrab(true);
                    break;

                case ActionFlags.Use:
                    break;

                case ActionFlags.Teleport:
                    if (!Teleporting.IsTeleporting)
                    {
                        Teleporting.IsTeleporting = this.isHandlingTeleport = true;
                        Teleporting.UsedController = this;
                        if (this.teleportCoroutine != null) // not really needed
                            this.StopCoroutine(this.teleportCoroutine);
                        this.teleportCoroutine = this.StartCoroutine(this.UpdateTeleportDestination());
                    }
                    break;

                default:
                    break;
            }
        }

        private void HandleActionEnded(ActionFlags actionFlag)
        {
            // TODO: [HandleActionEnded] Handle Use action.
            switch (actionFlag)
            {
                case ActionFlags.Grab:
                    this.HandleGrab(false);
                    break;

                case ActionFlags.Use:
                    break;

                case ActionFlags.Teleport:
                    if (Teleporting.IsTeleporting && this.isHandlingTeleport)
                    {
                        Teleporting.IsTeleporting = this.isHandlingTeleport = false;
                        Teleporting.UsedController = null;
                        this.StopCoroutine(this.teleportCoroutine);
                        this.teleportCoroutine = null;
                        this.HandleTeleport();
                    }
                    break;

                default:
                    break;
            }
        }

        #region Grab

        private void HandleGrab(bool started)
        {
            if (started && this.GrabbedObject == null)  // [GRAB]
            {
                float closestDistance = float.MaxValue;
                InteractableObject closestInteractable = null;
                foreach (var collider in this.overlappingCollidersBuffer)
                {
                    if (collider == null)
                        continue;

                    InteractableObject interactableObject = collider.GetComponentInParent<InteractableObject>();

                    // Not an InteractableObject.
                    if (interactableObject == null)
                        continue;

                    // Cannot be grabbed.
                    if (!interactableObject.CanBeGrabbed)
                        continue;

                    float distance = Vector3.Distance(this.tip.position, interactableObject.transform.position);
                    if (Mathf.Approximately(distance, closestDistance) && closestInteractable != null)
                    {
                        // If the distance is the same, pick the one that is closest to the controller's direction.
                        float angleOfClosest = Vector3.Angle(this.transform.forward, closestInteractable.transform.position - this.tip.position);
                        float angleOfCurrent = Vector3.Angle(this.transform.forward, interactableObject.transform.position - this.tip.position);
                        if (angleOfCurrent < angleOfClosest)
                        {
                            closestDistance = distance;
                            closestInteractable = interactableObject;
                        }
                    }
                    else if (distance < closestDistance)
                    {
                        closestDistance = distance;
                        closestInteractable = interactableObject;
                    }
                }

                this.AttachObject(closestInteractable);
            }
            else if (!started && this.GrabbedObject != null)  // [DROP]
                this.DetachObject(this.GrabbedObject);

            // The other cases are ignored.
        }

        #endregion Grab

        #region Teleport

        private IEnumerator UpdateTeleportDestination()
        {
            while (true)
            {
                RaycastHit closestHit;
                Ray ray = new Ray(this.tip.position, this.tip.forward);
                if (Physics.Raycast(ray, out closestHit))
                {
                    TeleportArea teleportArea = closestHit.transform.gameObject.GetComponent<TeleportArea>();
                    if (teleportArea != null)
                    {
                        this.currentTeleportDestination = closestHit.point;
                        Teleporting.MovePointerTo(closestHit.point, closestHit.normal);
                        Teleporting.IsPointerVisible = true;
                    }
                    else
                    {
                        this.currentTeleportDestination = null;
                        Teleporting.IsPointerVisible = false;
                    }
                }
                yield return null;
            }
            // ReSharper disable once IteratorNeverReturns
        }

        private void HandleTeleport()
        {
            if (this.currentTeleportDestination != null)
            {
                float halfFadeDuration;

                // Start teleport animation.
                if (Teleporting.TeleportType == TeleportType.BlinkEmulation)
                {
                    // Eye blink transition: https://youtu.be/addUnJpjjv4?t=2405
                    halfFadeDuration = Teleporting.BlinkDuration / 2000.0f;
                    Teleporting.StartBlinkAnimation();
                }
                else
                {
                    // Fade to black in 0.1 seconds and then clear in 0.1 seconds.
                    halfFadeDuration = 0.1f;
                    Teleporting.StartFadeToBlackAnimation(halfFadeDuration);
                }

                // TODO: Teleport sound?

                this.Invoke(this.TeleportPlayer, halfFadeDuration, halfFadeDuration);
            }
        }

        private void TeleportPlayer(float fadeDuration)
        {
            if (this.currentTeleportDestination.HasValue)
                Player.MoveTo(this.currentTeleportDestination.Value);

            this.currentTeleportDestination = null;
            Teleporting.ResetPointer();
        }

        #endregion Teleport

        #endregion Action handlers

        #region Event handlers

        private void TrackedController_Gripped(object sender, ControllerEventArgs e)
        {
            this.HandleActionStarted(this.GripAction);
        }

        private void TrackedController_Ungripped(object sender, ControllerEventArgs e)
        {
            this.HandleActionEnded(this.GripAction);
        }

        private void TrackedController_TriggerClicked(object sender, ControllerEventArgs e)
        {
            this.HandleActionStarted(this.TriggerAction);
        }

        private void TrackedController_TriggerUnclicked(object sender, ControllerEventArgs e)
        {
            this.HandleActionEnded(this.TriggerAction);
        }

        private void TrackedController_PadClicked(object sender, ControllerEventArgs e)
        {
            this.HandleActionStarted(this.PadAction);
        }

        private void TrackedController_PadUnclicked(object sender, ControllerEventArgs e)
        {
            this.HandleActionEnded(this.PadAction);
        }

        #endregion Event handlers

        #region Nested classes and enums

        [Flags]
        public enum ActionFlags
        {
            Nothing = 0,
            Grab = 1 << 1,
            Use = 1 << 2,
            Teleport = 1 << 3,
        }

        #endregion Nested classes and enums

#if UNITY_EDITOR

        private void OnDrawGizmos()
        {
            Gizmos.color = new Color(0.5f, 1.0f, 0.5f, 0.9f);
            Transform sphereTransform = Application.isPlaying ? (this.tip ?? this.transform) : this.transform;
            Gizmos.DrawWireSphere(sphereTransform.position, this.hoverSphereRadius);
        }

#endif
    }
}