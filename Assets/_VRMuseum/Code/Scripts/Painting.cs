﻿using UnityEngine;

namespace VRMuseum
{
    /// <summary>
    /// Just a painting.
    /// </summary>
    public class Painting : MonoBehaviour
    {
        #region Private fields

        [SerializeField]
        [Tooltip("The UID assigned to this painting in the MET website. (www.metmuseum.org/art/collection/search/{ID})")]
        private int _metId;

        private PaintingDataLoader paintingDataLoader;

#pragma warning disable 1635, 0169, 0649, 0414

        [SerializeField]
        private Vector3 frameSize = Vector3.one;

        [SerializeField]
        private Vector2 pictureSize = Vector2.one;

        [SerializeField]
        private Material frameBaseMaterial;

        [SerializeField]
        private Material pictureBaseMaterial;

        [SerializeField]
        private Texture2D pictureTexture;

        [SerializeField]
        private MeshFilter frameMesh;

        [SerializeField]
        private MeshFilter pictureMesh;

#pragma warning restore 0414, 0649, 0169, 1635

        #endregion Private fields

        #region Public properties

        public int MetId
        {
            get { return this._metId; }
        }

        public PaintingInfo PaintingInfo { get; private set; }

        /// <summary>
        /// The total painting's width in centimeters.
        /// </summary>
        public float Width
        {
            get { return this.pictureSize.x + 2 * this.frameSize.x; }
        }

        /// <summary>
        /// The total painting's height in centimeters.
        /// </summary>
        public float Height
        {
            get { return this.pictureSize.y + 2 * this.frameSize.y; }
        }

        #endregion Public properties

        private void Start()
        {
            // TODO: Optimize paintings' data [down]loading.
            //       Currently, every Painting object will try to download/load its data on start;
            //       they might all do it at the same time! Really not good! A simple controller with a queue should be fine.
            this.paintingDataLoader = this.gameObject.GetComponent<PaintingDataLoader>();
            if (this.paintingDataLoader != null)
            {
                this.paintingDataLoader.DataLoaded += this.PaintingDataLoader_DataLoaded;
                this.StartCoroutine(this.paintingDataLoader.LoadData());
            }
        }

        private void OnDestroy()
        {
            this.StopAllCoroutines();
            if (this.paintingDataLoader != null)
                this.paintingDataLoader.DataLoaded -= this.PaintingDataLoader_DataLoaded;
        }

        private void PaintingDataLoader_DataLoaded(object sender, PaintingDataEventArgs e)
        {
            this.PaintingInfo = e.PaintingData;
            this.paintingDataLoader.Tablet.SetInfo(this.PaintingInfo);
        }

#if UNITY_EDITOR

        public void UpdatePaintingTransform()
        {
            this.UpdatePaintingTransform(this.frameSize, this.pictureSize);
        }

        /// <summary>
        /// This method is used by PaintingEditor to update the painting's transform properties.
        /// </summary>
        /// <param name="frameSize"></param>
        /// <param name="pictureSize"></param>
        public void UpdatePaintingTransform(Vector3 frameSize, Vector2 pictureSize)
        {
            // frameSize and pictureSize are scales in world coordinates
            MeshFilter frame = this.frameMesh;
            MeshFilter picture = this.pictureMesh;

            Transform parent = null;

            if (frame != null)
            {
                parent = frame.gameObject.transform.parent;
                Vector3 parentScale = parent != null ? parent.lossyScale : Vector3.one;
                
                frame.gameObject.transform.localScale = new Vector3((pictureSize.x + 2 * frameSize.x) / (Mathf.Approximately(parentScale.x, 0.0f) ? 1.0f : parentScale.x),
                                                                    (pictureSize.y + 2 * frameSize.y) / (Mathf.Approximately(parentScale.y, 0.0f) ? 1.0f : parentScale.y),
                                                                    frameSize.z / (Mathf.Approximately(parentScale.z, 0.0f) ? 1.0f : parentScale.z)) / 100.0f;

                frame.gameObject.transform.localPosition = new Vector3(0.0f,
                                                                       0.0f,
                                                                       (-frameSize.z / 2.0f) / (Mathf.Approximately(parentScale.z, 0.0f) ? 1.0f : parentScale.z)) / 100.0f;

                frame.gameObject.transform.localRotation = Quaternion.identity;
            }

            if (picture != null)
            {
                parent = parent ?? picture.gameObject.transform.parent;
                Vector3 parentScale = parent != null ? parent.lossyScale : Vector3.one;

                picture.gameObject.transform.localScale = new Vector3((pictureSize.x / 100.0f) / (Mathf.Approximately(parentScale.x, 0.0f) ? 1.0f : parentScale.x),
                                                                      (pictureSize.y / 100.0f) / (Mathf.Approximately(parentScale.y, 0.0f) ? 1.0f : parentScale.y),
                                                                      1.0f / (Mathf.Approximately(parentScale.z, 0.0f) ? 1.0f : parentScale.z));

                picture.gameObject.transform.localPosition = new Vector3(0.0f,
                                                                         0.0f,
                                                                         (-frameSize.z - 0.01f) / (Mathf.Approximately(parentScale.z, 0.0f) ? 1.0f : parentScale.z)) / 100.0f;

                picture.gameObject.transform.localRotation = Quaternion.identity;
            }
        }

#endif
    }
}