﻿using System;
using UnityEngine;
using UnityEngine.Events;
using VRMuseum.Unity;

namespace VRMuseum
{
    /// <summary>
    /// An object that can react to the user's gaze and/or to the controllers.
    /// </summary>
    [RequireComponentInChildren(typeof(Collider))]
    public class InteractableObject : MonoBehaviourExtended
    {
        #region Private fields

        [Header("Behaviour")]
        [SerializeField]
        [Tooltip("Whether the object reacts to the user's gaze or not.")]
        private bool _canReactToGaze = true;

        [SerializeField]
        [Tooltip("Whether the object reacts to the controllers' input events or not.")]
        private bool _canReactToInteraction = true;

        [Header("Interactions")]
        [SerializeField]
        [BitMask(typeof(InteractionTypeFlags))]
        [Tooltip("Types of interactions allowed.")]
        private InteractionTypeFlags _allowedInteractions = InteractionTypeFlags.Grab | InteractionTypeFlags.Use;

        [SerializeField]
        [BitMask(typeof(UseTypeFlags))]
        [Tooltip("Types of 'use' interactions allowed.")]
        private UseTypeFlags _allowedUseTypes = UseTypeFlags.UseInWorld;


        private bool? _canBeGrabbed = null;
        private bool? _canBeUsed = null;

        #endregion Private fields

        public bool CanReactToGaze { get { return this._canReactToGaze; } }
        public bool CanReactToInteraction { get { return this._canReactToInteraction; } }

        public bool CanBeGrabbed
        {
            get
            {
                if (!this._canBeGrabbed.HasValue)
                {
                    bool canBeGrabbed = (this.AllowedInteractions & InteractionTypeFlags.Grab) != 0;
                    this._canBeGrabbed = this.CanReactToInteraction && canBeGrabbed;
                }
                return this._canBeGrabbed.Value;
            }
        }

        public bool CanBeUsed
        {
            get
            {
                if (!this._canBeUsed.HasValue)
                {
                    bool canBeUsed = (this.AllowedInteractions & InteractionTypeFlags.Use) != 0;
                    bool canReallyBeUsed = this.AllowedUseTypes != UseTypeFlags.Nothing;
                    this._canBeUsed = this.CanReactToInteraction && canBeUsed && canReallyBeUsed;
                }
                return this._canBeUsed.Value;
            }
        }

        public InteractionTypeFlags AllowedInteractions { get { return this._allowedInteractions; } }
        public UseTypeFlags AllowedUseTypes { get { return this._allowedUseTypes; } }

        public InteractableObjectState CurrentState { get; private set; }

        #region Private events

#pragma warning disable 0649 // `Field '*' is never assigned to, and will always have its default value null`

        [SerializeField]
        [Tooltip("Events that define this object's reaction when the user starts gazing at it.")]
        private GazeUnityEvent gazeEvents;

        [SerializeField]
        [Tooltip("Events that define this object's reaction when it is grabbed.")]
        private GrabInteractionUnityEvent grabInteractionEvents;

        [SerializeField]
        [Tooltip("Events that define this object's reaction when it is used.")]
        private UseInteractionUnityEvent useInteractionEvents;

#pragma warning restore 0649

        #endregion Private events

        #region Public events

        public event EventHandler GazeIn;

        public event EventHandler Gazed;

        public event EventHandler GazeOut;

        public event EventHandler GrabInteractionStarted;

        public event EventHandler GrabInteractionEnded;

        public event EventHandler<UseInteractionEventArgs> UseInteractionStarted;

        public event EventHandler<UseInteractionEventArgs> UseInteractionEnded;

        #endregion Public events

        protected override void Awake()
        {
            base.Awake();
            this.CurrentState = new InteractableObjectState();
        }

        /// <summary>
        /// Gaze this <see cref="InteractableObject"/>.
        /// </summary>
        /// <param name="isGazed"> true if this object is gazed; false if it is not (i.e. GazeOut). </param>
        /// <returns> Returns true if this <see cref="InteractableObject"/> reacted to the gaze event (i.e. only if it can be gazed), otherwise false. </returns>
        public virtual bool Gaze(bool isGazed)
        {
            if (this.CanReactToGaze)
            {
                if (isGazed)
                {
                    if (this.CurrentState.Gazed)
                        this.OnGazed();
                    else
                        this.OnGazeIn();
                }
                else if (this.CurrentState.Gazed)
                    this.OnGazeOut();
            }

            return this.CanReactToGaze;
        }

        /// <summary>
        /// Interact with this <see cref="InteractableObject"/>.
        /// </summary>
        /// <param name="flags"> Interaction flags: an OR'ed combination of <see cref="InteractionTypeFlags"/> and <see cref="UseTypeFlags"/>. </param>
        public virtual void Interact(uint flags)
        {
            if (this.CanReactToInteraction)
            {
                Debug.LogFormat("Trying to interact with \"{0}\"...\n", this.gameObject.name);

                uint outFlags = 0u;
                var oldState = this.CurrentState;

                var requestedInteractionFlags = (InteractionTypeFlags)(flags & (uint)InteractionTypeFlags.Everything);
                var requestedUseTypeFlags = (UseTypeFlags)(flags & (uint)UseTypeFlags.Everything);

                bool grabbed = (requestedInteractionFlags & InteractionTypeFlags.Grab) != 0;
                bool used = (requestedInteractionFlags & InteractionTypeFlags.Use) != 0;

                // GRAB
                if (this.CanBeGrabbed)
                {
                    if (grabbed)
                    {
                        Debug.LogFormat("{0}\n{1}", string.Format("\tGrab interaction with \"{0}\".", this.gameObject.name),
                                                    string.Format("\tWas already grabbed: {0}", oldState.Grabbed));

                        if (!oldState.Grabbed)
                            this.OnGrabInteractionStarted();
                        else
                        {
                            // Still holding down the 'grab' button.
                        }

                        outFlags |= (uint)InteractionTypeFlags.Grab;
                    }
                    else
                    {
                        if (oldState.Grabbed)
                            this.OnGrabInteractionEnded();
                    }
                }


                // USE
                if (this.CanBeUsed)
                {
                    // Only one UseType flag can be set.
                    bool requestedUseTypeIsAllowed = (requestedUseTypeFlags & this.AllowedUseTypes) != 0;  // this will be != 0 only if the requested UseType is allowed.
                    if (used)
                    {
                        Debug.LogFormat("{0}\n{1}", string.Format("\tUse interaction of type `{1}` with \"{0}\".", this.gameObject.name, requestedUseTypeFlags.ToString()),
                                                    string.Format("\tWas already being used: {0}; Allowed use types: `{1}`", oldState.Used, this.AllowedUseTypes.ToString()));

                        if (requestedUseTypeIsAllowed)
                        {
                            Debug.LogFormat("\t\tUse interaction event of type: `{0}`.\n", requestedUseTypeFlags.ToString());

                            if (!oldState.Used)
                                this.OnUseInteractionStarted(requestedUseTypeFlags);
                            else
                            {
                                // Still holding down the 'use' button.
                            }

                            outFlags |= (uint)InteractionTypeFlags.Use | (uint)requestedUseTypeFlags;
                        }
                    }
                    else
                    {
                        if (requestedUseTypeIsAllowed)
                        {
                            if (oldState.Used)
                                this.OnUseInteractionEnded(requestedUseTypeFlags);
                        }
                    }
                }

                // Update current state.
                this.CurrentState.SetInteractionFlags(outFlags);
            }
        }

        #region Event handlers

        private void OnGazeIn()
        {
            if (this.gazeEvents != null)
                this.gazeEvents.Invoke();
            if (this.GazeIn != null)
                this.GazeIn(this, EventArgs.Empty);
        }

        private void OnGazed()
        {
            if (this.Gazed != null)
                this.Gazed(this, EventArgs.Empty);
        }

        private void OnGazeOut()
        {
            if (this.GazeOut != null)
                this.GazeOut(this, EventArgs.Empty);
        }

        private void OnGrabInteractionStarted()
        {
            if (this.grabInteractionEvents != null)
                this.grabInteractionEvents.Invoke();
            if (this.GrabInteractionStarted != null)
                this.GrabInteractionStarted(this, EventArgs.Empty);
        }

        private void OnGrabInteractionEnded()
        {
            if (this.GrabInteractionEnded != null)
                this.GrabInteractionEnded(this, EventArgs.Empty);
        }

        private void OnUseInteractionStarted(UseTypeFlags useType)
        {
            var e = new UseInteractionEventArgs(useType);
            if (this.useInteractionEvents != null)
                this.useInteractionEvents.Invoke(useType);
            if (this.UseInteractionStarted != null)
                this.UseInteractionStarted(this, e);
        }

        private void OnUseInteractionEnded(UseTypeFlags useType)
        {
            var e = new UseInteractionEventArgs(useType);
            if (this.UseInteractionEnded != null)
                this.UseInteractionEnded(this, e);
        }

        #endregion Event handlers


        #region Nested classes and enums

        /// <summary>
        /// Determines what types of interaction an <see cref="InteractableObject"/> reacts to.
        /// </summary>
        /// <remarks>
        /// This enum's flags must not overlap with <see cref="UseTypeFlags"/>'s.
        /// </remarks>
        [Flags]
        public enum InteractionTypeFlags : uint
        {
            /// <summary>
            /// No interaction.
            /// </summary>
            Nothing = 0,

            /// <summary>
            /// The object can be grabbed, i.e. attached to the 'hand'.
            /// </summary>
            Grab = (1 << 0),

            /// <summary>
            /// The object can be 'used'.
            /// </summary>
            Use = (1 << 1),

            Everything = Grab | Use
        }

        /// <summary>
        /// Determines what types of 'use' interaction an <see cref="InteractableObject"/> reacts to.
        /// </summary>
        /// <remarks>
        /// This enum's flags must not overlap with <see cref="InteractionTypeFlags"/>'s.
        /// </remarks>
        [Flags]
        public enum UseTypeFlags : uint
        {
            Nothing = 0,

            /// <summary>
            /// The object can be 'used' when it is not grabbed.
            /// </summary>
            UseInWorld = (1 << 2),

            /// <summary>
            /// The object can be 'used' with the same 'hand' it is grabbed with.
            /// </summary>
            UseInHoldingHand = (1 << 3),

            /// <summary>
            /// The object can be 'used' with the 'hand' it is not grabbed with.
            /// </summary>
            UseInOtherHand = (1 << 4),

            Everything = UseInWorld | UseInHoldingHand | UseInOtherHand
        }

        // TODO: Is InteractionEventArgs really needed?
        public class InteractionEventArgs : EventArgs
        {
            public InteractionTypeFlags InteractionType { get; private set; }

            public InteractionEventArgs(InteractionTypeFlags interactionType)
            {
                this.InteractionType = interactionType;
            }
        }

        // TODO: Is UseInteractionEventArgs really needed?
        public class UseInteractionEventArgs : InteractionEventArgs
        {
            public UseTypeFlags UseType { get; private set; }

            public UseInteractionEventArgs(UseTypeFlags useType) : base(InteractionTypeFlags.Use)
            {
                this.UseType = useType;
            }
        }

        [Serializable]
        private class GazeUnityEvent : UnityEvent { }

        [Serializable]
        private class GrabInteractionUnityEvent : UnityEvent { }

        [Serializable]
        private class UseInteractionUnityEvent : UnityEvent<UseTypeFlags> { }

        #endregion Nested classes and enums


#if UNITY_EDITOR

        [HideInInspector]
        [NonSerialized]
        public bool showEvents = false;

#endif
    }
}