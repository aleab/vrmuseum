﻿using TMPro;
using UnityEngine;

namespace VRMuseum
{
    /// <summary>
    /// The Tablet that is going to display a painting's information.
    /// </summary>
    public class Tablet : MonoBehaviour
    {
#pragma warning disable 0649

        [SerializeField]
        private TextMeshProUGUI title;

        [SerializeField]
        private TextMeshProUGUI date;

        [SerializeField]
        private TextMeshProUGUI medium;

        [SerializeField]
        private TextMeshProUGUI description;

        [SerializeField]
        private TextMeshProUGUI notice;

#pragma warning restore 0649

        public void SetInfo(PaintingInfo paintingInfo)
        {
            this.title.text = paintingInfo.Title;
            this.date.text = paintingInfo.Date;
            this.medium.text = paintingInfo.Medium;
            this.description.text = paintingInfo.Description;
            this.notice.text = string.Format("{0}/{1}", PaintingDataLoader.BaseUrl, paintingInfo.Id);
        }
    }
}