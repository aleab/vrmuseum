﻿using System;
using UnityEngine;

namespace VRMuseum
{
    public class Room : MonoBehaviour
    {
        #region Private fields

        public const float BORDERS_WIDTH = 2 * 3.0f;
        public const float PASSAGE_WIDTH = 150.0f;

        public const float LEFT_PAINTINGS_TOTAL_WIDTH = 273.1f + 5 * BORDERS_WIDTH;
        public const float MIDDLELEFT_PAINTINGS_TOTAL_WIDTH = 271.6f + 4 * BORDERS_WIDTH;
        public const float MIDDLERIGHT_PAINTINGS_TOTAL_WIDTH = 160.6f + 3 * BORDERS_WIDTH;
        public const float RIGHT_PAINTINGS_TOTAL_WIDTH = 259.5f + 4 * BORDERS_WIDTH;

#pragma warning disable 1635, 0169, 0414  // [field never used]

        [SerializeField]
        private Vector3 roomSize = Vector3.one;

#pragma warning disable 0649  // [always default null value]

        #region Room Walls

        [SerializeField]
        private GameObject ceiling;

        [SerializeField]
        private GameObject leftWall;

        [SerializeField]
        private GameObject rightWall;

        [SerializeField]
        private GameObject middleWall;

        [SerializeField]
        private GameObject entranceWall;

        [SerializeField]
        private GameObject backWall;

        [SerializeField]
        private GameObject floor;

        [SerializeField]
        private GameObject middleWallLeft;

        [SerializeField]
        private GameObject middleWallRight;

        #endregion Room Walls

        #region Info Sections

        [SerializeField]
        [Range(150.0f, 250.0f)]
        private float infoSectionsHeight = 200.0f;

        [SerializeField]
        [Range(0.5f, 5.0f)]
        private float infoTextResolutionMultiplier = 2.0f;

        [SerializeField]
        private InfoSection leftInfoSection;

        [SerializeField]
        private InfoSection rightInfoSection;

        #endregion Info Sections

#pragma warning restore 0649

        #region Physical Dimensions

        [SerializeField]
        [Range(240.0f, 400.0f)]
        private float ceilingHeight = 320.0f;

        [SerializeField]
        [Range(130.0f, 180.0f)]
        private float paintingsHeight = 160.0f;

        [Space(4.0f)]
        [SerializeField]
        [Range(10.0f, 20.0f)]
        private float middleWallThickness = 11.5f;

        [SerializeField]
        [Range(150.0f, 350.0f)]
        private float corridorsWidth = 250.0f;

        [Space(4.0f)]
        [SerializeField]
        [Range(20.0f, 100.0f)]
        private float infoSectionCornerDistance = 45.0f;

        [SerializeField]
        [Range(50.0f, 200.0f)]
        private float entranceInfoWidth = 100.0f;

        [SerializeField]
        [Range(20.0f, 100.0f)]
        private float leftPaintingsDistance = 60.0f;

        [SerializeField]
        [Range(40.0f, 150.0f)]
        private float leftCornerDistance = 65.0f;

        #endregion Physical Dimensions

        [Space(4.0f)]
        [SerializeField]
        [Range(50.0f, 200.0f)]
        private float exitInfoWidth = 100.0f;

        [SerializeField]
        [Range(40.0f, 100.0f)]
        private float minRightCornerDistance = 50.0f;

        // Right

        [SerializeField]
        private float rightPaintingsDistance;

        [SerializeField]
        private float rightCornerDistance;

        [SerializeField]
        private float rightInfoPaintingsDistance;


        [SerializeField]
        [HideInInspector]
        private float k;

        [SerializeField]
        [HideInInspector]
        private float maxK;

#pragma warning restore 0414, 0169, 1635

        #endregion Private fields

#if UNITY_EDITOR

        [HideInInspector]
        [NonSerialized]
        public bool roomComponentsFoldedOut = false;

#endif
    }
}