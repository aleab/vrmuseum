﻿using System;
using System.IO;
using UnityEngine;

namespace VRMuseum
{
    public class ScreenshotController : MonoBehaviour
    {
        [SerializeField]
        [Range(1.0f, 8.0f)]
        private int multiplier = 1;

        [SerializeField]
        private KeyCode keyCode = KeyCode.K;

        private void LateUpdate()
        {
            if (Input.GetKeyDown(this.keyCode))
            {
                string path = string.Format("{0}/__screenshots", Application.dataPath);
                Directory.CreateDirectory(path);
                Application.CaptureScreenshot(string.Format("{0}/{1:yyyy-MM-dd_HH-mm-ss}.png", path, DateTime.Now), this.multiplier);
            }
        }
    }
}