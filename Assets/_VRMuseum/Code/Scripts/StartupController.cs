﻿using UnityEngine;
using LoadMode = VRMuseum.PaintingDataLoader.LoadMode;

namespace VRMuseum
{
    public class StartupController : MonoBehaviour
    {
#pragma warning disable 0649

#if UNITY_EDITOR

        [Space]
        [SerializeField]
        private bool serializePaintingsInfo;

        private Painting[] paintings;

#endif

#pragma warning disable 0649

        private void Awake()
        {
#if UNITY_EDITOR
            if (this.serializePaintingsInfo)
            {
                this.paintings = FindObjectsOfType<Painting>();
                foreach (var painting in this.paintings)
                {
                    PaintingDataLoader paintingDataLoader = painting.gameObject.GetComponent<PaintingDataLoader>();
                    paintingDataLoader.DataLoaded += this.PaintingDataLoader_DataLoaded;
                }
            }
#endif
        }

#if UNITY_EDITOR

        private void PaintingDataLoader_DataLoaded(object sender, PaintingDataEventArgs e)
        {
            if (e.LoadMode == LoadMode.Web)
            {
                PaintingInfoSerializer serializer = new PaintingInfoSerializer();
                serializer.Serialize(e.PaintingData, "Assets/_VRMuseum/Resources");
            }
        }

#endif
    }
}