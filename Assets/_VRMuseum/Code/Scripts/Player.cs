﻿using UnityEngine;
using UnityEngine.UI;

namespace VRMuseum
{
    public class Player : MonoBehaviour
    {
        #region Singleton

        private static Player _instance;

        #endregion Singleton

        #region Private fields

        [Space]
        [SerializeField]
        private SteamVR_ControllerManager cameraRig;

        [SerializeField]
        private SteamVR_TrackedObject head;

        [SerializeField]
        private SteamVR_Camera eye;

        [SerializeField]
        private SteamVR_Ears ears;


        private Canvas nonDiegeticUI;

        #endregion Private fields

        #region Static accessors

        public static SteamVR_ControllerManager CameraRig
        {
            get
            {
                if (_instance.cameraRig == null)
                    _instance.cameraRig = FindObjectOfType<SteamVR_ControllerManager>();
                return _instance.cameraRig;
            }
        }

        public static SteamVR_TrackedObject Head
        {
            get
            {
                if (_instance.head == null)
                    _instance.head = CameraRig.transform.FindChild("Camera (head)").gameObject.GetComponent<SteamVR_TrackedObject>();
                return _instance.head;
            }
        }

        public static SteamVR_Camera Eye
        {
            get
            {
                if (_instance.eye == null)
                    _instance.eye = FindObjectOfType<SteamVR_Camera>();
                return _instance.eye;
            }
        }

        public static SteamVR_Ears Ears
        {
            get
            {
                if (_instance.ears == null)
                    _instance.ears = FindObjectOfType<SteamVR_Ears>();
                return _instance.ears;
            }
        }

        public static SteamVR_TrackedController LeftController
        {
            get
            {
                return CameraRig.left.gameObject.GetComponent<SteamVR_TrackedController>();
            }
        }

        public static SteamVR_TrackedController RightController
        {
            get
            {
                return CameraRig.right.gameObject.GetComponent<SteamVR_TrackedController>();
            }
        }

        /// <summary>
        /// Guess the player height based on the position of the headset.
        /// </summary>
        public static float Height
        {
            get
            {
                if (CameraRig != null)
                    return Vector3.Project(CameraRig.transform.position, Vector3.up).magnitude + 0.02f;
                return 0.0f;
            }
        }


        public static Canvas NonDiegeticUI
        {
            get
            {
                if (_instance.nonDiegeticUI == null)
                {
                    GameObject go = new GameObject("NonDiegeticUI", typeof(RectTransform), typeof(Canvas), typeof(CanvasScaler));
                    go.transform.SetParent(Eye.transform, false);
                    go.layer = LayerMask.NameToLayer("UI");

                    var canvas = go.GetComponent<Canvas>();
                    var canvasScaler = go.GetComponent<CanvasScaler>();

                    canvas.renderMode = RenderMode.ScreenSpaceCamera;
                    canvas.pixelPerfect = false;
                    canvas.worldCamera = Eye.gameObject.GetComponent<Camera>();
                    canvas.planeDistance = 0.011f;
                    canvas.sortingLayerID = SortingLayer.NameToID("GUI");

                    canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;
                    canvasScaler.scaleFactor = 1.0f;
                    canvasScaler.referencePixelsPerUnit = 1.0f;

                    _instance.nonDiegeticUI = canvas;
                }
                return _instance.nonDiegeticUI;
            }
        }

        #endregion Static accessors

        public static void MoveTo(Vector3 position)
        {
            // The head cannot be moved directly: we have to move the CameraRig in a way that the head will end up in 'position'.
            Vector3 offset = CameraRig.gameObject.transform.position - Head.gameObject.transform.position;
            offset.y = 0;
            CameraRig.gameObject.transform.position = position + offset;
        }

        /// <summary>
        /// Tries to move the player to a position and returns the Head position.
        /// </summary>
        /// <returns> The position of the head after the movement. </returns>
        public static Vector3 TryMoveTo(Vector3 position)
        {
            // TODO: this might cause some strange effects to the player? Needs testing.

            Vector3 previousCameraRigPosition = CameraRig.gameObject.transform.position;
            Vector3 previousHeadPosition = Head.gameObject.transform.position;

            Vector3 offset = CameraRig.gameObject.transform.position - Head.gameObject.transform.position;
            offset.y = 0;

            SteamVR_Fade.Start(Color.clear, 0.0f);
            SteamVR_Fade.Start(Color.black, 0.0f);

            CameraRig.gameObject.transform.position = position + offset;
            Vector3 headPosition = Head.gameObject.transform.position;
            CameraRig.gameObject.transform.position = previousCameraRigPosition;

            SteamVR_Fade.Start(Color.clear, 0.0f);

            Debug.LogFormat("[Player.TryMoveTo] Before: CameraRig = {0}, Head = {1}\n[Player.TryMoveTo] After: CameraRig = {2}, Head = {3}",
                            previousCameraRigPosition.ToString(), previousHeadPosition,
                            CameraRig.gameObject.transform.position, Head.gameObject.transform.position);

            return headPosition;
        }

        private void Awake()
        {
            if (_instance == null)
                _instance = this;
            else if (_instance != this)
                Destroy(this.gameObject);

            //DontDestroyOnLoad(this.gameObject);
        }
    }
}