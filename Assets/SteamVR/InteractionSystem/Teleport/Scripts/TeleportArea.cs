﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: An area that the player can teleport to
//
//=============================================================================

using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class TeleportArea : TeleportMarkerBase
    {
        //Public properties
        public Bounds meshBounds { get; private set; }

        //Private data
        private MeshRenderer areaMesh;

        private int tintColorId = 0;
        private Color visibleTintColor = Color.clear;
        private Color highlightedTintColor = Color.clear;
        private Color lockedTintColor = Color.clear;
        private bool highlighted = false;

        //-------------------------------------------------
        public void Awake()
        {
            this.areaMesh = GetComponent<MeshRenderer>();

            this.tintColorId = Shader.PropertyToID("_TintColor");

            CalculateBounds();
        }

        //-------------------------------------------------
        public void Start()
        {
            this.visibleTintColor = Teleport.instance.areaVisibleMaterial.GetColor(this.tintColorId);
            this.highlightedTintColor = Teleport.instance.areaHighlightedMaterial.GetColor(this.tintColorId);
            this.lockedTintColor = Teleport.instance.areaLockedMaterial.GetColor(this.tintColorId);
        }

        //-------------------------------------------------
        public override bool ShouldActivate(Vector3 playerPosition)
        {
            return true;
        }

        //-------------------------------------------------
        public override bool ShouldMovePlayer()
        {
            return true;
        }

        //-------------------------------------------------
        public override void Highlight(bool highlight)
        {
            if (!this.locked)
            {
                this.highlighted = highlight;

                if (highlight)
                {
                    this.areaMesh.material = Teleport.instance.areaHighlightedMaterial;
                }
                else
                {
                    this.areaMesh.material = Teleport.instance.areaVisibleMaterial;
                }
            }
        }

        //-------------------------------------------------
        public override void SetAlpha(float tintAlpha, float alphaPercent)
        {
            Color tintedColor = GetTintColor();
            tintedColor.a *= alphaPercent;
            this.areaMesh.material.SetColor(this.tintColorId, tintedColor);
        }

        //-------------------------------------------------
        public override void UpdateVisuals()
        {
            if (this.locked)
            {
                this.areaMesh.material = Teleport.instance.areaLockedMaterial;
            }
            else
            {
                this.areaMesh.material = Teleport.instance.areaVisibleMaterial;
            }
        }

        //-------------------------------------------------
        public void UpdateVisualsInEditor()
        {
            this.areaMesh = GetComponent<MeshRenderer>();

            if (this.locked)
            {
                this.areaMesh.sharedMaterial = Teleport.instance.areaLockedMaterial;
            }
            else
            {
                this.areaMesh.sharedMaterial = Teleport.instance.areaVisibleMaterial;
            }
        }

        //-------------------------------------------------
        private bool CalculateBounds()
        {
            MeshFilter meshFilter = GetComponent<MeshFilter>();
            if (meshFilter == null)
            {
                return false;
            }

            Mesh mesh = meshFilter.sharedMesh;
            if (mesh == null)
            {
                return false;
            }

            this.meshBounds = mesh.bounds;
            return true;
        }

        //-------------------------------------------------
        private Color GetTintColor()
        {
            if (this.locked)
            {
                return this.lockedTintColor;
            }
            else
            {
                if (this.highlighted)
                {
                    return this.highlightedTintColor;
                }
                else
                {
                    return this.visibleTintColor;
                }
            }
        }
    }

#if UNITY_EDITOR

    //-------------------------------------------------------------------------
    [CustomEditor(typeof(TeleportArea))]
    public class TeleportAreaEditor : Editor
    {
        //-------------------------------------------------
        private void OnEnable()
        {
            if (Selection.activeTransform != null)
            {
                TeleportArea teleportArea = Selection.activeTransform.GetComponent<TeleportArea>();
                if (teleportArea != null)
                {
                    teleportArea.UpdateVisualsInEditor();
                }
            }
        }

        //-------------------------------------------------
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (Selection.activeTransform != null)
            {
                TeleportArea teleportArea = Selection.activeTransform.GetComponent<TeleportArea>();
                if (GUI.changed && teleportArea != null)
                {
                    teleportArea.UpdateVisualsInEditor();
                }
            }
        }
    }

#endif
}