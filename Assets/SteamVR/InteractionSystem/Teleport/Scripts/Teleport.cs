﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Handles all the teleport logic
//
//=============================================================================

using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class Teleport : MonoBehaviour
    {
        public LayerMask traceLayerMask;
        public LayerMask floorFixupTraceLayerMask;
        public float floorFixupMaximumTraceDistance = 1.0f;
        public Material areaVisibleMaterial;
        public Material areaLockedMaterial;
        public Material areaHighlightedMaterial;
        public Material pointVisibleMaterial;
        public Material pointLockedMaterial;
        public Material pointHighlightedMaterial;
        public Transform destinationReticleTransform;
        public Transform invalidReticleTransform;
        public GameObject playAreaPreviewCorner;
        public GameObject playAreaPreviewSide;
        public Color pointerValidColor;
        public Color pointerInvalidColor;
        public Color pointerLockedColor;
        public bool showPlayAreaMarker = true;

        public float teleportFadeTime = 0.1f;
        public float meshFadeTime = 0.2f;

        public float arcDistance = 10.0f;

        [Header("Effects")]
        public Transform onActivateObjectTransform;

        public Transform onDeactivateObjectTransform;
        public float activateObjectTime = 1.0f;
        public float deactivateObjectTime = 1.0f;

        [Header("Audio Sources")]
        public AudioSource pointerAudioSource;

        public AudioSource loopingAudioSource;
        public AudioSource headAudioSource;
        public AudioSource reticleAudioSource;

        [Header("Sounds")]
        public AudioClip teleportSound;

        public AudioClip pointerStartSound;
        public AudioClip pointerLoopSound;
        public AudioClip pointerStopSound;
        public AudioClip goodHighlightSound;
        public AudioClip badHighlightSound;

        [Header("Debug")]
        public bool debugFloor = false;

        public bool showOffsetReticle = false;
        public Transform offsetReticleTransform;
        public MeshRenderer floorDebugSphere;
        public LineRenderer floorDebugLine;

        private LineRenderer pointerLineRenderer;
        private GameObject teleportPointerObject;
        private Transform pointerStartTransform;
        private Hand pointerHand = null;
        private Player player = null;
        private TeleportArc teleportArc = null;

        private bool visible = false;

        private TeleportMarkerBase[] teleportMarkers;
        private TeleportMarkerBase pointedAtTeleportMarker;
        private TeleportMarkerBase teleportingToMarker;
        private Vector3 pointedAtPosition;
        private Vector3 prevPointedAtPosition;
        private bool teleporting = false;
        private float currentFadeTime = 0.0f;

        private float meshAlphaPercent = 1.0f;
        private float pointerShowStartTime = 0.0f;
        private float pointerHideStartTime = 0.0f;
        private bool meshFading = false;
        private float fullTintAlpha;

        private float invalidReticleMinScale = 0.2f;
        private float invalidReticleMaxScale = 1.0f;
        private float invalidReticleMinScaleDistance = 0.4f;
        private float invalidReticleMaxScaleDistance = 2.0f;
        private Vector3 invalidReticleScale = Vector3.one;
        private Quaternion invalidReticleTargetRotation = Quaternion.identity;

        private Transform playAreaPreviewTransform;
        private Transform[] playAreaPreviewCorners;
        private Transform[] playAreaPreviewSides;

        private float loopingAudioMaxVolume = 0.0f;

        private Coroutine hintCoroutine = null;

        private bool originalHoverLockState = false;
        private Interactable originalHoveringInteractable = null;
        private AllowTeleportWhileAttachedToHand allowTeleportWhileAttached = null;

        private Vector3 startingFeetOffset = Vector3.zero;
        private bool movedFeetFarEnough = false;

        private SteamVR_Events.Action chaperoneInfoInitializedAction;

        // Events

        public static SteamVR_Events.Event<float> ChangeScene = new SteamVR_Events.Event<float>();

        public static SteamVR_Events.Action<float> ChangeSceneAction(UnityAction<float> action)
        {
            return new SteamVR_Events.Action<float>(ChangeScene, action);
        }

        public static SteamVR_Events.Event<TeleportMarkerBase> Player = new SteamVR_Events.Event<TeleportMarkerBase>();

        public static SteamVR_Events.Action<TeleportMarkerBase> PlayerAction(UnityAction<TeleportMarkerBase> action)
        {
            return new SteamVR_Events.Action<TeleportMarkerBase>(Player, action);
        }

        public static SteamVR_Events.Event<TeleportMarkerBase> PlayerPre = new SteamVR_Events.Event<TeleportMarkerBase>();

        public static SteamVR_Events.Action<TeleportMarkerBase> PlayerPreAction(UnityAction<TeleportMarkerBase> action)
        {
            return new SteamVR_Events.Action<TeleportMarkerBase>(PlayerPre, action);
        }

        //-------------------------------------------------
        private static Teleport _instance;

        public static Teleport instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<Teleport>();
                }

                return _instance;
            }
        }

        //-------------------------------------------------
        private void Awake()
        {
            _instance = this;

            this.chaperoneInfoInitializedAction = ChaperoneInfo.InitializedAction(this.OnChaperoneInfoInitialized);

            this.pointerLineRenderer = GetComponentInChildren<LineRenderer>();
            this.teleportPointerObject = this.pointerLineRenderer.gameObject;

            int tintColorID = Shader.PropertyToID("_TintColor");
            this.fullTintAlpha = this.pointVisibleMaterial.GetColor(tintColorID).a;

            this.teleportArc = GetComponent<TeleportArc>();
            this.teleportArc.traceLayerMask = this.traceLayerMask;

            this.loopingAudioMaxVolume = this.loopingAudioSource.volume;

            this.playAreaPreviewCorner.SetActive(false);
            this.playAreaPreviewSide.SetActive(false);

            float invalidReticleStartingScale = this.invalidReticleTransform.localScale.x;
            this.invalidReticleMinScale *= invalidReticleStartingScale;
            this.invalidReticleMaxScale *= invalidReticleStartingScale;
        }

        //-------------------------------------------------
        private void Start()
        {
            this.teleportMarkers = GameObject.FindObjectsOfType<TeleportMarkerBase>();

            HidePointer();

            this.player = InteractionSystem.Player.instance;

            if (this.player == null)
            {
                Debug.LogError("Teleport: No Player instance found in map.");
                Destroy(this.gameObject);
                return;
            }

            CheckForSpawnPoint();

            Invoke("ShowTeleportHint", 5.0f);
        }

        //-------------------------------------------------
        private void OnEnable()
        {
            this.chaperoneInfoInitializedAction.enabled = true;
            OnChaperoneInfoInitialized(); // In case it's already initialized
        }

        //-------------------------------------------------
        private void OnDisable()
        {
            this.chaperoneInfoInitializedAction.enabled = false;
            HidePointer();
        }

        //-------------------------------------------------
        private void CheckForSpawnPoint()
        {
            foreach (TeleportMarkerBase teleportMarker in this.teleportMarkers)
            {
                TeleportPoint teleportPoint = teleportMarker as TeleportPoint;
                if (teleportPoint && teleportPoint.playerSpawnPoint)
                {
                    this.teleportingToMarker = teleportMarker;
                    TeleportPlayer();
                    break;
                }
            }
        }

        //-------------------------------------------------
        public void HideTeleportPointer()
        {
            if (this.pointerHand != null)
            {
                HidePointer();
            }
        }

        //-------------------------------------------------
        private void Update()
        {
            Hand oldPointerHand = this.pointerHand;
            Hand newPointerHand = null;

            foreach (Hand hand in this.player.hands)
            {
                if (this.visible)
                {
                    if (WasTeleportButtonReleased(hand))
                    {
                        if (this.pointerHand == hand) //This is the pointer hand
                        {
                            TryTeleportPlayer();
                        }
                    }
                }

                if (WasTeleportButtonPressed(hand))
                {
                    newPointerHand = hand;
                }
            }

            //If something is attached to the hand that is preventing teleport
            if (this.allowTeleportWhileAttached && !this.allowTeleportWhileAttached.teleportAllowed)
            {
                HidePointer();
            }
            else
            {
                if (!this.visible && newPointerHand != null)
                {
                    //Begin showing the pointer
                    ShowPointer(newPointerHand, oldPointerHand);
                }
                else if (this.visible)
                {
                    if (newPointerHand == null && !IsTeleportButtonDown(this.pointerHand))
                    {
                        //Hide the pointer
                        HidePointer();
                    }
                    else if (newPointerHand != null)
                    {
                        //Move the pointer to a new hand
                        ShowPointer(newPointerHand, oldPointerHand);
                    }
                }
            }

            if (this.visible)
            {
                UpdatePointer();

                if (this.meshFading)
                {
                    UpdateTeleportColors();
                }

                if (this.onActivateObjectTransform.gameObject.activeSelf && Time.time - this.pointerShowStartTime > this.activateObjectTime)
                {
                    this.onActivateObjectTransform.gameObject.SetActive(false);
                }
            }
            else
            {
                if (this.onDeactivateObjectTransform.gameObject.activeSelf && Time.time - this.pointerHideStartTime > this.deactivateObjectTime)
                {
                    this.onDeactivateObjectTransform.gameObject.SetActive(false);
                }
            }
        }

        //-------------------------------------------------
        private void UpdatePointer()
        {
            Vector3 pointerStart = this.pointerStartTransform.position;
            Vector3 pointerEnd;
            Vector3 pointerDir = this.pointerStartTransform.forward;
            bool hitSomething = false;
            bool showPlayAreaPreview = false;
            Vector3 playerFeetOffset = this.player.trackingOriginTransform.position - this.player.feetPositionGuess;

            Vector3 arcVelocity = pointerDir * this.arcDistance;

            TeleportMarkerBase hitTeleportMarker = null;

            //Check pointer angle
            float dotUp = Vector3.Dot(pointerDir, Vector3.up);
            float dotForward = Vector3.Dot(pointerDir, this.player.hmdTransform.forward);
            bool pointerAtBadAngle = false;
            if ((dotForward > 0 && dotUp > 0.75f) || (dotForward < 0.0f && dotUp > 0.5f))
            {
                pointerAtBadAngle = true;
            }

            //Trace to see if the pointer hit anything
            RaycastHit hitInfo;
            this.teleportArc.SetArcData(pointerStart, arcVelocity, true, pointerAtBadAngle);
            if (this.teleportArc.DrawArc(out hitInfo))
            {
                hitSomething = true;
                hitTeleportMarker = hitInfo.collider.GetComponentInParent<TeleportMarkerBase>();
            }

            if (pointerAtBadAngle)
            {
                hitTeleportMarker = null;
            }

            HighlightSelected(hitTeleportMarker);

            if (hitTeleportMarker != null) //Hit a teleport marker
            {
                if (hitTeleportMarker.locked)
                {
                    this.teleportArc.SetColor(this.pointerLockedColor);
#if (UNITY_5_4)
					pointerLineRenderer.SetColors( pointerLockedColor, pointerLockedColor );
#else
                    this.pointerLineRenderer.startColor = this.pointerLockedColor;
                    this.pointerLineRenderer.endColor = this.pointerLockedColor;
#endif
                    this.destinationReticleTransform.gameObject.SetActive(false);
                }
                else
                {
                    this.teleportArc.SetColor(this.pointerValidColor);
#if (UNITY_5_4)
					pointerLineRenderer.SetColors( pointerValidColor, pointerValidColor );
#else
                    this.pointerLineRenderer.startColor = this.pointerValidColor;
                    this.pointerLineRenderer.endColor = this.pointerValidColor;
#endif
                    this.destinationReticleTransform.gameObject.SetActive(hitTeleportMarker.showReticle);
                }

                this.offsetReticleTransform.gameObject.SetActive(true);

                this.invalidReticleTransform.gameObject.SetActive(false);

                this.pointedAtTeleportMarker = hitTeleportMarker;
                this.pointedAtPosition = hitInfo.point;

                if (this.showPlayAreaMarker)
                {
                    //Show the play area marker if this is a teleport area
                    TeleportArea teleportArea = this.pointedAtTeleportMarker as TeleportArea;
                    if (teleportArea != null && !teleportArea.locked && this.playAreaPreviewTransform != null)
                    {
                        Vector3 offsetToUse = playerFeetOffset;

                        //Adjust the actual offset to prevent the play area marker from moving too much
                        if (!this.movedFeetFarEnough)
                        {
                            float distanceFromStartingOffset = Vector3.Distance(playerFeetOffset, this.startingFeetOffset);
                            if (distanceFromStartingOffset < 0.1f)
                            {
                                offsetToUse = this.startingFeetOffset;
                            }
                            else if (distanceFromStartingOffset < 0.4f)
                            {
                                offsetToUse = Vector3.Lerp(this.startingFeetOffset, playerFeetOffset, (distanceFromStartingOffset - 0.1f) / 0.3f);
                            }
                            else
                            {
                                this.movedFeetFarEnough = true;
                            }
                        }

                        this.playAreaPreviewTransform.position = this.pointedAtPosition + offsetToUse;

                        showPlayAreaPreview = true;
                    }
                }

                pointerEnd = hitInfo.point;
            }
            else //Hit neither
            {
                this.destinationReticleTransform.gameObject.SetActive(false);
                this.offsetReticleTransform.gameObject.SetActive(false);

                this.teleportArc.SetColor(this.pointerInvalidColor);
#if (UNITY_5_4)
				pointerLineRenderer.SetColors( pointerInvalidColor, pointerInvalidColor );
#else
                this.pointerLineRenderer.startColor = this.pointerInvalidColor;
                this.pointerLineRenderer.endColor = this.pointerInvalidColor;
#endif
                this.invalidReticleTransform.gameObject.SetActive(!pointerAtBadAngle);

                //Orient the invalid reticle to the normal of the trace hit point
                Vector3 normalToUse = hitInfo.normal;
                float angle = Vector3.Angle(hitInfo.normal, Vector3.up);
                if (angle < 15.0f)
                {
                    normalToUse = Vector3.up;
                }
                this.invalidReticleTargetRotation = Quaternion.FromToRotation(Vector3.up, normalToUse);
                this.invalidReticleTransform.rotation = Quaternion.Slerp(this.invalidReticleTransform.rotation, this.invalidReticleTargetRotation, 0.1f);

                //Scale the invalid reticle based on the distance from the player
                float distanceFromPlayer = Vector3.Distance(hitInfo.point, this.player.hmdTransform.position);
                float invalidReticleCurrentScale = Util.RemapNumberClamped(distanceFromPlayer, this.invalidReticleMinScaleDistance, this.invalidReticleMaxScaleDistance, this.invalidReticleMinScale, this.invalidReticleMaxScale);
                this.invalidReticleScale.x = invalidReticleCurrentScale;
                this.invalidReticleScale.y = invalidReticleCurrentScale;
                this.invalidReticleScale.z = invalidReticleCurrentScale;
                this.invalidReticleTransform.transform.localScale = this.invalidReticleScale;

                this.pointedAtTeleportMarker = null;

                if (hitSomething)
                {
                    pointerEnd = hitInfo.point;
                }
                else
                {
                    pointerEnd = this.teleportArc.GetArcPositionAtTime(this.teleportArc.arcDuration);
                }

                //Debug floor
                if (this.debugFloor)
                {
                    this.floorDebugSphere.gameObject.SetActive(false);
                    this.floorDebugLine.gameObject.SetActive(false);
                }
            }

            if (this.playAreaPreviewTransform != null)
            {
                this.playAreaPreviewTransform.gameObject.SetActive(showPlayAreaPreview);
            }

            if (!this.showOffsetReticle)
            {
                this.offsetReticleTransform.gameObject.SetActive(false);
            }

            this.destinationReticleTransform.position = this.pointedAtPosition;
            this.invalidReticleTransform.position = pointerEnd;
            this.onActivateObjectTransform.position = pointerEnd;
            this.onDeactivateObjectTransform.position = pointerEnd;
            this.offsetReticleTransform.position = pointerEnd - playerFeetOffset;

            this.reticleAudioSource.transform.position = this.pointedAtPosition;

            this.pointerLineRenderer.SetPosition(0, pointerStart);
            this.pointerLineRenderer.SetPosition(1, pointerEnd);
        }

        //-------------------------------------------------
        private void FixedUpdate()
        {
            if (!this.visible)
            {
                return;
            }

            if (this.debugFloor)
            {
                //Debug floor
                TeleportArea teleportArea = this.pointedAtTeleportMarker as TeleportArea;
                if (teleportArea != null)
                {
                    if (this.floorFixupMaximumTraceDistance > 0.0f)
                    {
                        this.floorDebugSphere.gameObject.SetActive(true);
                        this.floorDebugLine.gameObject.SetActive(true);

                        RaycastHit raycastHit;
                        Vector3 traceDir = Vector3.down;
                        traceDir.x = 0.01f;
                        if (Physics.Raycast(this.pointedAtPosition + 0.05f * traceDir, traceDir, out raycastHit, this.floorFixupMaximumTraceDistance, this.floorFixupTraceLayerMask))
                        {
                            this.floorDebugSphere.transform.position = raycastHit.point;
                            this.floorDebugSphere.material.color = Color.green;
#if (UNITY_5_4)
							floorDebugLine.SetColors( Color.green, Color.green );
#else
                            this.floorDebugLine.startColor = Color.green;
                            this.floorDebugLine.endColor = Color.green;
#endif
                            this.floorDebugLine.SetPosition(0, this.pointedAtPosition);
                            this.floorDebugLine.SetPosition(1, raycastHit.point);
                        }
                        else
                        {
                            Vector3 rayEnd = this.pointedAtPosition + (traceDir * this.floorFixupMaximumTraceDistance);
                            this.floorDebugSphere.transform.position = rayEnd;
                            this.floorDebugSphere.material.color = Color.red;
#if (UNITY_5_4)
							floorDebugLine.SetColors( Color.red, Color.red );
#else
                            this.floorDebugLine.startColor = Color.red;
                            this.floorDebugLine.endColor = Color.red;
#endif
                            this.floorDebugLine.SetPosition(0, this.pointedAtPosition);
                            this.floorDebugLine.SetPosition(1, rayEnd);
                        }
                    }
                }
            }
        }

        //-------------------------------------------------
        private void OnChaperoneInfoInitialized()
        {
            ChaperoneInfo chaperone = ChaperoneInfo.instance;

            if (chaperone.initialized && chaperone.roomscale)
            {
                //Set up the render model for the play area bounds

                if (this.playAreaPreviewTransform == null)
                {
                    this.playAreaPreviewTransform = new GameObject("PlayAreaPreviewTransform").transform;
                    this.playAreaPreviewTransform.parent = this.transform;
                    Util.ResetTransform(this.playAreaPreviewTransform);

                    this.playAreaPreviewCorner.SetActive(true);
                    this.playAreaPreviewCorners = new Transform[4];
                    this.playAreaPreviewCorners[0] = this.playAreaPreviewCorner.transform;
                    this.playAreaPreviewCorners[1] = Instantiate(this.playAreaPreviewCorners[0]);
                    this.playAreaPreviewCorners[2] = Instantiate(this.playAreaPreviewCorners[0]);
                    this.playAreaPreviewCorners[3] = Instantiate(this.playAreaPreviewCorners[0]);

                    this.playAreaPreviewCorners[0].transform.parent = this.playAreaPreviewTransform;
                    this.playAreaPreviewCorners[1].transform.parent = this.playAreaPreviewTransform;
                    this.playAreaPreviewCorners[2].transform.parent = this.playAreaPreviewTransform;
                    this.playAreaPreviewCorners[3].transform.parent = this.playAreaPreviewTransform;

                    this.playAreaPreviewSide.SetActive(true);
                    this.playAreaPreviewSides = new Transform[4];
                    this.playAreaPreviewSides[0] = this.playAreaPreviewSide.transform;
                    this.playAreaPreviewSides[1] = Instantiate(this.playAreaPreviewSides[0]);
                    this.playAreaPreviewSides[2] = Instantiate(this.playAreaPreviewSides[0]);
                    this.playAreaPreviewSides[3] = Instantiate(this.playAreaPreviewSides[0]);

                    this.playAreaPreviewSides[0].transform.parent = this.playAreaPreviewTransform;
                    this.playAreaPreviewSides[1].transform.parent = this.playAreaPreviewTransform;
                    this.playAreaPreviewSides[2].transform.parent = this.playAreaPreviewTransform;
                    this.playAreaPreviewSides[3].transform.parent = this.playAreaPreviewTransform;
                }

                float x = chaperone.playAreaSizeX;
                float z = chaperone.playAreaSizeZ;

                this.playAreaPreviewSides[0].localPosition = new Vector3(0.0f, 0.0f, 0.5f * z - 0.25f);
                this.playAreaPreviewSides[1].localPosition = new Vector3(0.0f, 0.0f, -0.5f * z + 0.25f);
                this.playAreaPreviewSides[2].localPosition = new Vector3(0.5f * x - 0.25f, 0.0f, 0.0f);
                this.playAreaPreviewSides[3].localPosition = new Vector3(-0.5f * x + 0.25f, 0.0f, 0.0f);

                this.playAreaPreviewSides[0].localScale = new Vector3(x - 0.5f, 1.0f, 1.0f);
                this.playAreaPreviewSides[1].localScale = new Vector3(x - 0.5f, 1.0f, 1.0f);
                this.playAreaPreviewSides[2].localScale = new Vector3(z - 0.5f, 1.0f, 1.0f);
                this.playAreaPreviewSides[3].localScale = new Vector3(z - 0.5f, 1.0f, 1.0f);

                this.playAreaPreviewSides[0].localRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
                this.playAreaPreviewSides[1].localRotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
                this.playAreaPreviewSides[2].localRotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);
                this.playAreaPreviewSides[3].localRotation = Quaternion.Euler(0.0f, 270.0f, 0.0f);

                this.playAreaPreviewCorners[0].localPosition = new Vector3(0.5f * x - 0.25f, 0.0f, 0.5f * z - 0.25f);
                this.playAreaPreviewCorners[1].localPosition = new Vector3(0.5f * x - 0.25f, 0.0f, -0.5f * z + 0.25f);
                this.playAreaPreviewCorners[2].localPosition = new Vector3(-0.5f * x + 0.25f, 0.0f, -0.5f * z + 0.25f);
                this.playAreaPreviewCorners[3].localPosition = new Vector3(-0.5f * x + 0.25f, 0.0f, 0.5f * z - 0.25f);

                this.playAreaPreviewCorners[0].localRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
                this.playAreaPreviewCorners[1].localRotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);
                this.playAreaPreviewCorners[2].localRotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
                this.playAreaPreviewCorners[3].localRotation = Quaternion.Euler(0.0f, 270.0f, 0.0f);

                this.playAreaPreviewTransform.gameObject.SetActive(false);
            }
        }

        //-------------------------------------------------
        private void HidePointer()
        {
            if (this.visible)
            {
                this.pointerHideStartTime = Time.time;
            }

            this.visible = false;
            if (this.pointerHand)
            {
                if (ShouldOverrideHoverLock())
                {
                    //Restore the original hovering interactable on the hand
                    if (this.originalHoverLockState == true)
                    {
                        this.pointerHand.HoverLock(this.originalHoveringInteractable);
                    }
                    else
                    {
                        this.pointerHand.HoverUnlock(null);
                    }
                }

                //Stop looping sound
                this.loopingAudioSource.Stop();
                PlayAudioClip(this.pointerAudioSource, this.pointerStopSound);
            }
            this.teleportPointerObject.SetActive(false);

            this.teleportArc.Hide();

            foreach (TeleportMarkerBase teleportMarker in this.teleportMarkers)
            {
                if (teleportMarker != null && teleportMarker.markerActive && teleportMarker.gameObject != null)
                {
                    teleportMarker.gameObject.SetActive(false);
                }
            }

            this.destinationReticleTransform.gameObject.SetActive(false);
            this.invalidReticleTransform.gameObject.SetActive(false);
            this.offsetReticleTransform.gameObject.SetActive(false);

            if (this.playAreaPreviewTransform != null)
            {
                this.playAreaPreviewTransform.gameObject.SetActive(false);
            }

            if (this.onActivateObjectTransform.gameObject.activeSelf)
            {
                this.onActivateObjectTransform.gameObject.SetActive(false);
            }
            this.onDeactivateObjectTransform.gameObject.SetActive(true);

            this.pointerHand = null;
        }

        //-------------------------------------------------
        private void ShowPointer(Hand newPointerHand, Hand oldPointerHand)
        {
            if (!this.visible)
            {
                this.pointedAtTeleportMarker = null;
                this.pointerShowStartTime = Time.time;
                this.visible = true;
                this.meshFading = true;

                this.teleportPointerObject.SetActive(false);
                this.teleportArc.Show();

                foreach (TeleportMarkerBase teleportMarker in this.teleportMarkers)
                {
                    if (teleportMarker.markerActive && teleportMarker.ShouldActivate(this.player.feetPositionGuess))
                    {
                        teleportMarker.gameObject.SetActive(true);
                        teleportMarker.Highlight(false);
                    }
                }

                this.startingFeetOffset = this.player.trackingOriginTransform.position - this.player.feetPositionGuess;
                this.movedFeetFarEnough = false;

                if (this.onDeactivateObjectTransform.gameObject.activeSelf)
                {
                    this.onDeactivateObjectTransform.gameObject.SetActive(false);
                }
                this.onActivateObjectTransform.gameObject.SetActive(true);

                this.loopingAudioSource.clip = this.pointerLoopSound;
                this.loopingAudioSource.loop = true;
                this.loopingAudioSource.Play();
                this.loopingAudioSource.volume = 0.0f;
            }

            if (oldPointerHand)
            {
                if (ShouldOverrideHoverLock())
                {
                    //Restore the original hovering interactable on the hand
                    if (this.originalHoverLockState == true)
                    {
                        oldPointerHand.HoverLock(this.originalHoveringInteractable);
                    }
                    else
                    {
                        oldPointerHand.HoverUnlock(null);
                    }
                }
            }

            this.pointerHand = newPointerHand;

            if (this.visible && oldPointerHand != this.pointerHand)
            {
                PlayAudioClip(this.pointerAudioSource, this.pointerStartSound);
            }

            if (this.pointerHand)
            {
                this.pointerStartTransform = GetPointerStartTransform(this.pointerHand);

                if (this.pointerHand.currentAttachedObject != null)
                {
                    this.allowTeleportWhileAttached = this.pointerHand.currentAttachedObject.GetComponent<AllowTeleportWhileAttachedToHand>();
                }

                //Keep track of any existing hovering interactable on the hand
                this.originalHoverLockState = this.pointerHand.hoverLocked;
                this.originalHoveringInteractable = this.pointerHand.hoveringInteractable;

                if (ShouldOverrideHoverLock())
                {
                    this.pointerHand.HoverLock(null);
                }

                this.pointerAudioSource.transform.SetParent(this.pointerStartTransform);
                this.pointerAudioSource.transform.localPosition = Vector3.zero;

                this.loopingAudioSource.transform.SetParent(this.pointerStartTransform);
                this.loopingAudioSource.transform.localPosition = Vector3.zero;
            }
        }

        //-------------------------------------------------
        private void UpdateTeleportColors()
        {
            float deltaTime = Time.time - this.pointerShowStartTime;
            if (deltaTime > this.meshFadeTime)
            {
                this.meshAlphaPercent = 1.0f;
                this.meshFading = false;
            }
            else
            {
                this.meshAlphaPercent = Mathf.Lerp(0.0f, 1.0f, deltaTime / this.meshFadeTime);
            }

            //Tint color for the teleport points
            foreach (TeleportMarkerBase teleportMarker in this.teleportMarkers)
            {
                teleportMarker.SetAlpha(this.fullTintAlpha * this.meshAlphaPercent, this.meshAlphaPercent);
            }
        }

        //-------------------------------------------------
        private void PlayAudioClip(AudioSource source, AudioClip clip)
        {
            source.clip = clip;
            source.Play();
        }

        //-------------------------------------------------
        private void PlayPointerHaptic(bool validLocation)
        {
            if (this.pointerHand.controller != null)
            {
                if (validLocation)
                {
                    this.pointerHand.controller.TriggerHapticPulse(800);
                }
                else
                {
                    this.pointerHand.controller.TriggerHapticPulse(100);
                }
            }
        }

        //-------------------------------------------------
        private void TryTeleportPlayer()
        {
            if (this.visible && !this.teleporting)
            {
                if (this.pointedAtTeleportMarker != null && this.pointedAtTeleportMarker.locked == false)
                {
                    //Pointing at an unlocked teleport marker
                    this.teleportingToMarker = this.pointedAtTeleportMarker;
                    InitiateTeleportFade();

                    CancelTeleportHint();
                }
            }
        }

        //-------------------------------------------------
        private void InitiateTeleportFade()
        {
            this.teleporting = true;

            this.currentFadeTime = this.teleportFadeTime;

            TeleportPoint teleportPoint = this.teleportingToMarker as TeleportPoint;
            if (teleportPoint != null && teleportPoint.teleportType == TeleportPoint.TeleportPointType.SwitchToNewScene)
            {
                this.currentFadeTime *= 3.0f;
                Teleport.ChangeScene.Send(this.currentFadeTime);
            }

            SteamVR_Fade.Start(Color.clear, 0);
            SteamVR_Fade.Start(Color.black, this.currentFadeTime);

            this.headAudioSource.transform.SetParent(this.player.hmdTransform);
            this.headAudioSource.transform.localPosition = Vector3.zero;
            PlayAudioClip(this.headAudioSource, this.teleportSound);

            Invoke("TeleportPlayer", this.currentFadeTime);
        }

        //-------------------------------------------------
        private void TeleportPlayer()
        {
            this.teleporting = false;

            Teleport.PlayerPre.Send(this.pointedAtTeleportMarker);

            SteamVR_Fade.Start(Color.clear, this.currentFadeTime);

            TeleportPoint teleportPoint = this.teleportingToMarker as TeleportPoint;
            Vector3 teleportPosition = this.pointedAtPosition;

            if (teleportPoint != null)
            {
                teleportPosition = teleportPoint.transform.position;

                //Teleport to a new scene
                if (teleportPoint.teleportType == TeleportPoint.TeleportPointType.SwitchToNewScene)
                {
                    teleportPoint.TeleportToScene();
                    return;
                }
            }

            // Find the actual floor position below the navigation mesh
            TeleportArea teleportArea = this.teleportingToMarker as TeleportArea;
            if (teleportArea != null)
            {
                if (this.floorFixupMaximumTraceDistance > 0.0f)
                {
                    RaycastHit raycastHit;
                    if (Physics.Raycast(teleportPosition + 0.05f * Vector3.down, Vector3.down, out raycastHit, this.floorFixupMaximumTraceDistance, this.floorFixupTraceLayerMask))
                    {
                        teleportPosition = raycastHit.point;
                    }
                }
            }

            if (this.teleportingToMarker.ShouldMovePlayer())
            {
                Vector3 playerFeetOffset = this.player.trackingOriginTransform.position - this.player.feetPositionGuess;
                this.player.trackingOriginTransform.position = teleportPosition + playerFeetOffset;
            }
            else
            {
                this.teleportingToMarker.TeleportPlayer(this.pointedAtPosition);
            }

            Teleport.Player.Send(this.pointedAtTeleportMarker);
        }

        //-------------------------------------------------
        private void HighlightSelected(TeleportMarkerBase hitTeleportMarker)
        {
            if (this.pointedAtTeleportMarker != hitTeleportMarker) //Pointing at a new teleport marker
            {
                if (this.pointedAtTeleportMarker != null)
                {
                    this.pointedAtTeleportMarker.Highlight(false);
                }

                if (hitTeleportMarker != null)
                {
                    hitTeleportMarker.Highlight(true);

                    this.prevPointedAtPosition = this.pointedAtPosition;
                    PlayPointerHaptic(!hitTeleportMarker.locked);

                    PlayAudioClip(this.reticleAudioSource, this.goodHighlightSound);

                    this.loopingAudioSource.volume = this.loopingAudioMaxVolume;
                }
                else if (this.pointedAtTeleportMarker != null)
                {
                    PlayAudioClip(this.reticleAudioSource, this.badHighlightSound);

                    this.loopingAudioSource.volume = 0.0f;
                }
            }
            else if (hitTeleportMarker != null) //Pointing at the same teleport marker
            {
                if (Vector3.Distance(this.prevPointedAtPosition, this.pointedAtPosition) > 1.0f)
                {
                    this.prevPointedAtPosition = this.pointedAtPosition;
                    PlayPointerHaptic(!hitTeleportMarker.locked);
                }
            }
        }

        //-------------------------------------------------
        public void ShowTeleportHint()
        {
            CancelTeleportHint();

            this.hintCoroutine = StartCoroutine(TeleportHintCoroutine());
        }

        //-------------------------------------------------
        public void CancelTeleportHint()
        {
            if (this.hintCoroutine != null)
            {
                foreach (Hand hand in this.player.hands)
                {
                    ControllerButtonHints.HideTextHint(hand, EVRButtonId.k_EButton_SteamVR_Touchpad);
                }

                StopCoroutine(this.hintCoroutine);
                this.hintCoroutine = null;
            }

            CancelInvoke("ShowTeleportHint");
        }

        //-------------------------------------------------
        private IEnumerator TeleportHintCoroutine()
        {
            float prevBreakTime = Time.time;
            float prevHapticPulseTime = Time.time;

            while (true)
            {
                bool pulsed = false;

                //Show the hint on each eligible hand
                foreach (Hand hand in this.player.hands)
                {
                    bool showHint = IsEligibleForTeleport(hand);
                    bool isShowingHint = !string.IsNullOrEmpty(ControllerButtonHints.GetActiveHintText(hand, EVRButtonId.k_EButton_SteamVR_Touchpad));
                    if (showHint)
                    {
                        if (!isShowingHint)
                        {
                            ControllerButtonHints.ShowTextHint(hand, EVRButtonId.k_EButton_SteamVR_Touchpad, "Teleport");
                            prevBreakTime = Time.time;
                            prevHapticPulseTime = Time.time;
                        }

                        if (Time.time > prevHapticPulseTime + 0.05f)
                        {
                            //Haptic pulse for a few seconds
                            pulsed = true;

                            hand.controller.TriggerHapticPulse(500);
                        }
                    }
                    else if (!showHint && isShowingHint)
                    {
                        ControllerButtonHints.HideTextHint(hand, EVRButtonId.k_EButton_SteamVR_Touchpad);
                    }
                }

                if (Time.time > prevBreakTime + 3.0f)
                {
                    //Take a break for a few seconds
                    yield return new WaitForSeconds(3.0f);

                    prevBreakTime = Time.time;
                }

                if (pulsed)
                {
                    prevHapticPulseTime = Time.time;
                }

                yield return null;
            }
        }

        //-------------------------------------------------
        public bool IsEligibleForTeleport(Hand hand)
        {
            if (hand == null)
            {
                return false;
            }

            if (!hand.gameObject.activeInHierarchy)
            {
                return false;
            }

            if (hand.hoveringInteractable != null)
            {
                return false;
            }

            if (hand.noSteamVRFallbackCamera == null)
            {
                if (hand.controller == null)
                {
                    return false;
                }

                //Something is attached to the hand
                if (hand.currentAttachedObject != null)
                {
                    AllowTeleportWhileAttachedToHand allowTeleportWhileAttachedToHand = hand.currentAttachedObject.GetComponent<AllowTeleportWhileAttachedToHand>();

                    if (allowTeleportWhileAttachedToHand != null && allowTeleportWhileAttachedToHand.teleportAllowed == true)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        //-------------------------------------------------
        private bool ShouldOverrideHoverLock()
        {
            if (!this.allowTeleportWhileAttached || this.allowTeleportWhileAttached.overrideHoverLock)
            {
                return true;
            }

            return false;
        }

        //-------------------------------------------------
        private bool WasTeleportButtonReleased(Hand hand)
        {
            if (IsEligibleForTeleport(hand))
            {
                if (hand.noSteamVRFallbackCamera != null)
                {
                    return Input.GetKeyUp(KeyCode.T);
                }
                else
                {
                    return hand.controller.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad);
                }
            }

            return false;
        }

        //-------------------------------------------------
        private bool IsTeleportButtonDown(Hand hand)
        {
            if (IsEligibleForTeleport(hand))
            {
                if (hand.noSteamVRFallbackCamera != null)
                {
                    return Input.GetKey(KeyCode.T);
                }
                else
                {
                    return hand.controller.GetPress(SteamVR_Controller.ButtonMask.Touchpad);
                }
            }

            return false;
        }

        //-------------------------------------------------
        private bool WasTeleportButtonPressed(Hand hand)
        {
            if (IsEligibleForTeleport(hand))
            {
                if (hand.noSteamVRFallbackCamera != null)
                {
                    return Input.GetKeyDown(KeyCode.T);
                }
                else
                {
                    return hand.controller.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad);
                }
            }

            return false;
        }

        //-------------------------------------------------
        private Transform GetPointerStartTransform(Hand hand)
        {
            if (hand.noSteamVRFallbackCamera != null)
            {
                return hand.noSteamVRFallbackCamera.transform;
            }
            else
            {
                return this.pointerHand.GetAttachmentTransform("Attach_ControllerTip");
            }
        }
    }
}