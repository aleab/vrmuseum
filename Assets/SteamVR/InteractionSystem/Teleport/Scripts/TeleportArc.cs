﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Displays the arc lines for teleporting and does the traces
//
//=============================================================================

using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class TeleportArc : MonoBehaviour
    {
        public int segmentCount = 60;
        public float thickness = 0.01f;

        [Tooltip("The amount of time in seconds to predict the motion of the projectile.")]
        public float arcDuration = 3.0f;

        [Tooltip("The amount of time in seconds between each segment of the projectile.")]
        public float segmentBreak = 0.025f;

        [Tooltip("The speed at which the line segments of the arc move.")]
        public float arcSpeed = 0.2f;

        public Material material;

        [HideInInspector]
        public int traceLayerMask = 0;

        //Private data
        private LineRenderer[] lineRenderers;

        private float arcTimeOffset = 0.0f;
        private float prevThickness = 0.0f;
        private int prevSegmentCount = 0;
        private bool showArc = true;
        private Vector3 startPos;
        private Vector3 projectileVelocity;
        private bool useGravity = true;
        private Transform arcObjectsTransfrom;
        private bool arcInvalid = false;

        //-------------------------------------------------
        private void Start()
        {
            this.arcTimeOffset = Time.time;
        }

        //-------------------------------------------------
        private void Update()
        {
            if (this.thickness != this.prevThickness || this.segmentCount != this.prevSegmentCount)
            {
                CreateLineRendererObjects();
                this.prevThickness = this.thickness;
                this.prevSegmentCount = this.segmentCount;
            }
        }

        //-------------------------------------------------
        private void CreateLineRendererObjects()
        {
            //Destroy any existing line renderer objects
            if (this.arcObjectsTransfrom != null)
            {
                Destroy(this.arcObjectsTransfrom.gameObject);
            }

            GameObject arcObjectsParent = new GameObject("ArcObjects");
            this.arcObjectsTransfrom = arcObjectsParent.transform;
            this.arcObjectsTransfrom.SetParent(this.transform);

            //Create new line renderer objects
            this.lineRenderers = new LineRenderer[this.segmentCount];
            for (int i = 0; i < this.segmentCount; ++i)
            {
                GameObject newObject = new GameObject("LineRenderer_" + i);
                newObject.transform.SetParent(this.arcObjectsTransfrom);

                this.lineRenderers[i] = newObject.AddComponent<LineRenderer>();

                this.lineRenderers[i].receiveShadows = false;
                this.lineRenderers[i].reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;
                this.lineRenderers[i].lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;
                this.lineRenderers[i].shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                this.lineRenderers[i].material = this.material;
#if (UNITY_5_4)
				lineRenderers[i].SetWidth( thickness, thickness );
#else
                this.lineRenderers[i].startWidth = this.thickness;
                this.lineRenderers[i].endWidth = this.thickness;
#endif
                this.lineRenderers[i].enabled = false;
            }
        }

        //-------------------------------------------------
        public void SetArcData(Vector3 position, Vector3 velocity, bool gravity, bool pointerAtBadAngle)
        {
            this.startPos = position;
            this.projectileVelocity = velocity;
            this.useGravity = gravity;

            if (this.arcInvalid && !pointerAtBadAngle)
            {
                this.arcTimeOffset = Time.time;
            }
            this.arcInvalid = pointerAtBadAngle;
        }

        //-------------------------------------------------
        public void Show()
        {
            this.showArc = true;
            if (this.lineRenderers == null)
            {
                CreateLineRendererObjects();
            }
        }

        //-------------------------------------------------
        public void Hide()
        {
            //Hide the line segments if they were previously being shown
            if (this.showArc)
            {
                HideLineSegments(0, this.segmentCount);
            }
            this.showArc = false;
        }

        //-------------------------------------------------
        // Draws each segment of the arc individually
        //-------------------------------------------------
        public bool DrawArc(out RaycastHit hitInfo)
        {
            float timeStep = this.arcDuration / this.segmentCount;

            float currentTimeOffset = (Time.time - this.arcTimeOffset) * this.arcSpeed;

            //Reset the arc time offset when it has gone beyond a segment length
            if (currentTimeOffset > (timeStep + this.segmentBreak))
            {
                this.arcTimeOffset = Time.time;
                currentTimeOffset = 0.0f;
            }

            float segmentStartTime = currentTimeOffset;

            float arcHitTime = FindProjectileCollision(out hitInfo);

            if (this.arcInvalid)
            {
                //Only draw first segment
                this.lineRenderers[0].enabled = true;
                this.lineRenderers[0].SetPosition(0, GetArcPositionAtTime(0.0f));
                this.lineRenderers[0].SetPosition(1, GetArcPositionAtTime(arcHitTime < timeStep ? arcHitTime : timeStep));

                HideLineSegments(1, this.segmentCount);
            }
            else
            {
                //Draw the first segment outside the loop if needed
                int loopStartSegment = 0;
                if (segmentStartTime > this.segmentBreak)
                {
                    float firstSegmentEndTime = currentTimeOffset - this.segmentBreak;
                    if (arcHitTime < firstSegmentEndTime)
                    {
                        firstSegmentEndTime = arcHitTime;
                    }
                    DrawArcSegment(0, 0.0f, firstSegmentEndTime);

                    loopStartSegment = 1;
                }

                bool stopArc = false;
                int currentSegment = 0;
                if (segmentStartTime < arcHitTime)
                {
                    for (currentSegment = loopStartSegment; currentSegment < this.segmentCount; ++currentSegment)
                    {
                        //Clamp the segment end time to the arc duration
                        float segmentEndTime = segmentStartTime + timeStep;
                        if (segmentEndTime >= this.arcDuration)
                        {
                            segmentEndTime = this.arcDuration;
                            stopArc = true;
                        }

                        if (segmentEndTime >= arcHitTime)
                        {
                            segmentEndTime = arcHitTime;
                            stopArc = true;
                        }

                        DrawArcSegment(currentSegment, segmentStartTime, segmentEndTime);

                        segmentStartTime += timeStep + this.segmentBreak;

                        //If the previous end time or the next start time is beyond the duration then stop the arc
                        if (stopArc || segmentStartTime >= this.arcDuration || segmentStartTime >= arcHitTime)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    currentSegment--;
                }

                //Hide the rest of the line segments
                HideLineSegments(currentSegment + 1, this.segmentCount);
            }

            return arcHitTime != float.MaxValue;
        }

        //-------------------------------------------------
        private void DrawArcSegment(int index, float startTime, float endTime)
        {
            this.lineRenderers[index].enabled = true;
            this.lineRenderers[index].SetPosition(0, GetArcPositionAtTime(startTime));
            this.lineRenderers[index].SetPosition(1, GetArcPositionAtTime(endTime));
        }

        //-------------------------------------------------
        public void SetColor(Color color)
        {
            for (int i = 0; i < this.segmentCount; ++i)
            {
#if (UNITY_5_4)
				lineRenderers[i].SetColors( color, color );
#else
                this.lineRenderers[i].startColor = color;
                this.lineRenderers[i].endColor = color;
#endif
            }
        }

        //-------------------------------------------------
        private float FindProjectileCollision(out RaycastHit hitInfo)
        {
            float timeStep = this.arcDuration / this.segmentCount;
            float segmentStartTime = 0.0f;

            hitInfo = new RaycastHit();

            Vector3 segmentStartPos = GetArcPositionAtTime(segmentStartTime);
            for (int i = 0; i < this.segmentCount; ++i)
            {
                float segmentEndTime = segmentStartTime + timeStep;
                Vector3 segmentEndPos = GetArcPositionAtTime(segmentEndTime);

                if (Physics.Linecast(segmentStartPos, segmentEndPos, out hitInfo, this.traceLayerMask))
                {
                    if (hitInfo.collider.GetComponent<IgnoreTeleportTrace>() == null)
                    {
                        Util.DrawCross(hitInfo.point, Color.red, 0.5f);
                        float segmentDistance = Vector3.Distance(segmentStartPos, segmentEndPos);
                        float hitTime = segmentStartTime + (timeStep * (hitInfo.distance / segmentDistance));
                        return hitTime;
                    }
                }

                segmentStartTime = segmentEndTime;
                segmentStartPos = segmentEndPos;
            }

            return float.MaxValue;
        }

        //-------------------------------------------------
        public Vector3 GetArcPositionAtTime(float time)
        {
            Vector3 gravity = this.useGravity ? Physics.gravity : Vector3.zero;

            Vector3 arcPos = this.startPos + ((this.projectileVelocity * time) + (0.5f * time * time) * gravity);
            return arcPos;
        }

        //-------------------------------------------------
        private void HideLineSegments(int startSegment, int endSegment)
        {
            if (this.lineRenderers != null)
            {
                for (int i = startSegment; i < endSegment; ++i)
                {
                    this.lineRenderers[i].enabled = false;
                }
            }
        }
    }
}