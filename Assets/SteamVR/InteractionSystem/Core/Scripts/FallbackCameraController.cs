﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Controls for the non-VR debug camera
//
//=============================================================================

using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    [RequireComponent(typeof(Camera))]
    public class FallbackCameraController : MonoBehaviour
    {
        public float speed = 4.0f;
        public float shiftSpeed = 16.0f;
        public bool showInstructions = true;

        private Vector3 startEulerAngles;
        private Vector3 startMousePosition;
        private float realTime;

        //-------------------------------------------------
        private void OnEnable()
        {
            this.realTime = Time.realtimeSinceStartup;
        }

        //-------------------------------------------------
        private void Update()
        {
            float forward = 0.0f;
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            {
                forward += 1.0f;
            }
            if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
            {
                forward -= 1.0f;
            }

            float right = 0.0f;
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                right += 1.0f;
            }
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                right -= 1.0f;
            }

            float currentSpeed = this.speed;
            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            {
                currentSpeed = this.shiftSpeed;
            }

            float realTimeNow = Time.realtimeSinceStartup;
            float deltaRealTime = realTimeNow - this.realTime;
            this.realTime = realTimeNow;

            Vector3 delta = new Vector3(right, 0.0f, forward) * currentSpeed * deltaRealTime;

            this.transform.position += this.transform.TransformDirection(delta);

            Vector3 mousePosition = Input.mousePosition;

            if (Input.GetMouseButtonDown(1) /* right mouse */)
            {
                this.startMousePosition = mousePosition;
                this.startEulerAngles = this.transform.localEulerAngles;
            }

            if (Input.GetMouseButton(1) /* right mouse */)
            {
                Vector3 offset = mousePosition - this.startMousePosition;
                this.transform.localEulerAngles = this.startEulerAngles + new Vector3(-offset.y * 360.0f / Screen.height, offset.x * 360.0f / Screen.width, 0.0f);
            }
        }

        //-------------------------------------------------
        private void OnGUI()
        {
            if (this.showInstructions)
            {
                GUI.Label(new Rect(10.0f, 10.0f, 600.0f, 400.0f),
                    "WASD/Arrow Keys to translate the camera\n" +
                    "Right mouse click to rotate the camera\n" +
                    "Left mouse click for standard interactions.\n");
            }
        }
    }
}