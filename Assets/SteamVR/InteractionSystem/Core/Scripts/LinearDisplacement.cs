﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Move the position of this object based on a linear mapping
//
//=============================================================================

using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class LinearDisplacement : MonoBehaviour
    {
        public Vector3 displacement;
        public LinearMapping linearMapping;

        private Vector3 initialPosition;

        //-------------------------------------------------
        private void Start()
        {
            this.initialPosition = this.transform.localPosition;

            if (this.linearMapping == null)
            {
                this.linearMapping = GetComponent<LinearMapping>();
            }
        }

        //-------------------------------------------------
        private void Update()
        {
            if (this.linearMapping)
            {
                this.transform.localPosition = this.initialPosition + this.linearMapping.value * this.displacement;
            }
        }
    }
}