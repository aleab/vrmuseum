﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Uses the see thru renderer while attached to hand
//
//=============================================================================

using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class SeeThru : MonoBehaviour
    {
        public Material seeThruMaterial;

        private GameObject seeThru;
        private Interactable interactable;
        private Renderer sourceRenderer;
        private Renderer destRenderer;

        //-------------------------------------------------
        private void Awake()
        {
            this.interactable = GetComponentInParent<Interactable>();

            //
            // Create child game object for see thru renderer
            //
            this.seeThru = new GameObject("_see_thru");
            this.seeThru.transform.parent = this.transform;
            this.seeThru.transform.localPosition = Vector3.zero;
            this.seeThru.transform.localRotation = Quaternion.identity;
            this.seeThru.transform.localScale = Vector3.one;

            //
            // Copy mesh filter
            //
            MeshFilter sourceMeshFilter = GetComponent<MeshFilter>();
            if (sourceMeshFilter != null)
            {
                MeshFilter destMeshFilter = this.seeThru.AddComponent<MeshFilter>();
                destMeshFilter.sharedMesh = sourceMeshFilter.sharedMesh;
            }

            //
            // Copy mesh renderer
            //
            MeshRenderer sourceMeshRenderer = GetComponent<MeshRenderer>();
            if (sourceMeshRenderer != null)
            {
                this.sourceRenderer = sourceMeshRenderer;
                this.destRenderer = this.seeThru.AddComponent<MeshRenderer>();
            }

            //
            // Copy skinned mesh renderer
            //
            SkinnedMeshRenderer sourceSkinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
            if (sourceSkinnedMeshRenderer != null)
            {
                SkinnedMeshRenderer destSkinnedMeshRenderer = this.seeThru.AddComponent<SkinnedMeshRenderer>();

                this.sourceRenderer = sourceSkinnedMeshRenderer;
                this.destRenderer = destSkinnedMeshRenderer;

                destSkinnedMeshRenderer.sharedMesh = sourceSkinnedMeshRenderer.sharedMesh;
                destSkinnedMeshRenderer.rootBone = sourceSkinnedMeshRenderer.rootBone;
                destSkinnedMeshRenderer.bones = sourceSkinnedMeshRenderer.bones;
                destSkinnedMeshRenderer.quality = sourceSkinnedMeshRenderer.quality;
                destSkinnedMeshRenderer.updateWhenOffscreen = sourceSkinnedMeshRenderer.updateWhenOffscreen;
            }

            //
            // Create see thru materials
            //
            if (this.sourceRenderer != null && this.destRenderer != null)
            {
                int materialCount = this.sourceRenderer.sharedMaterials.Length;
                Material[] destRendererMaterials = new Material[materialCount];
                for (int i = 0; i < materialCount; i++)
                {
                    destRendererMaterials[i] = this.seeThruMaterial;
                }
                this.destRenderer.sharedMaterials = destRendererMaterials;

                for (int i = 0; i < this.destRenderer.materials.Length; i++)
                {
                    this.destRenderer.materials[i].renderQueue = 2001; // Rendered after geometry
                }

                for (int i = 0; i < this.sourceRenderer.materials.Length; i++)
                {
                    if (this.sourceRenderer.materials[i].renderQueue == 2000)
                    {
                        this.sourceRenderer.materials[i].renderQueue = 2002;
                    }
                }
            }

            this.seeThru.gameObject.SetActive(false);
        }

        //-------------------------------------------------
        private void OnEnable()
        {
            this.interactable.onAttachedToHand += this.AttachedToHand;
            this.interactable.onDetachedFromHand += this.DetachedFromHand;
        }

        //-------------------------------------------------
        private void OnDisable()
        {
            this.interactable.onAttachedToHand -= this.AttachedToHand;
            this.interactable.onDetachedFromHand -= this.DetachedFromHand;
        }

        //-------------------------------------------------
        private void AttachedToHand(Hand hand)
        {
            this.seeThru.SetActive(true);
        }

        //-------------------------------------------------
        private void DetachedFromHand(Hand hand)
        {
            this.seeThru.SetActive(false);
        }

        //-------------------------------------------------
        private void Update()
        {
            if (this.seeThru.activeInHierarchy)
            {
                int materialCount = Mathf.Min(this.sourceRenderer.materials.Length, this.destRenderer.materials.Length);
                for (int i = 0; i < materialCount; i++)
                {
                    this.destRenderer.materials[i].mainTexture = this.sourceRenderer.materials[i].mainTexture;
                    this.destRenderer.materials[i].color = this.destRenderer.materials[i].color * this.sourceRenderer.materials[i].color;
                }
            }
        }
    }
}