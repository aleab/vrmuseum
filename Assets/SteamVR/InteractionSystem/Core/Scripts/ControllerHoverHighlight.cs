﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Highlights the controller when hovering over interactables
//
//=============================================================================

using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class ControllerHoverHighlight : MonoBehaviour
    {
        public Material highLightMaterial;
        public bool fireHapticsOnHightlight = true;

        private Hand hand;

        private MeshRenderer bodyMeshRenderer;
        private MeshRenderer trackingHatMeshRenderer;
        private SteamVR_RenderModel renderModel;
        private bool renderModelLoaded = false;

        private SteamVR_Events.Action renderModelLoadedAction;

        //-------------------------------------------------
        private void Start()
        {
            this.hand = GetComponentInParent<Hand>();
        }

        //-------------------------------------------------
        private void Awake()
        {
            this.renderModelLoadedAction = SteamVR_Events.RenderModelLoadedAction(this.OnRenderModelLoaded);
        }

        //-------------------------------------------------
        private void OnEnable()
        {
            this.renderModelLoadedAction.enabled = true;
        }

        //-------------------------------------------------
        private void OnDisable()
        {
            this.renderModelLoadedAction.enabled = false;
        }

        //-------------------------------------------------
        private void OnHandInitialized(int deviceIndex)
        {
            this.renderModel = this.gameObject.AddComponent<SteamVR_RenderModel>();
            this.renderModel.SetDeviceIndex(deviceIndex);
            this.renderModel.updateDynamically = false;
        }

        //-------------------------------------------------
        private void OnRenderModelLoaded(SteamVR_RenderModel renderModel, bool success)
        {
            if (renderModel != this.renderModel)
            {
                return;
            }

            Transform bodyTransform = this.transform.Find("body");
            if (bodyTransform != null)
            {
                this.bodyMeshRenderer = bodyTransform.GetComponent<MeshRenderer>();
                this.bodyMeshRenderer.material = this.highLightMaterial;
                this.bodyMeshRenderer.enabled = false;
            }

            Transform trackingHatTransform = this.transform.Find("trackhat");
            if (trackingHatTransform != null)
            {
                this.trackingHatMeshRenderer = trackingHatTransform.GetComponent<MeshRenderer>();
                this.trackingHatMeshRenderer.material = this.highLightMaterial;
                this.trackingHatMeshRenderer.enabled = false;
            }

            foreach (Transform child in this.transform)
            {
                if ((child.name != "body") && (child.name != "trackhat"))
                {
                    Destroy(child.gameObject);
                }
            }

            this.renderModelLoaded = true;
        }

        //-------------------------------------------------
        private void OnParentHandHoverBegin(Interactable other)
        {
            if (!this.isActiveAndEnabled)
            {
                return;
            }

            if (other.transform.parent != this.transform.parent)
            {
                ShowHighlight();
            }
        }

        //-------------------------------------------------
        private void OnParentHandHoverEnd(Interactable other)
        {
            HideHighlight();
        }

        //-------------------------------------------------
        private void OnParentHandInputFocusAcquired()
        {
            if (!this.isActiveAndEnabled)
            {
                return;
            }

            if (this.hand.hoveringInteractable && this.hand.hoveringInteractable.transform.parent != this.transform.parent)
            {
                ShowHighlight();
            }
        }

        //-------------------------------------------------
        private void OnParentHandInputFocusLost()
        {
            HideHighlight();
        }

        //-------------------------------------------------
        public void ShowHighlight()
        {
            if (this.renderModelLoaded == false)
            {
                return;
            }

            if (this.fireHapticsOnHightlight)
            {
                this.hand.controller.TriggerHapticPulse(500);
            }

            if (this.bodyMeshRenderer != null)
            {
                this.bodyMeshRenderer.enabled = true;
            }

            if (this.trackingHatMeshRenderer != null)
            {
                this.trackingHatMeshRenderer.enabled = true;
            }
        }

        //-------------------------------------------------
        public void HideHighlight()
        {
            if (this.renderModelLoaded == false)
            {
                return;
            }

            if (this.fireHapticsOnHightlight)
            {
                this.hand.controller.TriggerHapticPulse(300);
            }

            if (this.bodyMeshRenderer != null)
            {
                this.bodyMeshRenderer.enabled = false;
            }

            if (this.trackingHatMeshRenderer != null)
            {
                this.trackingHatMeshRenderer.enabled = false;
            }
        }
    }
}