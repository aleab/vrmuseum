﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Animator whose speed is set based on a linear mapping
//
//=============================================================================

using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class LinearAnimator : MonoBehaviour
    {
        public LinearMapping linearMapping;
        public Animator animator;

        private float currentLinearMapping = float.NaN;
        private int framesUnchanged = 0;

        //-------------------------------------------------
        private void Awake()
        {
            if (this.animator == null)
            {
                this.animator = GetComponent<Animator>();
            }

            this.animator.speed = 0.0f;

            if (this.linearMapping == null)
            {
                this.linearMapping = GetComponent<LinearMapping>();
            }
        }

        //-------------------------------------------------
        private void Update()
        {
            if (this.currentLinearMapping != this.linearMapping.value)
            {
                this.currentLinearMapping = this.linearMapping.value;
                this.animator.enabled = true;
                this.animator.Play(0, 0, this.currentLinearMapping);
                this.framesUnchanged = 0;
            }
            else
            {
                this.framesUnchanged++;
                if (this.framesUnchanged > 2)
                {
                    this.animator.enabled = false;
                }
            }
        }
    }
}