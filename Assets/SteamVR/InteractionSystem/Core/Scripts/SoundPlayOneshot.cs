﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Play one-shot sounds as opposed to continuos/looping ones
//
//=============================================================================

using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class SoundPlayOneshot : MonoBehaviour
    {
        public AudioClip[] waveFiles;
        private AudioSource thisAudioSource;

        public float volMin;
        public float volMax;

        public float pitchMin;
        public float pitchMax;

        public bool playOnAwake;

        //-------------------------------------------------
        private void Awake()
        {
            this.thisAudioSource = GetComponent<AudioSource>();

            if (this.playOnAwake)
            {
                Play();
            }
        }

        //-------------------------------------------------
        public void Play()
        {
            if (this.thisAudioSource != null && this.thisAudioSource.isActiveAndEnabled && !Util.IsNullOrEmpty(this.waveFiles))
            {
                //randomly apply a volume between the volume min max
                this.thisAudioSource.volume = Random.Range(this.volMin, this.volMax);

                //randomly apply a pitch between the pitch min max
                this.thisAudioSource.pitch = Random.Range(this.pitchMin, this.pitchMax);

                // play the sound
                this.thisAudioSource.PlayOneShot(this.waveFiles[Random.Range(0, this.waveFiles.Length)]);
            }
        }

        //-------------------------------------------------
        public void Pause()
        {
            if (this.thisAudioSource != null)
            {
                this.thisAudioSource.Pause();
            }
        }

        //-------------------------------------------------
        public void UnPause()
        {
            if (this.thisAudioSource != null)
            {
                this.thisAudioSource.UnPause();
            }
        }
    }
}