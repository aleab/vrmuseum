﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Animation that moves based on a linear mapping
//
//=============================================================================

using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class LinearAnimation : MonoBehaviour
    {
        public LinearMapping linearMapping;
        public new Animation animation;

        private AnimationState animState;
        private float animLength;
        private float lastValue;

        //-------------------------------------------------
        private void Awake()
        {
            if (this.animation == null)
            {
                this.animation = GetComponent<Animation>();
            }

            if (this.linearMapping == null)
            {
                this.linearMapping = GetComponent<LinearMapping>();
            }

            //We're assuming the animation has a single clip, and that's the one we're
            //going to scrub with the linear mapping.
            this.animation.playAutomatically = true;
            this.animState = this.animation[this.animation.clip.name];

            //If the anim state's (i.e. clip's) wrap mode is Once (the default) or ClampForever,
            //Unity will automatically stop playing the anim, regardless of subsequent changes
            //to animState.time. Thus, we set the wrap mode to PingPong.
            this.animState.wrapMode = WrapMode.PingPong;
            this.animState.speed = 0;
            this.animLength = this.animState.length;
        }

        //-------------------------------------------------
        private void Update()
        {
            float value = this.linearMapping.value;

            //No need to set the anim if our value hasn't changed.
            if (value != this.lastValue)
            {
                this.animState.time = value / this.animLength;
            }

            this.lastValue = value;
        }
    }
}