//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Destroys this object when its particle system dies
//
//=============================================================================

using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    [RequireComponent(typeof(ParticleSystem))]
    public class DestroyOnParticleSystemDeath : MonoBehaviour
    {
        private ParticleSystem particles;

        //-------------------------------------------------
        private void Awake()
        {
            this.particles = GetComponent<ParticleSystem>();

            InvokeRepeating("CheckParticleSystem", 0.1f, 0.1f);
        }

        //-------------------------------------------------
        private void CheckParticleSystem()
        {
            if (!this.particles.IsAlive())
            {
                Destroy(this.gameObject);
            }
        }
    }
}