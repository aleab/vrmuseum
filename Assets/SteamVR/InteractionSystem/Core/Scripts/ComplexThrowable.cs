﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Throwable that uses physics joints to attach instead of just
//			parenting
//
//=============================================================================

using System.Collections.Generic;
using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    [RequireComponent(typeof(Interactable))]
    public class ComplexThrowable : MonoBehaviour
    {
        public enum AttachMode
        {
            FixedJoint,
            Force,
        }

        public float attachForce = 800.0f;
        public float attachForceDamper = 25.0f;

        public AttachMode attachMode = AttachMode.FixedJoint;

        [EnumFlags]
        public Hand.AttachmentFlags attachmentFlags = 0;

        private List<Hand> holdingHands = new List<Hand>();
        private List<Rigidbody> holdingBodies = new List<Rigidbody>();
        private List<Vector3> holdingPoints = new List<Vector3>();

        private List<Rigidbody> rigidBodies = new List<Rigidbody>();

        //-------------------------------------------------
        private void Awake()
        {
            GetComponentsInChildren<Rigidbody>(this.rigidBodies);
        }

        //-------------------------------------------------
        private void Update()
        {
            for (int i = 0; i < this.holdingHands.Count; i++)
            {
                if (!this.holdingHands[i].GetStandardInteractionButton())
                {
                    PhysicsDetach(this.holdingHands[i]);
                }
            }
        }

        //-------------------------------------------------
        private void OnHandHoverBegin(Hand hand)
        {
            if (this.holdingHands.IndexOf(hand) == -1)
            {
                if (hand.controller != null)
                {
                    hand.controller.TriggerHapticPulse(800);
                }
            }
        }

        //-------------------------------------------------
        private void OnHandHoverEnd(Hand hand)
        {
            if (this.holdingHands.IndexOf(hand) == -1)
            {
                if (hand.controller != null)
                {
                    hand.controller.TriggerHapticPulse(500);
                }
            }
        }

        //-------------------------------------------------
        private void HandHoverUpdate(Hand hand)
        {
            if (hand.GetStandardInteractionButtonDown())
            {
                PhysicsAttach(hand);
            }
        }

        //-------------------------------------------------
        private void PhysicsAttach(Hand hand)
        {
            PhysicsDetach(hand);

            Rigidbody holdingBody = null;
            Vector3 holdingPoint = Vector3.zero;

            // The hand should grab onto the nearest rigid body
            float closestDistance = float.MaxValue;
            for (int i = 0; i < this.rigidBodies.Count; i++)
            {
                float distance = Vector3.Distance(this.rigidBodies[i].worldCenterOfMass, hand.transform.position);
                if (distance < closestDistance)
                {
                    holdingBody = this.rigidBodies[i];
                    closestDistance = distance;
                }
            }

            // Couldn't grab onto a body
            if (holdingBody == null)
                return;

            // Create a fixed joint from the hand to the holding body
            if (this.attachMode == AttachMode.FixedJoint)
            {
                Rigidbody handRigidbody = Util.FindOrAddComponent<Rigidbody>(hand.gameObject);
                handRigidbody.isKinematic = true;

                FixedJoint handJoint = hand.gameObject.AddComponent<FixedJoint>();
                handJoint.connectedBody = holdingBody;
            }

            // Don't let the hand interact with other things while it's holding us
            hand.HoverLock(null);

            // Affix this point
            Vector3 offset = hand.transform.position - holdingBody.worldCenterOfMass;
            offset = Mathf.Min(offset.magnitude, 1.0f) * offset.normalized;
            holdingPoint = holdingBody.transform.InverseTransformPoint(holdingBody.worldCenterOfMass + offset);

            hand.AttachObject(this.gameObject, this.attachmentFlags);

            // Update holding list
            this.holdingHands.Add(hand);
            this.holdingBodies.Add(holdingBody);
            this.holdingPoints.Add(holdingPoint);
        }

        //-------------------------------------------------
        private bool PhysicsDetach(Hand hand)
        {
            int i = this.holdingHands.IndexOf(hand);

            if (i != -1)
            {
                // Detach this object from the hand
                this.holdingHands[i].DetachObject(this.gameObject, false);

                // Allow the hand to do other things
                this.holdingHands[i].HoverUnlock(null);

                // Delete any existing joints from the hand
                if (this.attachMode == AttachMode.FixedJoint)
                {
                    Destroy(this.holdingHands[i].GetComponent<FixedJoint>());
                }

                Util.FastRemove(this.holdingHands, i);
                Util.FastRemove(this.holdingBodies, i);
                Util.FastRemove(this.holdingPoints, i);

                return true;
            }

            return false;
        }

        //-------------------------------------------------
        private void FixedUpdate()
        {
            if (this.attachMode == AttachMode.Force)
            {
                for (int i = 0; i < this.holdingHands.Count; i++)
                {
                    Vector3 targetPoint = this.holdingBodies[i].transform.TransformPoint(this.holdingPoints[i]);
                    Vector3 vdisplacement = this.holdingHands[i].transform.position - targetPoint;

                    this.holdingBodies[i].AddForceAtPosition(this.attachForce * vdisplacement, targetPoint, ForceMode.Acceleration);
                    this.holdingBodies[i].AddForceAtPosition(-this.attachForceDamper * this.holdingBodies[i].GetPointVelocity(targetPoint), targetPoint, ForceMode.Acceleration);
                }
            }
        }
    }
}