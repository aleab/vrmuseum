﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Spawns a render model for the controller from SteamVR
//
//=============================================================================

using System.Collections.Generic;
using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class SpawnRenderModel : MonoBehaviour
    {
        public Material[] materials;

        private SteamVR_RenderModel[] renderModels;
        private Hand hand;
        private List<MeshRenderer> renderers = new List<MeshRenderer>();

        private static List<SpawnRenderModel> spawnRenderModels = new List<SpawnRenderModel>();
        private static int lastFrameUpdated;
        private static int spawnRenderModelUpdateIndex;

        private SteamVR_Events.Action renderModelLoadedAction;

        //-------------------------------------------------
        private void Awake()
        {
            this.renderModels = new SteamVR_RenderModel[this.materials.Length];
            this.renderModelLoadedAction = SteamVR_Events.RenderModelLoadedAction(this.OnRenderModelLoaded);
        }

        //-------------------------------------------------
        private void OnEnable()
        {
            ShowController();

            this.renderModelLoadedAction.enabled = true;

            spawnRenderModels.Add(this);
        }

        //-------------------------------------------------
        private void OnDisable()
        {
            HideController();

            this.renderModelLoadedAction.enabled = false;

            spawnRenderModels.Remove(this);
        }

        //-------------------------------------------------
        private void OnAttachedToHand(Hand hand)
        {
            this.hand = hand;
            ShowController();
        }

        //-------------------------------------------------
        private void OnDetachedFromHand(Hand hand)
        {
            this.hand = null;
            HideController();
        }

        //-------------------------------------------------
        private void Update()
        {
            // Only update one per frame
            if (lastFrameUpdated == Time.renderedFrameCount)
            {
                return;
            }
            lastFrameUpdated = Time.renderedFrameCount;

            // SpawnRenderModel overflow
            if (spawnRenderModelUpdateIndex >= spawnRenderModels.Count)
            {
                spawnRenderModelUpdateIndex = 0;
            }

            // Perform update
            if (spawnRenderModelUpdateIndex < spawnRenderModels.Count)
            {
                SteamVR_RenderModel renderModel = spawnRenderModels[spawnRenderModelUpdateIndex].renderModels[0];
                if (renderModel != null)
                {
                    renderModel.UpdateComponents(OpenVR.RenderModels);
                }
            }

            spawnRenderModelUpdateIndex++;
        }

        //-------------------------------------------------
        private void ShowController()
        {
            if (this.hand == null || this.hand.controller == null)
            {
                return;
            }

            for (int i = 0; i < this.renderModels.Length; i++)
            {
                if (this.renderModels[i] == null)
                {
                    this.renderModels[i] = new GameObject("SteamVR_RenderModel").AddComponent<SteamVR_RenderModel>();
                    this.renderModels[i].updateDynamically = false; // Update one per frame (see Update() method)
                    this.renderModels[i].transform.parent = this.transform;
                    Util.ResetTransform(this.renderModels[i].transform);
                }

                this.renderModels[i].gameObject.SetActive(true);
                this.renderModels[i].SetDeviceIndex((int)this.hand.controller.index);
            }
        }

        //-------------------------------------------------
        private void HideController()
        {
            for (int i = 0; i < this.renderModels.Length; i++)
            {
                if (this.renderModels[i] != null)
                {
                    this.renderModels[i].gameObject.SetActive(false);
                }
            }
        }

        //-------------------------------------------------
        private void OnRenderModelLoaded(SteamVR_RenderModel renderModel, bool success)
        {
            for (int i = 0; i < this.renderModels.Length; i++)
            {
                if (renderModel == this.renderModels[i])
                {
                    if (this.materials[i] != null)
                    {
                        this.renderers.Clear();
                        this.renderModels[i].GetComponentsInChildren<MeshRenderer>(this.renderers);
                        for (int j = 0; j < this.renderers.Count; j++)
                        {
                            Texture mainTexture = this.renderers[j].material.mainTexture;
                            this.renderers[j].sharedMaterial = this.materials[i];
                            this.renderers[j].material.mainTexture = mainTexture;
                        }
                    }
                }
            }
        }
    }
}