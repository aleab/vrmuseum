﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Interactable that can be used to move in a circular motion
//
//=============================================================================

using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    [RequireComponent(typeof(Interactable))]
    public class CircularDrive : MonoBehaviour
    {
        public enum Axis_t
        {
            XAxis,
            YAxis,
            ZAxis
        };

        [Tooltip("The axis around which the circular drive will rotate in local space")]
        public Axis_t axisOfRotation = Axis_t.XAxis;

        [Tooltip("Child GameObject which has the Collider component to initiate interaction, only needs to be set if there is more than one Collider child")]
        public Collider childCollider = null;

        [Tooltip("A LinearMapping component to drive, if not specified one will be dynamically added to this GameObject")]
        public LinearMapping linearMapping;

        [Tooltip("If true, the drive will stay manipulating as long as the button is held down, if false, it will stop if the controller moves out of the collider")]
        public bool hoverLock = false;

        [HeaderAttribute("Limited Rotation")]
        [Tooltip("If true, the rotation will be limited to [minAngle, maxAngle], if false, the rotation is unlimited")]
        public bool limited = false;

        public Vector2 frozenDistanceMinMaxThreshold = new Vector2(0.1f, 0.2f);
        public UnityEvent onFrozenDistanceThreshold;

        [HeaderAttribute("Limited Rotation Min")]
        [Tooltip("If limited is true, the specifies the lower limit, otherwise value is unused")]
        public float minAngle = -45.0f;

        [Tooltip("If limited, set whether drive will freeze its angle when the min angle is reached")]
        public bool freezeOnMin = false;

        [Tooltip("If limited, event invoked when minAngle is reached")]
        public UnityEvent onMinAngle;

        [HeaderAttribute("Limited Rotation Max")]
        [Tooltip("If limited is true, the specifies the upper limit, otherwise value is unused")]
        public float maxAngle = 45.0f;

        [Tooltip("If limited, set whether drive will freeze its angle when the max angle is reached")]
        public bool freezeOnMax = false;

        [Tooltip("If limited, event invoked when maxAngle is reached")]
        public UnityEvent onMaxAngle;

        [Tooltip("If limited is true, this forces the starting angle to be startAngle, clamped to [minAngle, maxAngle]")]
        public bool forceStart = false;

        [Tooltip("If limited is true and forceStart is true, the starting angle will be this, clamped to [minAngle, maxAngle]")]
        public float startAngle = 0.0f;

        [Tooltip("If true, the transform of the GameObject this component is on will be rotated accordingly")]
        public bool rotateGameObject = true;

        [Tooltip("If true, the path of the Hand (red) and the projected value (green) will be drawn")]
        public bool debugPath = false;

        [Tooltip("If debugPath is true, this is the maximum number of GameObjects to create to draw the path")]
        public int dbgPathLimit = 50;

        [Tooltip("If not null, the TextMesh will display the linear value and the angular value of this circular drive")]
        public TextMesh debugText = null;

        [Tooltip("The output angle value of the drive in degrees, unlimited will increase or decrease without bound, take the 360 modulus to find number of rotations")]
        public float outAngle;

        private Quaternion start;

        private Vector3 worldPlaneNormal = new Vector3(1.0f, 0.0f, 0.0f);
        private Vector3 localPlaneNormal = new Vector3(1.0f, 0.0f, 0.0f);

        private Vector3 lastHandProjected;

        private Color red = new Color(1.0f, 0.0f, 0.0f);
        private Color green = new Color(0.0f, 1.0f, 0.0f);

        private GameObject[] dbgHandObjects;
        private GameObject[] dbgProjObjects;
        private GameObject dbgObjectsParent;
        private int dbgObjectCount = 0;
        private int dbgObjectIndex = 0;

        private bool driving = false;

        // If the drive is limited as is at min/max, angles greater than this are ignored
        private float minMaxAngularThreshold = 1.0f;

        private bool frozen = false;
        private float frozenAngle = 0.0f;
        private Vector3 frozenHandWorldPos = new Vector3(0.0f, 0.0f, 0.0f);
        private Vector2 frozenSqDistanceMinMaxThreshold = new Vector2(0.0f, 0.0f);

        private Hand handHoverLocked = null;

        //-------------------------------------------------
        private void Freeze(Hand hand)
        {
            this.frozen = true;
            this.frozenAngle = this.outAngle;
            this.frozenHandWorldPos = hand.hoverSphereTransform.position;
            this.frozenSqDistanceMinMaxThreshold.x = this.frozenDistanceMinMaxThreshold.x * this.frozenDistanceMinMaxThreshold.x;
            this.frozenSqDistanceMinMaxThreshold.y = this.frozenDistanceMinMaxThreshold.y * this.frozenDistanceMinMaxThreshold.y;
        }

        //-------------------------------------------------
        private void UnFreeze()
        {
            this.frozen = false;
            this.frozenHandWorldPos.Set(0.0f, 0.0f, 0.0f);
        }

        //-------------------------------------------------
        private void Start()
        {
            if (this.childCollider == null)
            {
                this.childCollider = GetComponentInChildren<Collider>();
            }

            if (this.linearMapping == null)
            {
                this.linearMapping = GetComponent<LinearMapping>();
            }

            if (this.linearMapping == null)
            {
                this.linearMapping = this.gameObject.AddComponent<LinearMapping>();
            }

            this.worldPlaneNormal = new Vector3(0.0f, 0.0f, 0.0f);
            this.worldPlaneNormal[(int)this.axisOfRotation] = 1.0f;

            this.localPlaneNormal = this.worldPlaneNormal;

            if (this.transform.parent)
            {
                this.worldPlaneNormal = this.transform.parent.localToWorldMatrix.MultiplyVector(this.worldPlaneNormal).normalized;
            }

            if (this.limited)
            {
                this.start = Quaternion.identity;
                this.outAngle = this.transform.localEulerAngles[(int)this.axisOfRotation];

                if (this.forceStart)
                {
                    this.outAngle = Mathf.Clamp(this.startAngle, this.minAngle, this.maxAngle);
                }
            }
            else
            {
                this.start = Quaternion.AngleAxis(this.transform.localEulerAngles[(int)this.axisOfRotation], this.localPlaneNormal);
                this.outAngle = 0.0f;
            }

            if (this.debugText)
            {
                this.debugText.alignment = TextAlignment.Left;
                this.debugText.anchor = TextAnchor.UpperLeft;
            }

            UpdateAll();
        }

        //-------------------------------------------------
        private void OnDisable()
        {
            if (this.handHoverLocked)
            {
                ControllerButtonHints.HideButtonHint(this.handHoverLocked, Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger);
                this.handHoverLocked.HoverUnlock(GetComponent<Interactable>());
                this.handHoverLocked = null;
            }
        }

        //-------------------------------------------------
        private IEnumerator HapticPulses(SteamVR_Controller.Device controller, float flMagnitude, int nCount)
        {
            if (controller != null)
            {
                int nRangeMax = (int)Util.RemapNumberClamped(flMagnitude, 0.0f, 1.0f, 100.0f, 900.0f);
                nCount = Mathf.Clamp(nCount, 1, 10);

                for (ushort i = 0; i < nCount; ++i)
                {
                    ushort duration = (ushort)Random.Range(100, nRangeMax);
                    controller.TriggerHapticPulse(duration);
                    yield return new WaitForSeconds(.01f);
                }
            }
        }

        //-------------------------------------------------
        private void OnHandHoverBegin(Hand hand)
        {
            ControllerButtonHints.ShowButtonHint(hand, Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger);
        }

        //-------------------------------------------------
        private void OnHandHoverEnd(Hand hand)
        {
            ControllerButtonHints.HideButtonHint(hand, Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger);

            if (this.driving && hand.GetStandardInteractionButton())
            {
                StartCoroutine(HapticPulses(hand.controller, 1.0f, 10));
            }

            this.driving = false;
            this.handHoverLocked = null;
        }

        //-------------------------------------------------
        private void HandHoverUpdate(Hand hand)
        {
            if (hand.GetStandardInteractionButtonDown())
            {
                // Trigger was just pressed
                this.lastHandProjected = ComputeToTransformProjected(hand.hoverSphereTransform);

                if (this.hoverLock)
                {
                    hand.HoverLock(GetComponent<Interactable>());
                    this.handHoverLocked = hand;
                }

                this.driving = true;

                ComputeAngle(hand);
                UpdateAll();

                ControllerButtonHints.HideButtonHint(hand, Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger);
            }
            else if (hand.GetStandardInteractionButtonUp())
            {
                // Trigger was just released
                if (this.hoverLock)
                {
                    hand.HoverUnlock(GetComponent<Interactable>());
                    this.handHoverLocked = null;
                }
            }
            else if (this.driving && hand.GetStandardInteractionButton() && hand.hoveringInteractable == GetComponent<Interactable>())
            {
                ComputeAngle(hand);
                UpdateAll();
            }
        }

        //-------------------------------------------------
        private Vector3 ComputeToTransformProjected(Transform xForm)
        {
            Vector3 toTransform = (xForm.position - this.transform.position).normalized;
            Vector3 toTransformProjected = new Vector3(0.0f, 0.0f, 0.0f);

            // Need a non-zero distance from the hand to the center of the CircularDrive
            if (toTransform.sqrMagnitude > 0.0f)
            {
                toTransformProjected = Vector3.ProjectOnPlane(toTransform, this.worldPlaneNormal).normalized;
            }
            else
            {
                Debug.LogFormat("The collider needs to be a minimum distance away from the CircularDrive GameObject {0}", this.gameObject.ToString());
                Debug.Assert(false, string.Format("The collider needs to be a minimum distance away from the CircularDrive GameObject {0}", this.gameObject.ToString()));
            }

            if (this.debugPath && this.dbgPathLimit > 0)
            {
                DrawDebugPath(xForm, toTransformProjected);
            }

            return toTransformProjected;
        }

        //-------------------------------------------------
        private void DrawDebugPath(Transform xForm, Vector3 toTransformProjected)
        {
            if (this.dbgObjectCount == 0)
            {
                this.dbgObjectsParent = new GameObject("Circular Drive Debug");
                this.dbgHandObjects = new GameObject[this.dbgPathLimit];
                this.dbgProjObjects = new GameObject[this.dbgPathLimit];
                this.dbgObjectCount = this.dbgPathLimit;
                this.dbgObjectIndex = 0;
            }

            //Actual path
            GameObject gSphere = null;

            if (this.dbgHandObjects[this.dbgObjectIndex])
            {
                gSphere = this.dbgHandObjects[this.dbgObjectIndex];
            }
            else
            {
                gSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                gSphere.transform.SetParent(this.dbgObjectsParent.transform);
                this.dbgHandObjects[this.dbgObjectIndex] = gSphere;
            }

            gSphere.name = string.Format("actual_{0}", (int)((1.0f - this.red.r) * 10.0f));
            gSphere.transform.position = xForm.position;
            gSphere.transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
            gSphere.transform.localScale = new Vector3(0.004f, 0.004f, 0.004f);
            gSphere.gameObject.GetComponent<Renderer>().material.color = this.red;

            if (this.red.r > 0.1f)
            {
                this.red.r -= 0.1f;
            }
            else
            {
                this.red.r = 1.0f;
            }

            //Projected path
            gSphere = null;

            if (this.dbgProjObjects[this.dbgObjectIndex])
            {
                gSphere = this.dbgProjObjects[this.dbgObjectIndex];
            }
            else
            {
                gSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                gSphere.transform.SetParent(this.dbgObjectsParent.transform);
                this.dbgProjObjects[this.dbgObjectIndex] = gSphere;
            }

            gSphere.name = string.Format("projed_{0}", (int)((1.0f - this.green.g) * 10.0f));
            gSphere.transform.position = this.transform.position + toTransformProjected * 0.25f;
            gSphere.transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
            gSphere.transform.localScale = new Vector3(0.004f, 0.004f, 0.004f);
            gSphere.gameObject.GetComponent<Renderer>().material.color = this.green;

            if (this.green.g > 0.1f)
            {
                this.green.g -= 0.1f;
            }
            else
            {
                this.green.g = 1.0f;
            }

            this.dbgObjectIndex = (this.dbgObjectIndex + 1) % this.dbgObjectCount;
        }

        //-------------------------------------------------
        // Updates the LinearMapping value from the angle
        //-------------------------------------------------
        private void UpdateLinearMapping()
        {
            if (this.limited)
            {
                // Map it to a [0, 1] value
                this.linearMapping.value = (this.outAngle - this.minAngle) / (this.maxAngle - this.minAngle);
            }
            else
            {
                // Normalize to [0, 1] based on 360 degree windings
                float flTmp = this.outAngle / 360.0f;
                this.linearMapping.value = flTmp - Mathf.Floor(flTmp);
            }

            UpdateDebugText();
        }

        //-------------------------------------------------
        // Updates the LinearMapping value from the angle
        //-------------------------------------------------
        private void UpdateGameObject()
        {
            if (this.rotateGameObject)
            {
                this.transform.localRotation = this.start * Quaternion.AngleAxis(this.outAngle, this.localPlaneNormal);
            }
        }

        //-------------------------------------------------
        // Updates the Debug TextMesh with the linear mapping value and the angle
        //-------------------------------------------------
        private void UpdateDebugText()
        {
            if (this.debugText)
            {
                this.debugText.text = string.Format("Linear: {0}\nAngle:  {1}\n", this.linearMapping.value, this.outAngle);
            }
        }

        //-------------------------------------------------
        // Updates the Debug TextMesh with the linear mapping value and the angle
        //-------------------------------------------------
        private void UpdateAll()
        {
            UpdateLinearMapping();
            UpdateGameObject();
            UpdateDebugText();
        }

        //-------------------------------------------------
        // Computes the angle to rotate the game object based on the change in the transform
        //-------------------------------------------------
        private void ComputeAngle(Hand hand)
        {
            Vector3 toHandProjected = ComputeToTransformProjected(hand.hoverSphereTransform);

            if (!toHandProjected.Equals(this.lastHandProjected))
            {
                float absAngleDelta = Vector3.Angle(this.lastHandProjected, toHandProjected);

                if (absAngleDelta > 0.0f)
                {
                    if (this.frozen)
                    {
                        float frozenSqDist = (hand.hoverSphereTransform.position - this.frozenHandWorldPos).sqrMagnitude;
                        if (frozenSqDist > this.frozenSqDistanceMinMaxThreshold.x)
                        {
                            this.outAngle = this.frozenAngle + Random.Range(-1.0f, 1.0f);

                            float magnitude = Util.RemapNumberClamped(frozenSqDist, this.frozenSqDistanceMinMaxThreshold.x, this.frozenSqDistanceMinMaxThreshold.y, 0.0f, 1.0f);
                            if (magnitude > 0)
                            {
                                StartCoroutine(HapticPulses(hand.controller, magnitude, 10));
                            }
                            else
                            {
                                StartCoroutine(HapticPulses(hand.controller, 0.5f, 10));
                            }

                            if (frozenSqDist >= this.frozenSqDistanceMinMaxThreshold.y)
                            {
                                this.onFrozenDistanceThreshold.Invoke();
                            }
                        }
                    }
                    else
                    {
                        Vector3 cross = Vector3.Cross(this.lastHandProjected, toHandProjected).normalized;
                        float dot = Vector3.Dot(this.worldPlaneNormal, cross);

                        float signedAngleDelta = absAngleDelta;

                        if (dot < 0.0f)
                        {
                            signedAngleDelta = -signedAngleDelta;
                        }

                        if (this.limited)
                        {
                            float angleTmp = Mathf.Clamp(this.outAngle + signedAngleDelta, this.minAngle, this.maxAngle);

                            if (this.outAngle == this.minAngle)
                            {
                                if (angleTmp > this.minAngle && absAngleDelta < this.minMaxAngularThreshold)
                                {
                                    this.outAngle = angleTmp;
                                    this.lastHandProjected = toHandProjected;
                                }
                            }
                            else if (this.outAngle == this.maxAngle)
                            {
                                if (angleTmp < this.maxAngle && absAngleDelta < this.minMaxAngularThreshold)
                                {
                                    this.outAngle = angleTmp;
                                    this.lastHandProjected = toHandProjected;
                                }
                            }
                            else if (angleTmp == this.minAngle)
                            {
                                this.outAngle = angleTmp;
                                this.lastHandProjected = toHandProjected;
                                this.onMinAngle.Invoke();
                                if (this.freezeOnMin)
                                {
                                    Freeze(hand);
                                }
                            }
                            else if (angleTmp == this.maxAngle)
                            {
                                this.outAngle = angleTmp;
                                this.lastHandProjected = toHandProjected;
                                this.onMaxAngle.Invoke();
                                if (this.freezeOnMax)
                                {
                                    Freeze(hand);
                                }
                            }
                            else
                            {
                                this.outAngle = angleTmp;
                                this.lastHandProjected = toHandProjected;
                            }
                        }
                        else
                        {
                            this.outAngle += signedAngleDelta;
                            this.lastHandProjected = toHandProjected;
                        }
                    }
                }
            }
        }
    }
}