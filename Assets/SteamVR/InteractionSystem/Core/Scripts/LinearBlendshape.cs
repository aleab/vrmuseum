﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Set the blend shape weight based on a linear mapping
//
//=============================================================================

using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class LinearBlendshape : MonoBehaviour
    {
        public LinearMapping linearMapping;
        public SkinnedMeshRenderer skinnedMesh;

        private float lastValue;

        //-------------------------------------------------
        private void Awake()
        {
            if (this.skinnedMesh == null)
            {
                this.skinnedMesh = GetComponent<SkinnedMeshRenderer>();
            }

            if (this.linearMapping == null)
            {
                this.linearMapping = GetComponent<LinearMapping>();
            }
        }

        //-------------------------------------------------
        private void Update()
        {
            float value = this.linearMapping.value;

            //No need to set the blend if our value hasn't changed.
            if (value != this.lastValue)
            {
                float blendValue = Util.RemapNumberClamped(value, 0f, 1f, 1f, 100f);
                this.skinnedMesh.SetBlendShapeWeight(0, blendValue);
            }

            this.lastValue = value;
        }
    }
}