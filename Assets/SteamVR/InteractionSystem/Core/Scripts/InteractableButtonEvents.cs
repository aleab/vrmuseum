﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Sends simple controller button events to UnityEvents
//
//=============================================================================

using UnityEngine;
using UnityEngine.Events;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    [RequireComponent(typeof(Interactable))]
    public class InteractableButtonEvents : MonoBehaviour
    {
        public UnityEvent onTriggerDown;
        public UnityEvent onTriggerUp;
        public UnityEvent onGripDown;
        public UnityEvent onGripUp;
        public UnityEvent onTouchpadDown;
        public UnityEvent onTouchpadUp;
        public UnityEvent onTouchpadTouch;
        public UnityEvent onTouchpadRelease;

        //-------------------------------------------------
        private void Update()
        {
            for (int i = 0; i < Player.instance.handCount; i++)
            {
                Hand hand = Player.instance.GetHand(i);

                if (hand.controller != null)
                {
                    if (hand.controller.GetPressDown(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger))
                    {
                        this.onTriggerDown.Invoke();
                    }

                    if (hand.controller.GetPressUp(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger))
                    {
                        this.onTriggerUp.Invoke();
                    }

                    if (hand.controller.GetPressDown(Valve.VR.EVRButtonId.k_EButton_Grip))
                    {
                        this.onGripDown.Invoke();
                    }

                    if (hand.controller.GetPressUp(Valve.VR.EVRButtonId.k_EButton_Grip))
                    {
                        this.onGripUp.Invoke();
                    }

                    if (hand.controller.GetPressDown(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad))
                    {
                        this.onTouchpadDown.Invoke();
                    }

                    if (hand.controller.GetPressUp(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad))
                    {
                        this.onTouchpadUp.Invoke();
                    }

                    if (hand.controller.GetTouchDown(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad))
                    {
                        this.onTouchpadTouch.Invoke();
                    }

                    if (hand.controller.GetTouchUp(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad))
                    {
                        this.onTouchpadRelease.Invoke();
                    }
                }
            }
        }
    }
}