﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Handles the spawning and returning of the ItemPackage
//
//=============================================================================

using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
#endif

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    [RequireComponent(typeof(Interactable))]
    public class ItemPackageSpawner : MonoBehaviour
    {
        public ItemPackage itemPackage
        {
            get
            {
                return this._itemPackage;
            }
            set
            {
                CreatePreviewObject();
            }
        }

        public ItemPackage _itemPackage;

        private bool useItemPackagePreview = true;
        private bool useFadedPreview = false;
        private GameObject previewObject;

        public bool requireTriggerPressToTake = false;
        public bool requireTriggerPressToReturn = false;
        public bool showTriggerHint = false;

        [EnumFlags]
        public Hand.AttachmentFlags attachmentFlags = Hand.defaultAttachmentFlags;

        public string attachmentPoint;

        public bool takeBackItem = false; // if a hand enters this trigger and has the item this spawner dispenses at the top of the stack, remove it from the stack

        public bool acceptDifferentItems = false;

        private GameObject spawnedItem;
        private bool itemIsSpawned = false;

        public UnityEvent pickupEvent;
        public UnityEvent dropEvent;

        public bool justPickedUpItem = false;

        //-------------------------------------------------
        private void CreatePreviewObject()
        {
            if (!this.useItemPackagePreview)
            {
                return;
            }

            ClearPreview();

            if (this.useItemPackagePreview)
            {
                if (this.itemPackage == null)
                {
                    return;
                }

                if (this.useFadedPreview == false) // if we don't have a spawned item out there, use the regular preview
                {
                    if (this.itemPackage.previewPrefab != null)
                    {
                        this.previewObject = Instantiate(this.itemPackage.previewPrefab, this.transform.position, Quaternion.identity) as GameObject;
                        this.previewObject.transform.parent = this.transform;
                        this.previewObject.transform.localRotation = Quaternion.identity;
                    }
                }
                else // there's a spawned item out there. Use the faded preview
                {
                    if (this.itemPackage.fadedPreviewPrefab != null)
                    {
                        this.previewObject = Instantiate(this.itemPackage.fadedPreviewPrefab, this.transform.position, Quaternion.identity) as GameObject;
                        this.previewObject.transform.parent = this.transform;
                        this.previewObject.transform.localRotation = Quaternion.identity;
                    }
                }
            }
        }

        //-------------------------------------------------
        private void Start()
        {
            VerifyItemPackage();
        }

        //-------------------------------------------------
        private void VerifyItemPackage()
        {
            if (this.itemPackage == null)
            {
                ItemPackageNotValid();
            }

            if (this.itemPackage.itemPrefab == null)
            {
                ItemPackageNotValid();
            }
        }

        //-------------------------------------------------
        private void ItemPackageNotValid()
        {
            Debug.LogError("ItemPackage assigned to " + this.gameObject.name + " is not valid. Destroying this game object.");
            Destroy(this.gameObject);
        }

        //-------------------------------------------------
        private void ClearPreview()
        {
            foreach (Transform child in this.transform)
            {
                if (Time.time > 0)
                {
                    GameObject.Destroy(child.gameObject);
                }
                else
                {
                    GameObject.DestroyImmediate(child.gameObject);
                }
            }
        }

        //-------------------------------------------------
        private void Update()
        {
            if ((this.itemIsSpawned == true) && (this.spawnedItem == null))
            {
                this.itemIsSpawned = false;
                this.useFadedPreview = false;
                this.dropEvent.Invoke();
                CreatePreviewObject();
            }
        }

        //-------------------------------------------------
        private void OnHandHoverBegin(Hand hand)
        {
            ItemPackage currentAttachedItemPackage = GetAttachedItemPackage(hand);

            if (currentAttachedItemPackage == this.itemPackage) // the item at the top of the hand's stack has an associated ItemPackage
            {
                if (this.takeBackItem && !this.requireTriggerPressToReturn) // if we want to take back matching items and aren't waiting for a trigger press
                {
                    TakeBackItem(hand);
                }
            }

            if (!this.requireTriggerPressToTake) // we don't require trigger press for pickup. Spawn and attach object.
            {
                SpawnAndAttachObject(hand);
            }

            if (this.requireTriggerPressToTake && this.showTriggerHint)
            {
                ControllerButtonHints.ShowTextHint(hand, Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger, "PickUp");
            }
        }

        //-------------------------------------------------
        private void TakeBackItem(Hand hand)
        {
            RemoveMatchingItemsFromHandStack(this.itemPackage, hand);

            if (this.itemPackage.packageType == ItemPackage.ItemPackageType.TwoHanded)
            {
                RemoveMatchingItemsFromHandStack(this.itemPackage, hand.otherHand);
            }
        }

        //-------------------------------------------------
        private ItemPackage GetAttachedItemPackage(Hand hand)
        {
            GameObject currentAttachedObject = hand.currentAttachedObject;

            if (currentAttachedObject == null) // verify the hand is holding something
            {
                return null;
            }

            ItemPackageReference packageReference = hand.currentAttachedObject.GetComponent<ItemPackageReference>();
            if (packageReference == null) // verify the item in the hand is matchable
            {
                return null;
            }

            ItemPackage attachedItemPackage = packageReference.itemPackage; // return the ItemPackage reference we find.

            return attachedItemPackage;
        }

        //-------------------------------------------------
        private void HandHoverUpdate(Hand hand)
        {
            if (this.requireTriggerPressToTake)
            {
                if (hand.controller != null && hand.controller.GetHairTriggerDown())
                {
                    SpawnAndAttachObject(hand);
                }
            }
        }

        //-------------------------------------------------
        private void OnHandHoverEnd(Hand hand)
        {
            if (!this.justPickedUpItem && this.requireTriggerPressToTake && this.showTriggerHint)
            {
                ControllerButtonHints.HideTextHint(hand, Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger);
            }

            this.justPickedUpItem = false;
        }

        //-------------------------------------------------
        private void RemoveMatchingItemsFromHandStack(ItemPackage package, Hand hand)
        {
            for (int i = 0; i < hand.AttachedObjects.Count; i++)
            {
                ItemPackageReference packageReference = hand.AttachedObjects[i].attachedObject.GetComponent<ItemPackageReference>();
                if (packageReference != null)
                {
                    ItemPackage attachedObjectItemPackage = packageReference.itemPackage;
                    if ((attachedObjectItemPackage != null) && (attachedObjectItemPackage == package))
                    {
                        GameObject detachedItem = hand.AttachedObjects[i].attachedObject;
                        hand.DetachObject(detachedItem);
                    }
                }
            }
        }

        //-------------------------------------------------
        private void RemoveMatchingItemTypesFromHand(ItemPackage.ItemPackageType packageType, Hand hand)
        {
            for (int i = 0; i < hand.AttachedObjects.Count; i++)
            {
                ItemPackageReference packageReference = hand.AttachedObjects[i].attachedObject.GetComponent<ItemPackageReference>();
                if (packageReference != null)
                {
                    if (packageReference.itemPackage.packageType == packageType)
                    {
                        GameObject detachedItem = hand.AttachedObjects[i].attachedObject;
                        hand.DetachObject(detachedItem);
                    }
                }
            }
        }

        //-------------------------------------------------
        private void SpawnAndAttachObject(Hand hand)
        {
            if (hand.otherHand != null)
            {
                //If the other hand has this item package, take it back from the other hand
                ItemPackage otherHandItemPackage = GetAttachedItemPackage(hand.otherHand);
                if (otherHandItemPackage == this.itemPackage)
                {
                    TakeBackItem(hand.otherHand);
                }
            }

            if (this.showTriggerHint)
            {
                ControllerButtonHints.HideTextHint(hand, Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger);
            }

            if (this.itemPackage.otherHandItemPrefab != null)
            {
                if (hand.otherHand.hoverLocked)
                {
                    //Debug.Log( "Not attaching objects because other hand is hoverlocked and we can't deliver both items." );
                    return;
                }
            }

            // if we're trying to spawn a one-handed item, remove one and two-handed items from this hand and two-handed items from both hands
            if (this.itemPackage.packageType == ItemPackage.ItemPackageType.OneHanded)
            {
                RemoveMatchingItemTypesFromHand(ItemPackage.ItemPackageType.OneHanded, hand);
                RemoveMatchingItemTypesFromHand(ItemPackage.ItemPackageType.TwoHanded, hand);
                RemoveMatchingItemTypesFromHand(ItemPackage.ItemPackageType.TwoHanded, hand.otherHand);
            }

            // if we're trying to spawn a two-handed item, remove one and two-handed items from both hands
            if (this.itemPackage.packageType == ItemPackage.ItemPackageType.TwoHanded)
            {
                RemoveMatchingItemTypesFromHand(ItemPackage.ItemPackageType.OneHanded, hand);
                RemoveMatchingItemTypesFromHand(ItemPackage.ItemPackageType.OneHanded, hand.otherHand);
                RemoveMatchingItemTypesFromHand(ItemPackage.ItemPackageType.TwoHanded, hand);
                RemoveMatchingItemTypesFromHand(ItemPackage.ItemPackageType.TwoHanded, hand.otherHand);
            }

            this.spawnedItem = GameObject.Instantiate(this.itemPackage.itemPrefab);
            this.spawnedItem.SetActive(true);
            hand.AttachObject(this.spawnedItem, this.attachmentFlags, this.attachmentPoint);

            if ((this.itemPackage.otherHandItemPrefab != null) && (hand.otherHand.controller != null))
            {
                GameObject otherHandObjectToAttach = GameObject.Instantiate(this.itemPackage.otherHandItemPrefab);
                otherHandObjectToAttach.SetActive(true);
                hand.otherHand.AttachObject(otherHandObjectToAttach, this.attachmentFlags);
            }

            this.itemIsSpawned = true;

            this.justPickedUpItem = true;

            if (this.takeBackItem)
            {
                this.useFadedPreview = true;
                this.pickupEvent.Invoke();
                CreatePreviewObject();
            }
        }
    }
}