﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Unparents an object and keeps track of the old parent
//
//=============================================================================

using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class Unparent : MonoBehaviour
    {
        private Transform oldParent;

        //-------------------------------------------------
        private void Start()
        {
            this.oldParent = this.transform.parent;
            this.transform.parent = null;
            this.gameObject.name = this.oldParent.gameObject.name + "." + this.gameObject.name;
        }

        //-------------------------------------------------
        private void Update()
        {
            if (this.oldParent == null)
                Object.Destroy(this.gameObject);
        }

        //-------------------------------------------------
        public Transform GetOldParent()
        {
            return this.oldParent;
        }
    }
}