﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Drives a linear mapping based on position between 2 positions
//
//=============================================================================

using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    [RequireComponent(typeof(Interactable))]
    public class LinearDrive : MonoBehaviour
    {
        public Transform startPosition;
        public Transform endPosition;
        public LinearMapping linearMapping;
        public bool repositionGameObject = true;
        public bool maintainMomemntum = true;
        public float momemtumDampenRate = 5.0f;

        private float initialMappingOffset;
        private int numMappingChangeSamples = 5;
        private float[] mappingChangeSamples;
        private float prevMapping = 0.0f;
        private float mappingChangeRate;
        private int sampleCount = 0;

        //-------------------------------------------------
        private void Awake()
        {
            this.mappingChangeSamples = new float[this.numMappingChangeSamples];
        }

        //-------------------------------------------------
        private void Start()
        {
            if (this.linearMapping == null)
            {
                this.linearMapping = GetComponent<LinearMapping>();
            }

            if (this.linearMapping == null)
            {
                this.linearMapping = this.gameObject.AddComponent<LinearMapping>();
            }

            this.initialMappingOffset = this.linearMapping.value;

            if (this.repositionGameObject)
            {
                UpdateLinearMapping(this.transform);
            }
        }

        //-------------------------------------------------
        private void HandHoverUpdate(Hand hand)
        {
            if (hand.GetStandardInteractionButtonDown())
            {
                hand.HoverLock(GetComponent<Interactable>());

                this.initialMappingOffset = this.linearMapping.value - CalculateLinearMapping(hand.transform);
                this.sampleCount = 0;
                this.mappingChangeRate = 0.0f;
            }

            if (hand.GetStandardInteractionButtonUp())
            {
                hand.HoverUnlock(GetComponent<Interactable>());

                CalculateMappingChangeRate();
            }

            if (hand.GetStandardInteractionButton())
            {
                UpdateLinearMapping(hand.transform);
            }
        }

        //-------------------------------------------------
        private void CalculateMappingChangeRate()
        {
            //Compute the mapping change rate
            this.mappingChangeRate = 0.0f;
            int mappingSamplesCount = Mathf.Min(this.sampleCount, this.mappingChangeSamples.Length);
            if (mappingSamplesCount != 0)
            {
                for (int i = 0; i < mappingSamplesCount; ++i)
                {
                    this.mappingChangeRate += this.mappingChangeSamples[i];
                }
                this.mappingChangeRate /= mappingSamplesCount;
            }
        }

        //-------------------------------------------------
        private void UpdateLinearMapping(Transform tr)
        {
            this.prevMapping = this.linearMapping.value;
            this.linearMapping.value = Mathf.Clamp01(this.initialMappingOffset + CalculateLinearMapping(tr));

            this.mappingChangeSamples[this.sampleCount % this.mappingChangeSamples.Length] = (1.0f / Time.deltaTime) * (this.linearMapping.value - this.prevMapping);
            this.sampleCount++;

            if (this.repositionGameObject)
            {
                this.transform.position = Vector3.Lerp(this.startPosition.position, this.endPosition.position, this.linearMapping.value);
            }
        }

        //-------------------------------------------------
        private float CalculateLinearMapping(Transform tr)
        {
            Vector3 direction = this.endPosition.position - this.startPosition.position;
            float length = direction.magnitude;
            direction.Normalize();

            Vector3 displacement = tr.position - this.startPosition.position;

            return Vector3.Dot(displacement, direction) / length;
        }

        //-------------------------------------------------
        private void Update()
        {
            if (this.maintainMomemntum && this.mappingChangeRate != 0.0f)
            {
                //Dampen the mapping change rate and apply it to the mapping
                this.mappingChangeRate = Mathf.Lerp(this.mappingChangeRate, 0.0f, this.momemtumDampenRate * Time.deltaTime);
                this.linearMapping.value = Mathf.Clamp01(this.linearMapping.value + (this.mappingChangeRate * Time.deltaTime));

                if (this.repositionGameObject)
                {
                    this.transform.position = Vector3.Lerp(this.startPosition.position, this.endPosition.position, this.linearMapping.value);
                }
            }
        }
    }
}