﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Destroys this object when it enters a trigger
//
//=============================================================================

using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class DestroyOnTriggerEnter : MonoBehaviour
    {
        public string tagFilter;

        private bool useTag;

        //-------------------------------------------------
        private void Start()
        {
            if (!string.IsNullOrEmpty(this.tagFilter))
            {
                this.useTag = true;
            }
        }

        //-------------------------------------------------
        private void OnTriggerEnter(Collider collider)
        {
            if (!this.useTag || (this.useTag && collider.gameObject.tag == this.tagFilter))
            {
                Destroy(collider.gameObject.transform.root.gameObject);
            }
        }
    }
}