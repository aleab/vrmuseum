﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Estimates the velocity of an object based on change in position
//
//=============================================================================

using System.Collections;
using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class VelocityEstimator : MonoBehaviour
    {
        [Tooltip("How many frames to average over for computing velocity")]
        public int velocityAverageFrames = 5;

        [Tooltip("How many frames to average over for computing angular velocity")]
        public int angularVelocityAverageFrames = 11;

        public bool estimateOnAwake = false;

        private Coroutine routine;
        private int sampleCount;
        private Vector3[] velocitySamples;
        private Vector3[] angularVelocitySamples;

        //-------------------------------------------------
        public void BeginEstimatingVelocity()
        {
            FinishEstimatingVelocity();

            this.routine = StartCoroutine(EstimateVelocityCoroutine());
        }

        //-------------------------------------------------
        public void FinishEstimatingVelocity()
        {
            if (this.routine != null)
            {
                StopCoroutine(this.routine);
                this.routine = null;
            }
        }

        //-------------------------------------------------
        public Vector3 GetVelocityEstimate()
        {
            // Compute average velocity
            Vector3 velocity = Vector3.zero;
            int velocitySampleCount = Mathf.Min(this.sampleCount, this.velocitySamples.Length);
            if (velocitySampleCount != 0)
            {
                for (int i = 0; i < velocitySampleCount; i++)
                {
                    velocity += this.velocitySamples[i];
                }
                velocity *= (1.0f / velocitySampleCount);
            }

            return velocity;
        }

        //-------------------------------------------------
        public Vector3 GetAngularVelocityEstimate()
        {
            // Compute average angular velocity
            Vector3 angularVelocity = Vector3.zero;
            int angularVelocitySampleCount = Mathf.Min(this.sampleCount, this.angularVelocitySamples.Length);
            if (angularVelocitySampleCount != 0)
            {
                for (int i = 0; i < angularVelocitySampleCount; i++)
                {
                    angularVelocity += this.angularVelocitySamples[i];
                }
                angularVelocity *= (1.0f / angularVelocitySampleCount);
            }

            return angularVelocity;
        }

        //-------------------------------------------------
        public Vector3 GetAccelerationEstimate()
        {
            Vector3 average = Vector3.zero;
            for (int i = 2 + this.sampleCount - this.velocitySamples.Length; i < this.sampleCount; i++)
            {
                if (i < 2)
                    continue;

                int first = i - 2;
                int second = i - 1;

                Vector3 v1 = this.velocitySamples[first % this.velocitySamples.Length];
                Vector3 v2 = this.velocitySamples[second % this.velocitySamples.Length];
                average += v2 - v1;
            }
            average *= (1.0f / Time.deltaTime);
            return average;
        }

        //-------------------------------------------------
        private void Awake()
        {
            this.velocitySamples = new Vector3[this.velocityAverageFrames];
            this.angularVelocitySamples = new Vector3[this.angularVelocityAverageFrames];

            if (this.estimateOnAwake)
            {
                BeginEstimatingVelocity();
            }
        }

        //-------------------------------------------------
        private IEnumerator EstimateVelocityCoroutine()
        {
            this.sampleCount = 0;

            Vector3 previousPosition = this.transform.position;
            Quaternion previousRotation = this.transform.rotation;
            while (true)
            {
                yield return new WaitForEndOfFrame();

                float velocityFactor = 1.0f / Time.deltaTime;

                int v = this.sampleCount % this.velocitySamples.Length;
                int w = this.sampleCount % this.angularVelocitySamples.Length;
                this.sampleCount++;

                // Estimate linear velocity
                this.velocitySamples[v] = velocityFactor * (this.transform.position - previousPosition);

                // Estimate angular velocity
                Quaternion deltaRotation = this.transform.rotation * Quaternion.Inverse(previousRotation);

                float theta = 2.0f * Mathf.Acos(Mathf.Clamp(deltaRotation.w, -1.0f, 1.0f));
                if (theta > Mathf.PI)
                {
                    theta -= 2.0f * Mathf.PI;
                }

                Vector3 angularVelocity = new Vector3(deltaRotation.x, deltaRotation.y, deltaRotation.z);
                if (angularVelocity.sqrMagnitude > 0.0f)
                {
                    angularVelocity = theta * velocityFactor * angularVelocity.normalized;
                }

                this.angularVelocitySamples[w] = angularVelocity;

                previousPosition = this.transform.position;
                previousRotation = this.transform.rotation;
            }
        }
    }
}