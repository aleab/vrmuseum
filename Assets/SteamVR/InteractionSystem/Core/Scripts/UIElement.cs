﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: UIElement that responds to VR hands and generates UnityEvents
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    [RequireComponent(typeof(Interactable))]
    public class UIElement : MonoBehaviour
    {
        public CustomEvents.UnityEventHand onHandClick;

        private Hand currentHand;

        //-------------------------------------------------
        private void Awake()
        {
            Button button = GetComponent<Button>();
            if (button)
            {
                button.onClick.AddListener(this.OnButtonClick);
            }
        }

        //-------------------------------------------------
        private void OnHandHoverBegin(Hand hand)
        {
            this.currentHand = hand;
            InputModule.instance.HoverBegin(this.gameObject);
            ControllerButtonHints.ShowButtonHint(hand, Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger);
        }

        //-------------------------------------------------
        private void OnHandHoverEnd(Hand hand)
        {
            InputModule.instance.HoverEnd(this.gameObject);
            ControllerButtonHints.HideButtonHint(hand, Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger);
            this.currentHand = null;
        }

        //-------------------------------------------------
        private void HandHoverUpdate(Hand hand)
        {
            if (hand.GetStandardInteractionButtonDown())
            {
                InputModule.instance.Submit(this.gameObject);
                ControllerButtonHints.HideButtonHint(hand, Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger);
            }
        }

        //-------------------------------------------------
        private void OnButtonClick()
        {
            this.onHandClick.Invoke(this.currentHand);
        }
    }

#if UNITY_EDITOR

    //-------------------------------------------------------------------------
    [UnityEditor.CustomEditor(typeof(UIElement))]
    public class UIElementEditor : UnityEditor.Editor
    {
        //-------------------------------------------------
        // Custom Inspector GUI allows us to click from within the UI
        //-------------------------------------------------
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            UIElement uiElement = (UIElement)this.target;
            if (GUILayout.Button("Click"))
            {
                InputModule.instance.Submit(uiElement.gameObject);
            }
        }
    }

#endif
}