﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Spawns and attaches an object to the hand after the controller has
//			tracking
//
//=============================================================================

using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class SpawnAndAttachAfterControllerIsTracking : MonoBehaviour
    {
        private Hand hand;
        public GameObject itemPrefab;

        //-------------------------------------------------
        private void Start()
        {
            this.hand = GetComponentInParent<Hand>();
        }

        //-------------------------------------------------
        private void Update()
        {
            if (this.itemPrefab != null)
            {
                if (this.hand.controller != null)
                {
                    if (this.hand.controller.hasTracking)
                    {
                        GameObject objectToAttach = GameObject.Instantiate(this.itemPrefab);
                        objectToAttach.SetActive(true);
                        this.hand.AttachObject(objectToAttach);
                        this.hand.controller.TriggerHapticPulse(800);
                        Destroy(this.gameObject);

                        // If the player's scale has been changed the object to attach will be the wrong size.
                        // To fix this we change the object's scale back to its original, pre-attach scale.
                        objectToAttach.transform.localScale = this.itemPrefab.transform.localScale;
                    }
                }
            }
        }
    }
}