﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Sends UnityEvents for basic hand interactions
//
//=============================================================================

using UnityEngine;
using UnityEngine.Events;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    [RequireComponent(typeof(Interactable))]
    public class InteractableHoverEvents : MonoBehaviour
    {
        public UnityEvent onHandHoverBegin;
        public UnityEvent onHandHoverEnd;
        public UnityEvent onAttachedToHand;
        public UnityEvent onDetachedFromHand;

        //-------------------------------------------------
        private void OnHandHoverBegin()
        {
            this.onHandHoverBegin.Invoke();
        }

        //-------------------------------------------------
        private void OnHandHoverEnd()
        {
            this.onHandHoverEnd.Invoke();
        }

        //-------------------------------------------------
        private void OnAttachedToHand(Hand hand)
        {
            this.onAttachedToHand.Invoke();
        }

        //-------------------------------------------------
        private void OnDetachedFromHand(Hand hand)
        {
            this.onDetachedFromHand.Invoke();
        }
    }
}