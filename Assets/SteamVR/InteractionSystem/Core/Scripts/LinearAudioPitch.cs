﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Changes the pitch of this audio source based on a linear mapping
//			and a curve
//
//=============================================================================

using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class LinearAudioPitch : MonoBehaviour
    {
        public LinearMapping linearMapping;
        public AnimationCurve pitchCurve;
        public float minPitch;
        public float maxPitch;
        public bool applyContinuously = true;

        private AudioSource audioSource;

        //-------------------------------------------------
        private void Awake()
        {
            if (this.audioSource == null)
            {
                this.audioSource = GetComponent<AudioSource>();
            }

            if (this.linearMapping == null)
            {
                this.linearMapping = GetComponent<LinearMapping>();
            }
        }

        //-------------------------------------------------
        private void Update()
        {
            if (this.applyContinuously)
            {
                Apply();
            }
        }

        //-------------------------------------------------
        private void Apply()
        {
            float y = this.pitchCurve.Evaluate(this.linearMapping.value);

            this.audioSource.pitch = Mathf.Lerp(this.minPitch, this.maxPitch, y);
        }
    }
}