﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: This object can be set on fire
//
//=============================================================================

using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class FireSource : MonoBehaviour
    {
        public GameObject fireParticlePrefab;
        public bool startActive;
        private GameObject fireObject;

        public ParticleSystem customParticles;

        public bool isBurning;

        public float burnTime;
        public float ignitionDelay = 0;
        private float ignitionTime;

        private Hand hand;

        public AudioSource ignitionSound;

        public bool canSpreadFromThisSource = true;

        //-------------------------------------------------
        private void Start()
        {
            if (this.startActive)
            {
                StartBurning();
            }
        }

        //-------------------------------------------------
        private void Update()
        {
            if ((this.burnTime != 0) && (Time.time > (this.ignitionTime + this.burnTime)) && this.isBurning)
            {
                this.isBurning = false;
                if (this.customParticles != null)
                {
                    this.customParticles.Stop();
                }
                else
                {
                    Destroy(this.fireObject);
                }
            }
        }

        //-------------------------------------------------
        private void OnTriggerEnter(Collider other)
        {
            if (this.isBurning && this.canSpreadFromThisSource)
            {
                other.SendMessageUpwards("FireExposure", SendMessageOptions.DontRequireReceiver);
            }
        }

        //-------------------------------------------------
        private void FireExposure()
        {
            if (this.fireObject == null)
            {
                Invoke("StartBurning", this.ignitionDelay);
            }

            if (this.hand = GetComponentInParent<Hand>())
            {
                this.hand.controller.TriggerHapticPulse(1000);
            }
        }

        //-------------------------------------------------
        private void StartBurning()
        {
            this.isBurning = true;
            this.ignitionTime = Time.time;

            // Play the fire ignition sound if there is one
            if (this.ignitionSound != null)
            {
                this.ignitionSound.Play();
            }

            if (this.customParticles != null)
            {
                this.customParticles.Play();
            }
            else
            {
                if (this.fireParticlePrefab != null)
                {
                    this.fireObject = Instantiate(this.fireParticlePrefab, this.transform.position, this.transform.rotation) as GameObject;
                    this.fireObject.transform.parent = this.transform;
                }
            }
        }
    }
}