﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Sounds for the bow pulling back
//
//=============================================================================

using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class SoundBowClick : MonoBehaviour
    {
        public AudioClip bowClick;
        public AnimationCurve pitchTensionCurve;
        public float minPitch;
        public float maxPitch;

        private AudioSource thisAudioSource;

        //-------------------------------------------------
        private void Awake()
        {
            this.thisAudioSource = GetComponent<AudioSource>();
        }

        //-------------------------------------------------
        public void PlayBowTensionClicks(float normalizedTension)
        {
            // Tension is a float between 0 and 1. 1 being max tension and 0 being no tension
            float y = this.pitchTensionCurve.Evaluate(normalizedTension);

            this.thisAudioSource.pitch = ((this.maxPitch - this.minPitch) * y) + this.minPitch;
            this.thisAudioSource.PlayOneShot(this.bowClick);
        }
    }
}