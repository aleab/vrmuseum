﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Can be attached to the controller to collide with the balloons
//
//=============================================================================

using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class BalloonColliders : MonoBehaviour
    {
        public GameObject[] colliders;
        private Vector3[] colliderLocalPositions;
        private Quaternion[] colliderLocalRotations;

        private Rigidbody rb;

        //-------------------------------------------------
        private void Awake()
        {
            this.rb = GetComponent<Rigidbody>();

            this.colliderLocalPositions = new Vector3[this.colliders.Length];
            this.colliderLocalRotations = new Quaternion[this.colliders.Length];

            for (int i = 0; i < this.colliders.Length; ++i)
            {
                this.colliderLocalPositions[i] = this.colliders[i].transform.localPosition;
                this.colliderLocalRotations[i] = this.colliders[i].transform.localRotation;

                this.colliders[i].name = this.gameObject.name + "." + this.colliders[i].name;
            }
        }

        //-------------------------------------------------
        private void OnEnable()
        {
            for (int i = 0; i < this.colliders.Length; ++i)
            {
                this.colliders[i].transform.SetParent(this.transform);

                this.colliders[i].transform.localPosition = this.colliderLocalPositions[i];
                this.colliders[i].transform.localRotation = this.colliderLocalRotations[i];

                this.colliders[i].transform.SetParent(null);

                FixedJoint fixedJoint = this.colliders[i].AddComponent<FixedJoint>();
                fixedJoint.connectedBody = this.rb;
                fixedJoint.breakForce = Mathf.Infinity;
                fixedJoint.breakTorque = Mathf.Infinity;
                fixedJoint.enableCollision = false;
                fixedJoint.enablePreprocessing = true;

                this.colliders[i].SetActive(true);
            }
        }

        //-------------------------------------------------
        private void OnDisable()
        {
            for (int i = 0; i < this.colliders.Length; ++i)
            {
                if (this.colliders[i] != null)
                {
                    Destroy(this.colliders[i].GetComponent<FixedJoint>());

                    this.colliders[i].SetActive(false);
                }
            }
        }

        //-------------------------------------------------
        private void OnDestroy()
        {
            for (int i = 0; i < this.colliders.Length; ++i)
            {
                Destroy(this.colliders[i]);
            }
        }
    }
}