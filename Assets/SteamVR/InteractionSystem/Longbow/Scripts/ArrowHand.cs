﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: The object attached to the player's hand that spawns and fires the
//			arrow
//
//=============================================================================

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class ArrowHand : MonoBehaviour
    {
        private Hand hand;
        private Longbow bow;

        private GameObject currentArrow;
        public GameObject arrowPrefab;

        public Transform arrowNockTransform;

        public float nockDistance = 0.1f;
        public float lerpCompleteDistance = 0.08f;
        public float rotationLerpThreshold = 0.15f;
        public float positionLerpThreshold = 0.15f;

        private bool allowArrowSpawn = true;
        private bool nocked;

        private bool inNockRange = false;
        private bool arrowLerpComplete = false;

        public SoundPlayOneshot arrowSpawnSound;

        private AllowTeleportWhileAttachedToHand allowTeleport = null;

        public int maxArrowCount = 10;
        private List<GameObject> arrowList;

        //-------------------------------------------------
        private void Awake()
        {
            this.allowTeleport = GetComponent<AllowTeleportWhileAttachedToHand>();
            this.allowTeleport.teleportAllowed = true;
            this.allowTeleport.overrideHoverLock = false;

            this.arrowList = new List<GameObject>();
        }

        //-------------------------------------------------
        private void OnAttachedToHand(Hand attachedHand)
        {
            this.hand = attachedHand;
            FindBow();
        }

        //-------------------------------------------------
        private GameObject InstantiateArrow()
        {
            GameObject arrow = Instantiate(this.arrowPrefab, this.arrowNockTransform.position, this.arrowNockTransform.rotation) as GameObject;
            arrow.name = "Bow Arrow";
            arrow.transform.parent = this.arrowNockTransform;
            Util.ResetTransform(arrow.transform);

            this.arrowList.Add(arrow);

            while (this.arrowList.Count > this.maxArrowCount)
            {
                GameObject oldArrow = this.arrowList[0];
                this.arrowList.RemoveAt(0);
                if (oldArrow)
                {
                    Destroy(oldArrow);
                }
            }

            return arrow;
        }

        //-------------------------------------------------
        private void HandAttachedUpdate(Hand hand)
        {
            if (this.bow == null)
            {
                FindBow();
            }

            if (this.bow == null)
            {
                return;
            }

            if (this.allowArrowSpawn && (this.currentArrow == null)) // If we're allowed to have an active arrow in hand but don't yet, spawn one
            {
                this.currentArrow = InstantiateArrow();
                this.arrowSpawnSound.Play();
            }

            float distanceToNockPosition = Vector3.Distance(this.transform.parent.position, this.bow.nockTransform.position);

            // If there's an arrow spawned in the hand and it's not nocked yet
            if (!this.nocked)
            {
                // If we're close enough to nock position that we want to start arrow rotation lerp, do so
                if (distanceToNockPosition < this.rotationLerpThreshold)
                {
                    float lerp = Util.RemapNumber(distanceToNockPosition, this.rotationLerpThreshold, this.lerpCompleteDistance, 0, 1);

                    this.arrowNockTransform.rotation = Quaternion.Lerp(this.arrowNockTransform.parent.rotation, this.bow.nockRestTransform.rotation, lerp);
                }
                else // Not close enough for rotation lerp, reset rotation
                {
                    this.arrowNockTransform.localRotation = Quaternion.identity;
                }

                // If we're close enough to the nock position that we want to start arrow position lerp, do so
                if (distanceToNockPosition < this.positionLerpThreshold)
                {
                    float posLerp = Util.RemapNumber(distanceToNockPosition, this.positionLerpThreshold, this.lerpCompleteDistance, 0, 1);

                    posLerp = Mathf.Clamp(posLerp, 0f, 1f);

                    this.arrowNockTransform.position = Vector3.Lerp(this.arrowNockTransform.parent.position, this.bow.nockRestTransform.position, posLerp);
                }
                else // Not close enough for position lerp, reset position
                {
                    this.arrowNockTransform.position = this.arrowNockTransform.parent.position;
                }

                // Give a haptic tick when lerp is visually complete
                if (distanceToNockPosition < this.lerpCompleteDistance)
                {
                    if (!this.arrowLerpComplete)
                    {
                        this.arrowLerpComplete = true;
                        hand.controller.TriggerHapticPulse(500);
                    }
                }
                else
                {
                    if (this.arrowLerpComplete)
                    {
                        this.arrowLerpComplete = false;
                    }
                }

                // Allow nocking the arrow when controller is close enough
                if (distanceToNockPosition < this.nockDistance)
                {
                    if (!this.inNockRange)
                    {
                        this.inNockRange = true;
                        this.bow.ArrowInPosition();
                    }
                }
                else
                {
                    if (this.inNockRange)
                    {
                        this.inNockRange = false;
                    }
                }

                // If arrow is close enough to the nock position and we're pressing the trigger, and we're not nocked yet, Nock
                if ((distanceToNockPosition < this.nockDistance) && hand.controller.GetPress(SteamVR_Controller.ButtonMask.Trigger) && !this.nocked)
                {
                    if (this.currentArrow == null)
                    {
                        this.currentArrow = InstantiateArrow();
                    }

                    this.nocked = true;
                    this.bow.StartNock(this);
                    hand.HoverLock(GetComponent<Interactable>());
                    this.allowTeleport.teleportAllowed = false;
                    this.currentArrow.transform.parent = this.bow.nockTransform;
                    Util.ResetTransform(this.currentArrow.transform);
                    Util.ResetTransform(this.arrowNockTransform);
                }
            }

            // If arrow is nocked, and we release the trigger
            if (this.nocked && (!hand.controller.GetPress(SteamVR_Controller.ButtonMask.Trigger) || hand.controller.GetPressUp(SteamVR_Controller.ButtonMask.Trigger)))
            {
                if (this.bow.pulled) // If bow is pulled back far enough, fire arrow, otherwise reset arrow in arrowhand
                {
                    FireArrow();
                }
                else
                {
                    this.arrowNockTransform.rotation = this.currentArrow.transform.rotation;
                    this.currentArrow.transform.parent = this.arrowNockTransform;
                    Util.ResetTransform(this.currentArrow.transform);
                    this.nocked = false;
                    this.bow.ReleaseNock();
                    hand.HoverUnlock(GetComponent<Interactable>());
                    this.allowTeleport.teleportAllowed = true;
                }

                this.bow.StartRotationLerp(); // Arrow is releasing from the bow, tell the bow to lerp back to controller rotation
            }
        }

        //-------------------------------------------------
        private void OnDetachedFromHand(Hand hand)
        {
            Destroy(this.gameObject);
        }

        //-------------------------------------------------
        private void FireArrow()
        {
            this.currentArrow.transform.parent = null;

            Arrow arrow = this.currentArrow.GetComponent<Arrow>();
            arrow.shaftRB.isKinematic = false;
            arrow.shaftRB.useGravity = true;
            arrow.shaftRB.transform.GetComponent<BoxCollider>().enabled = true;

            arrow.arrowHeadRB.isKinematic = false;
            arrow.arrowHeadRB.useGravity = true;
            arrow.arrowHeadRB.transform.GetComponent<BoxCollider>().enabled = true;

            arrow.arrowHeadRB.AddForce(this.currentArrow.transform.forward * this.bow.GetArrowVelocity(), ForceMode.VelocityChange);
            arrow.arrowHeadRB.AddTorque(this.currentArrow.transform.forward * 10);

            this.nocked = false;

            this.currentArrow.GetComponent<Arrow>().ArrowReleased(this.bow.GetArrowVelocity());
            this.bow.ArrowReleased();

            this.allowArrowSpawn = false;
            Invoke("EnableArrowSpawn", 0.5f);
            StartCoroutine(ArrowReleaseHaptics());

            this.currentArrow = null;
            this.allowTeleport.teleportAllowed = true;
        }

        //-------------------------------------------------
        private void EnableArrowSpawn()
        {
            this.allowArrowSpawn = true;
        }

        //-------------------------------------------------
        private IEnumerator ArrowReleaseHaptics()
        {
            yield return new WaitForSeconds(0.05f);

            this.hand.otherHand.controller.TriggerHapticPulse(1500);
            yield return new WaitForSeconds(0.05f);

            this.hand.otherHand.controller.TriggerHapticPulse(800);
            yield return new WaitForSeconds(0.05f);

            this.hand.otherHand.controller.TriggerHapticPulse(500);
            yield return new WaitForSeconds(0.05f);

            this.hand.otherHand.controller.TriggerHapticPulse(300);
        }

        //-------------------------------------------------
        private void OnHandFocusLost(Hand hand)
        {
            this.gameObject.SetActive(false);
        }

        //-------------------------------------------------
        private void OnHandFocusAcquired(Hand hand)
        {
            this.gameObject.SetActive(true);
        }

        //-------------------------------------------------
        private void FindBow()
        {
            this.bow = this.hand.otherHand.GetComponentInChildren<Longbow>();
        }
    }
}