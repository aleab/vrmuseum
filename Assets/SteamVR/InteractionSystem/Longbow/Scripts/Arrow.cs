﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: The arrow for the longbow
//
//=============================================================================

using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class Arrow : MonoBehaviour
    {
        public ParticleSystem glintParticle;
        public Rigidbody arrowHeadRB;
        public Rigidbody shaftRB;

        public PhysicMaterial targetPhysMaterial;

        private Vector3 prevPosition;
        private Quaternion prevRotation;
        private Vector3 prevVelocity;
        private Vector3 prevHeadPosition;

        public SoundPlayOneshot fireReleaseSound;
        public SoundPlayOneshot airReleaseSound;
        public SoundPlayOneshot hitTargetSound;

        public PlaySound hitGroundSound;

        private bool inFlight;
        private bool released;
        private bool hasSpreadFire = false;

        private int travelledFrames = 0;

        private GameObject scaleParentObject = null;

        //-------------------------------------------------
        private void Start()
        {
            Physics.IgnoreCollision(this.shaftRB.GetComponent<Collider>(), Player.instance.headCollider);
        }

        //-------------------------------------------------
        private void FixedUpdate()
        {
            if (this.released && this.inFlight)
            {
                this.prevPosition = this.transform.position;
                this.prevRotation = this.transform.rotation;
                this.prevVelocity = GetComponent<Rigidbody>().velocity;
                this.prevHeadPosition = this.arrowHeadRB.transform.position;
                this.travelledFrames++;
            }
        }

        //-------------------------------------------------
        public void ArrowReleased(float inputVelocity)
        {
            this.inFlight = true;
            this.released = true;

            this.airReleaseSound.Play();

            if (this.glintParticle != null)
            {
                this.glintParticle.Play();
            }

            if (this.gameObject.GetComponentInChildren<FireSource>().isBurning)
            {
                this.fireReleaseSound.Play();
            }

            // Check if arrow is shot inside or too close to an object
            RaycastHit[] hits = Physics.SphereCastAll(this.transform.position, 0.01f, this.transform.forward, 0.80f, Physics.DefaultRaycastLayers, QueryTriggerInteraction.Ignore);
            foreach (RaycastHit hit in hits)
            {
                if (hit.collider.gameObject != this.gameObject && hit.collider.gameObject != this.arrowHeadRB.gameObject && hit.collider != Player.instance.headCollider)
                {
                    Destroy(this.gameObject);
                    return;
                }
            }

            this.travelledFrames = 0;
            this.prevPosition = this.transform.position;
            this.prevRotation = this.transform.rotation;
            this.prevHeadPosition = this.arrowHeadRB.transform.position;
            this.prevVelocity = GetComponent<Rigidbody>().velocity;

            Destroy(this.gameObject, 30);
        }

        //-------------------------------------------------
        private void OnCollisionEnter(Collision collision)
        {
            if (this.inFlight)
            {
                Rigidbody rb = GetComponent<Rigidbody>();
                float rbSpeed = rb.velocity.sqrMagnitude;
                bool canStick = (this.targetPhysMaterial != null && collision.collider.sharedMaterial == this.targetPhysMaterial && rbSpeed > 0.2f);
                bool hitBalloon = collision.collider.gameObject.GetComponent<Balloon>() != null;

                if (this.travelledFrames < 2 && !canStick)
                {
                    // Reset transform but halve your velocity
                    this.transform.position = this.prevPosition - this.prevVelocity * Time.deltaTime;
                    this.transform.rotation = this.prevRotation;

                    Vector3 reflfectDir = Vector3.Reflect(this.arrowHeadRB.velocity, collision.contacts[0].normal);
                    this.arrowHeadRB.velocity = reflfectDir * 0.25f;
                    this.shaftRB.velocity = reflfectDir * 0.25f;

                    this.travelledFrames = 0;
                    return;
                }

                if (this.glintParticle != null)
                {
                    this.glintParticle.Stop(true);
                }

                // Only play hit sounds if we're moving quickly
                if (rbSpeed > 0.1f)
                {
                    this.hitGroundSound.Play();
                }

                FireSource arrowFire = this.gameObject.GetComponentInChildren<FireSource>();
                FireSource fireSourceOnTarget = collision.collider.GetComponentInParent<FireSource>();

                if (arrowFire != null && arrowFire.isBurning && (fireSourceOnTarget != null))
                {
                    if (!this.hasSpreadFire)
                    {
                        collision.collider.gameObject.SendMessageUpwards("FireExposure", this.gameObject, SendMessageOptions.DontRequireReceiver);
                        this.hasSpreadFire = true;
                    }
                }
                else
                {
                    // Only count collisions with good speed so that arrows on the ground can't deal damage
                    // always pop balloons
                    if (rbSpeed > 0.1f || hitBalloon)
                    {
                        collision.collider.gameObject.SendMessageUpwards("ApplyDamage", SendMessageOptions.DontRequireReceiver);
                        this.gameObject.SendMessage("HasAppliedDamage", SendMessageOptions.DontRequireReceiver);
                    }
                }

                if (hitBalloon)
                {
                    // Revert my physics properties cause I don't want balloons to influence my travel
                    this.transform.position = this.prevPosition;
                    this.transform.rotation = this.prevRotation;
                    this.arrowHeadRB.velocity = this.prevVelocity;
                    Physics.IgnoreCollision(this.arrowHeadRB.GetComponent<Collider>(), collision.collider);
                    Physics.IgnoreCollision(this.shaftRB.GetComponent<Collider>(), collision.collider);
                }

                if (canStick)
                {
                    StickInTarget(collision, this.travelledFrames < 2);
                }

                // Player Collision Check (self hit)
                if (Player.instance && collision.collider == Player.instance.headCollider)
                {
                    Player.instance.PlayerShotSelf();
                }
            }
        }

        //-------------------------------------------------
        private void StickInTarget(Collision collision, bool bSkipRayCast)
        {
            Vector3 prevForward = this.prevRotation * Vector3.forward;

            // Only stick in target if the collider is front of the arrow head
            if (!bSkipRayCast)
            {
                RaycastHit[] hitInfo;
                hitInfo = Physics.RaycastAll(this.prevHeadPosition - this.prevVelocity * Time.deltaTime, prevForward, this.prevVelocity.magnitude * Time.deltaTime * 2.0f);
                bool properHit = false;
                for (int i = 0; i < hitInfo.Length; ++i)
                {
                    RaycastHit hit = hitInfo[i];

                    if (hit.collider == collision.collider)
                    {
                        properHit = true;
                        break;
                    }
                }

                if (!properHit)
                {
                    return;
                }
            }

            Destroy(this.glintParticle);

            this.inFlight = false;

            this.shaftRB.velocity = Vector3.zero;
            this.shaftRB.angularVelocity = Vector3.zero;
            this.shaftRB.isKinematic = true;
            this.shaftRB.useGravity = false;
            this.shaftRB.transform.GetComponent<BoxCollider>().enabled = false;

            this.arrowHeadRB.velocity = Vector3.zero;
            this.arrowHeadRB.angularVelocity = Vector3.zero;
            this.arrowHeadRB.isKinematic = true;
            this.arrowHeadRB.useGravity = false;
            this.arrowHeadRB.transform.GetComponent<BoxCollider>().enabled = false;

            this.hitTargetSound.Play();

            // If the hit item has a parent, dock an empty object to that
            // this fixes an issue with scaling hierarchy. I suspect this is not sustainable for a large object / scaling hierarchy.
            this.scaleParentObject = new GameObject("Arrow Scale Parent");
            Transform parentTransform = collision.collider.transform;

            // Don't do this for weebles because of how it has a fixed joint
            ExplosionWobble wobble = collision.collider.gameObject.GetComponent<ExplosionWobble>();
            if (!wobble)
            {
                if (parentTransform.parent)
                {
                    parentTransform = parentTransform.parent;
                }
            }

            this.scaleParentObject.transform.parent = parentTransform;

            // Move the arrow to the place on the target collider we were expecting to hit prior to the impact itself knocking it around
            this.transform.parent = this.scaleParentObject.transform;
            this.transform.rotation = this.prevRotation;
            this.transform.position = this.prevPosition;
            this.transform.position = collision.contacts[0].point - this.transform.forward * (0.75f - (Util.RemapNumberClamped(this.prevVelocity.magnitude, 0f, 10f, 0.0f, 0.1f) + Random.Range(0.0f, 0.05f)));
        }

        //-------------------------------------------------
        private void OnDestroy()
        {
            if (this.scaleParentObject != null)
            {
                Destroy(this.scaleParentObject);
            }
        }
    }
}