﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Spawns balloons
//
//=============================================================================

using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    public class BalloonSpawner : MonoBehaviour
    {
        public float minSpawnTime = 5f;
        public float maxSpawnTime = 15f;
        private float nextSpawnTime;
        public GameObject balloonPrefab;

        public bool autoSpawn = true;
        public bool spawnAtStartup = true;

        public bool playSounds = true;
        public SoundPlayOneshot inflateSound;
        public SoundPlayOneshot stretchSound;

        public bool sendSpawnMessageToParent = false;

        public float scale = 1f;

        public Transform spawnDirectionTransform;
        public float spawnForce;

        public bool attachBalloon = false;

        public Balloon.BalloonColor color = Balloon.BalloonColor.Random;

        //-------------------------------------------------
        private void Start()
        {
            if (this.balloonPrefab == null)
            {
                return;
            }

            if (this.autoSpawn && this.spawnAtStartup)
            {
                SpawnBalloon(this.color);
                this.nextSpawnTime = Random.Range(this.minSpawnTime, this.maxSpawnTime) + Time.time;
            }
        }

        //-------------------------------------------------
        private void Update()
        {
            if (this.balloonPrefab == null)
            {
                return;
            }

            if ((Time.time > this.nextSpawnTime) && this.autoSpawn)
            {
                SpawnBalloon(this.color);
                this.nextSpawnTime = Random.Range(this.minSpawnTime, this.maxSpawnTime) + Time.time;
            }
        }

        //-------------------------------------------------
        public GameObject SpawnBalloon(Balloon.BalloonColor color = Balloon.BalloonColor.Red)
        {
            if (this.balloonPrefab == null)
            {
                return null;
            }
            GameObject balloon = Instantiate(this.balloonPrefab, this.transform.position, this.transform.rotation) as GameObject;
            balloon.transform.localScale = new Vector3(this.scale, this.scale, this.scale);
            if (this.attachBalloon)
            {
                balloon.transform.parent = this.transform;
            }

            if (this.sendSpawnMessageToParent)
            {
                if (this.transform.parent != null)
                {
                    this.transform.parent.SendMessage("OnBalloonSpawned", balloon, SendMessageOptions.DontRequireReceiver);
                }
            }

            if (this.playSounds)
            {
                if (this.inflateSound != null)
                {
                    this.inflateSound.Play();
                }
                if (this.stretchSound != null)
                {
                    this.stretchSound.Play();
                }
            }
            balloon.GetComponentInChildren<Balloon>().SetColor(color);
            if (this.spawnDirectionTransform != null)
            {
                balloon.GetComponentInChildren<Rigidbody>().AddForce(this.spawnDirectionTransform.forward * this.spawnForce);
            }

            return balloon;
        }

        //-------------------------------------------------
        public void SpawnBalloonFromEvent(int color)
        {
            // Copy of SpawnBalloon using int because we can't pass in enums through the event system
            SpawnBalloon((Balloon.BalloonColor)color);
        }
    }
}