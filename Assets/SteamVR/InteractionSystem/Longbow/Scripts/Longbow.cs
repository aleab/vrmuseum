﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: The bow
//
//=============================================================================

using System.Collections;
using UnityEngine;

namespace Valve.VR.InteractionSystem
{
    //-------------------------------------------------------------------------
    [RequireComponent(typeof(Interactable))]
    public class Longbow : MonoBehaviour
    {
        public enum Handedness { Left, Right };

        public Handedness currentHandGuess = Handedness.Left;
        private float timeOfPossibleHandSwitch = 0f;
        private float timeBeforeConfirmingHandSwitch = 1.5f;
        private bool possibleHandSwitch = false;

        public Transform pivotTransform;
        public Transform handleTransform;

        private Hand hand;
        private ArrowHand arrowHand;

        public Transform nockTransform;
        public Transform nockRestTransform;

        public bool autoSpawnArrowHand = true;
        public ItemPackage arrowHandItemPackage;
        public GameObject arrowHandPrefab;

        public bool nocked;
        public bool pulled;

        private const float minPull = 0.05f;
        private const float maxPull = 0.5f;
        private float nockDistanceTravelled = 0f;
        private float hapticDistanceThreshold = 0.01f;
        private float lastTickDistance;
        private const float bowPullPulseStrengthLow = 100;
        private const float bowPullPulseStrengthHigh = 500;
        private Vector3 bowLeftVector;

        public float arrowMinVelocity = 3f;
        public float arrowMaxVelocity = 30f;
        private float arrowVelocity = 30f;

        private float minStrainTickTime = 0.1f;
        private float maxStrainTickTime = 0.5f;
        private float nextStrainTick = 0;

        private bool lerpBackToZeroRotation;
        private float lerpStartTime;
        private float lerpDuration = 0.15f;
        private Quaternion lerpStartRotation;

        private float nockLerpStartTime;

        private Quaternion nockLerpStartRotation;

        public float drawOffset = 0.06f;

        public LinearMapping bowDrawLinearMapping;

        private bool deferNewPoses = false;
        private Vector3 lateUpdatePos;
        private Quaternion lateUpdateRot;

        public SoundBowClick drawSound;
        private float drawTension;
        public SoundPlayOneshot arrowSlideSound;
        public SoundPlayOneshot releaseSound;
        public SoundPlayOneshot nockSound;

        private SteamVR_Events.Action newPosesAppliedAction;

        //-------------------------------------------------
        private void OnAttachedToHand(Hand attachedHand)
        {
            this.hand = attachedHand;
        }

        //-------------------------------------------------
        private void Awake()
        {
            this.newPosesAppliedAction = SteamVR_Events.NewPosesAppliedAction(this.OnNewPosesApplied);
        }

        //-------------------------------------------------
        private void OnEnable()
        {
            this.newPosesAppliedAction.enabled = true;
        }

        //-------------------------------------------------
        private void OnDisable()
        {
            this.newPosesAppliedAction.enabled = false;
        }

        //-------------------------------------------------
        private void LateUpdate()
        {
            if (this.deferNewPoses)
            {
                this.lateUpdatePos = this.transform.position;
                this.lateUpdateRot = this.transform.rotation;
            }
        }

        //-------------------------------------------------
        private void OnNewPosesApplied()
        {
            if (this.deferNewPoses)
            {
                // Set longbow object back to previous pose position to avoid jitter
                this.transform.position = this.lateUpdatePos;
                this.transform.rotation = this.lateUpdateRot;

                this.deferNewPoses = false;
            }
        }

        //-------------------------------------------------
        private void HandAttachedUpdate(Hand hand)
        {
            // Reset transform since we cheated it right after getting poses on previous frame
            this.transform.localPosition = Vector3.zero;
            this.transform.localRotation = Quaternion.identity;

            // Update handedness guess
            EvaluateHandedness();

            if (this.nocked)
            {
                this.deferNewPoses = true;

                Vector3 nockToarrowHand = (this.arrowHand.arrowNockTransform.parent.position - this.nockRestTransform.position); // Vector from bow nock transform to arrowhand nock transform - used to align bow when drawing

                // Align bow
                // Time lerp value used for ramping into drawn bow orientation
                float lerp = Util.RemapNumberClamped(Time.time, this.nockLerpStartTime, (this.nockLerpStartTime + this.lerpDuration), 0f, 1f);

                float pullLerp = Util.RemapNumberClamped(nockToarrowHand.magnitude, minPull, maxPull, 0f, 1f); // Normalized current state of bow draw 0 - 1

                Vector3 arrowNockTransformToHeadset = ((Player.instance.hmdTransform.position + (Vector3.down * 0.05f)) - this.arrowHand.arrowNockTransform.parent.position).normalized;
                Vector3 arrowHandPosition = (this.arrowHand.arrowNockTransform.parent.position + ((arrowNockTransformToHeadset * this.drawOffset) * pullLerp)); // Use this line to lerp arrowHand nock position
                                                                                                                                                      //Vector3 arrowHandPosition = arrowHand.arrowNockTransform.position; // Use this line if we don't want to lerp arrowHand nock position

                Vector3 pivotToString = (arrowHandPosition - this.pivotTransform.position).normalized;
                Vector3 pivotToLowerHandle = (this.handleTransform.position - this.pivotTransform.position).normalized;
                this.bowLeftVector = -Vector3.Cross(pivotToLowerHandle, pivotToString);
                this.pivotTransform.rotation = Quaternion.Lerp(this.nockLerpStartRotation, Quaternion.LookRotation(pivotToString, this.bowLeftVector), lerp);

                // Move nock position
                if (Vector3.Dot(nockToarrowHand, -this.nockTransform.forward) > 0)
                {
                    float distanceToarrowHand = nockToarrowHand.magnitude * lerp;

                    this.nockTransform.localPosition = new Vector3(0f, 0f, Mathf.Clamp(-distanceToarrowHand, -maxPull, 0f));

                    this.nockDistanceTravelled = -this.nockTransform.localPosition.z;

                    this.arrowVelocity = Util.RemapNumber(this.nockDistanceTravelled, minPull, maxPull, this.arrowMinVelocity, this.arrowMaxVelocity);

                    this.drawTension = Util.RemapNumberClamped(this.nockDistanceTravelled, 0, maxPull, 0f, 1f);

                    this.bowDrawLinearMapping.value = this.drawTension; // Send drawTension value to LinearMapping script, which drives the bow draw animation

                    if (this.nockDistanceTravelled > minPull)
                    {
                        this.pulled = true;
                    }
                    else
                    {
                        this.pulled = false;
                    }

                    if ((this.nockDistanceTravelled > (this.lastTickDistance + this.hapticDistanceThreshold)) || this.nockDistanceTravelled < (this.lastTickDistance - this.hapticDistanceThreshold))
                    {
                        ushort hapticStrength = (ushort)Util.RemapNumber(this.nockDistanceTravelled, 0, maxPull, bowPullPulseStrengthLow, bowPullPulseStrengthHigh);
                        hand.controller.TriggerHapticPulse(hapticStrength);
                        hand.otherHand.controller.TriggerHapticPulse(hapticStrength);

                        this.drawSound.PlayBowTensionClicks(this.drawTension);

                        this.lastTickDistance = this.nockDistanceTravelled;
                    }

                    if (this.nockDistanceTravelled >= maxPull)
                    {
                        if (Time.time > this.nextStrainTick)
                        {
                            hand.controller.TriggerHapticPulse(400);
                            hand.otherHand.controller.TriggerHapticPulse(400);

                            this.drawSound.PlayBowTensionClicks(this.drawTension);

                            this.nextStrainTick = Time.time + Random.Range(this.minStrainTickTime, this.maxStrainTickTime);
                        }
                    }
                }
                else
                {
                    this.nockTransform.localPosition = new Vector3(0f, 0f, 0f);

                    this.bowDrawLinearMapping.value = 0f;
                }
            }
            else
            {
                if (this.lerpBackToZeroRotation)
                {
                    float lerp = Util.RemapNumber(Time.time, this.lerpStartTime, this.lerpStartTime + this.lerpDuration, 0, 1);

                    this.pivotTransform.localRotation = Quaternion.Lerp(this.lerpStartRotation, Quaternion.identity, lerp);

                    if (lerp >= 1)
                    {
                        this.lerpBackToZeroRotation = false;
                    }
                }
            }
        }

        //-------------------------------------------------
        public void ArrowReleased()
        {
            this.nocked = false;
            this.hand.HoverUnlock(GetComponent<Interactable>());
            this.hand.otherHand.HoverUnlock(this.arrowHand.GetComponent<Interactable>());

            if (this.releaseSound != null)
            {
                this.releaseSound.Play();
            }

            this.StartCoroutine(this.ResetDrawAnim());
        }

        //-------------------------------------------------
        private IEnumerator ResetDrawAnim()
        {
            float startTime = Time.time;
            float startLerp = this.drawTension;

            while (Time.time < (startTime + 0.02f))
            {
                float lerp = Util.RemapNumberClamped(Time.time, startTime, startTime + 0.02f, startLerp, 0f);
                this.bowDrawLinearMapping.value = lerp;
                yield return null;
            }

            this.bowDrawLinearMapping.value = 0;

            yield break;
        }

        //-------------------------------------------------
        public float GetArrowVelocity()
        {
            return this.arrowVelocity;
        }

        //-------------------------------------------------
        public void StartRotationLerp()
        {
            this.lerpStartTime = Time.time;
            this.lerpBackToZeroRotation = true;
            this.lerpStartRotation = this.pivotTransform.localRotation;

            Util.ResetTransform(this.nockTransform);
        }

        //-------------------------------------------------
        public void StartNock(ArrowHand currentArrowHand)
        {
            this.arrowHand = currentArrowHand;
            this.hand.HoverLock(GetComponent<Interactable>());
            this.nocked = true;
            this.nockLerpStartTime = Time.time;
            this.nockLerpStartRotation = this.pivotTransform.rotation;

            // Sound of arrow sliding on nock as it's being pulled back
            this.arrowSlideSound.Play();

            // Decide which hand we're drawing with and lerp to the correct side
            DoHandednessCheck();
        }

        //-------------------------------------------------
        private void EvaluateHandedness()
        {
            Hand.HandType handType = this.hand.GuessCurrentHandType();

            if (handType == Hand.HandType.Left)// Bow hand is further left than arrow hand.
            {
                // We were considering a switch, but the current controller orientation matches our currently assigned handedness, so no longer consider a switch
                if (this.possibleHandSwitch && this.currentHandGuess == Handedness.Left)
                {
                    this.possibleHandSwitch = false;
                }

                // If we previously thought the bow was right-handed, and were not already considering switching, start considering a switch
                if (!this.possibleHandSwitch && this.currentHandGuess == Handedness.Right)
                {
                    this.possibleHandSwitch = true;
                    this.timeOfPossibleHandSwitch = Time.time;
                }

                // If we are considering a handedness switch, and it's been this way long enough, switch
                if (this.possibleHandSwitch && Time.time > (this.timeOfPossibleHandSwitch + this.timeBeforeConfirmingHandSwitch))
                {
                    this.currentHandGuess = Handedness.Left;
                    this.possibleHandSwitch = false;
                }
            }
            else // Bow hand is further right than arrow hand
            {
                // We were considering a switch, but the current controller orientation matches our currently assigned handedness, so no longer consider a switch
                if (this.possibleHandSwitch && this.currentHandGuess == Handedness.Right)
                {
                    this.possibleHandSwitch = false;
                }

                // If we previously thought the bow was right-handed, and were not already considering switching, start considering a switch
                if (!this.possibleHandSwitch && this.currentHandGuess == Handedness.Left)
                {
                    this.possibleHandSwitch = true;
                    this.timeOfPossibleHandSwitch = Time.time;
                }

                // If we are considering a handedness switch, and it's been this way long enough, switch
                if (this.possibleHandSwitch && Time.time > (this.timeOfPossibleHandSwitch + this.timeBeforeConfirmingHandSwitch))
                {
                    this.currentHandGuess = Handedness.Right;
                    this.possibleHandSwitch = false;
                }
            }
        }

        //-------------------------------------------------
        private void DoHandednessCheck()
        {
            // Based on our current best guess about hand, switch bow orientation and arrow lerp direction
            if (this.currentHandGuess == Handedness.Left)
            {
                this.pivotTransform.localScale = new Vector3(1f, 1f, 1f);
            }
            else
            {
                this.pivotTransform.localScale = new Vector3(1f, -1f, 1f);
            }
        }

        //-------------------------------------------------
        public void ArrowInPosition()
        {
            DoHandednessCheck();

            if (this.nockSound != null)
            {
                this.nockSound.Play();
            }
        }

        //-------------------------------------------------
        public void ReleaseNock()
        {
            // ArrowHand tells us to do this when we release the buttons when bow is nocked but not drawn far enough
            this.nocked = false;
            this.hand.HoverUnlock(GetComponent<Interactable>());
            this.StartCoroutine(this.ResetDrawAnim());
        }

        //-------------------------------------------------
        private void ShutDown()
        {
            if (this.hand != null && this.hand.otherHand.currentAttachedObject != null)
            {
                if (this.hand.otherHand.currentAttachedObject.GetComponent<ItemPackageReference>() != null)
                {
                    if (this.hand.otherHand.currentAttachedObject.GetComponent<ItemPackageReference>().itemPackage == this.arrowHandItemPackage)
                    {
                        this.hand.otherHand.DetachObject(this.hand.otherHand.currentAttachedObject);
                    }
                }
            }
        }

        //-------------------------------------------------
        private void OnHandFocusLost(Hand hand)
        {
            this.gameObject.SetActive(false);
        }

        //-------------------------------------------------
        private void OnHandFocusAcquired(Hand hand)
        {
            this.gameObject.SetActive(true);
            OnAttachedToHand(hand);
        }

        //-------------------------------------------------
        private void OnDetachedFromHand(Hand hand)
        {
            Destroy(this.gameObject);
        }

        //-------------------------------------------------
        private void OnDestroy()
        {
            ShutDown();
        }
    }
}