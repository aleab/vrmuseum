﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Custom inspector display for SteamVR_Camera
//
//=============================================================================

using System.IO;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SteamVR_Camera)), CanEditMultipleObjects]
public class SteamVR_Editor : Editor
{
    private int bannerHeight = 150;
    private Texture logo;

    private SerializedProperty script, wireframe;

    private string GetResourcePath()
    {
        var ms = MonoScript.FromScriptableObject(this);
        var path = AssetDatabase.GetAssetPath(ms);
        path = Path.GetDirectoryName(path);
        return path.Substring(0, path.Length - "Editor".Length) + "Textures/";
    }

    private void OnEnable()
    {
        var resourcePath = GetResourcePath();

        this.logo = AssetDatabase.LoadAssetAtPath<Texture2D>(resourcePath + "logo.png");

        this.script = this.serializedObject.FindProperty("m_Script");

        this.wireframe = this.serializedObject.FindProperty("wireframe");

        foreach (SteamVR_Camera target in this.targets)
            target.ForceLast();
    }

    public override void OnInspectorGUI()
    {
        this.serializedObject.Update();

        var rect = GUILayoutUtility.GetRect(Screen.width - 38, this.bannerHeight, GUI.skin.box);
        if (this.logo)
            GUI.DrawTexture(rect, this.logo, ScaleMode.ScaleToFit);

        if (!Application.isPlaying)
        {
            var expand = false;
            var collapse = false;
            foreach (SteamVR_Camera target in this.targets)
            {
                if (AssetDatabase.Contains(target))
                    continue;
                if (target.isExpanded)
                    collapse = true;
                else
                    expand = true;
            }

            if (expand)
            {
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Expand"))
                {
                    foreach (SteamVR_Camera target in this.targets)
                    {
                        if (AssetDatabase.Contains(target))
                            continue;
                        if (!target.isExpanded)
                        {
                            target.Expand();
                            EditorUtility.SetDirty(target);
                        }
                    }
                }
                GUILayout.Space(18);
                GUILayout.EndHorizontal();
            }

            if (collapse)
            {
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Collapse"))
                {
                    foreach (SteamVR_Camera target in this.targets)
                    {
                        if (AssetDatabase.Contains(target))
                            continue;
                        if (target.isExpanded)
                        {
                            target.Collapse();
                            EditorUtility.SetDirty(target);
                        }
                    }
                }
                GUILayout.Space(18);
                GUILayout.EndHorizontal();
            }
        }

        EditorGUILayout.PropertyField(this.script);
        EditorGUILayout.PropertyField(this.wireframe);

        this.serializedObject.ApplyModifiedProperties();
    }

    public static void ExportPackage()
    {
        AssetDatabase.ExportPackage(new string[] {
            "Assets/SteamVR",
            "Assets/Plugins/openvr_api.cs",
            "Assets/Plugins/openvr_api.bundle",
            "Assets/Plugins/x86/openvr_api.dll",
            "Assets/Plugins/x86/steam_api.dll",
            "Assets/Plugins/x86/libsteam_api.so",
            "Assets/Plugins/x86_64/openvr_api.dll",
            "Assets/Plugins/x86_64/steam_api.dll",
            "Assets/Plugins/x86_64/libsteam_api.so",
            "Assets/Plugins/x86_64/libopenvr_api.so",
        }, "steamvr.unitypackage", ExportPackageOptions.Recurse);
        EditorApplication.Exit(0);
    }
}