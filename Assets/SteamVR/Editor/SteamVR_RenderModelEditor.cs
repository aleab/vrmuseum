﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Custom inspector display for SteamVR_RenderModel
//
//=============================================================================

using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SteamVR_RenderModel)), CanEditMultipleObjects]
public class SteamVR_RenderModelEditor : Editor
{
    private SerializedProperty script, index, modelOverride, shader, verbose, createComponents, updateDynamically;

    private static string[] renderModelNames;
    private int renderModelIndex;

    private void OnEnable()
    {
        this.script = this.serializedObject.FindProperty("m_Script");
        this.index = this.serializedObject.FindProperty("index");
        this.modelOverride = this.serializedObject.FindProperty("modelOverride");
        this.shader = this.serializedObject.FindProperty("shader");
        this.verbose = this.serializedObject.FindProperty("verbose");
        this.createComponents = this.serializedObject.FindProperty("createComponents");
        this.updateDynamically = this.serializedObject.FindProperty("updateDynamically");

        // Load render model names if necessary.
        if (renderModelNames == null)
        {
            renderModelNames = LoadRenderModelNames();
        }

        // Update renderModelIndex based on current modelOverride value.
        if (this.modelOverride.stringValue != "")
        {
            for (int i = 0; i < renderModelNames.Length; i++)
            {
                if (this.modelOverride.stringValue == renderModelNames[i])
                {
                    this.renderModelIndex = i;
                    break;
                }
            }
        }
    }

    private static string[] LoadRenderModelNames()
    {
        var results = new List<string>
        {
            "None"
        };
        using (var holder = new SteamVR_RenderModel.RenderModelInterfaceHolder())
        {
            var renderModels = holder.instance;
            if (renderModels != null)
            {
                uint count = renderModels.GetRenderModelCount();
                for (uint i = 0; i < count; i++)
                {
                    var buffer = new StringBuilder();
                    var requiredSize = renderModels.GetRenderModelName(i, buffer, 0);
                    if (requiredSize == 0)
                        continue;

                    buffer.EnsureCapacity((int)requiredSize);
                    renderModels.GetRenderModelName(i, buffer, requiredSize);
                    results.Add(buffer.ToString());
                }
            }
        }

        return results.ToArray();
    }

    public override void OnInspectorGUI()
    {
        this.serializedObject.Update();

        EditorGUILayout.PropertyField(this.script);
        EditorGUILayout.PropertyField(this.index);
        //EditorGUILayout.PropertyField(modelOverride);

        GUILayout.BeginHorizontal();
        GUILayout.Label("Model Override");
        var selected = EditorGUILayout.Popup(this.renderModelIndex, renderModelNames);
        if (selected != this.renderModelIndex)
        {
            this.renderModelIndex = selected;
            this.modelOverride.stringValue = (selected > 0) ? renderModelNames[selected] : "";
        }
        GUILayout.EndHorizontal();

        EditorGUILayout.PropertyField(this.shader);
        EditorGUILayout.PropertyField(this.verbose);
        EditorGUILayout.PropertyField(this.createComponents);
        EditorGUILayout.PropertyField(this.updateDynamically);

        this.serializedObject.ApplyModifiedProperties();
    }
}