﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Helper to display various hmd stats via GUIText
//
//=============================================================================

using UnityEngine;
using Valve.VR;

public class SteamVR_Stats : MonoBehaviour
{
    public GUIText text;

    public Color fadeColor = Color.black;
    public float fadeDuration = 1.0f;

    private void Awake()
    {
        if (this.text == null)
        {
            this.text = GetComponent<GUIText>();
            this.text.enabled = false;
        }

        if (this.fadeDuration > 0)
        {
            SteamVR_Fade.Start(this.fadeColor, 0);
            SteamVR_Fade.Start(Color.clear, this.fadeDuration);
        }
    }

    private double lastUpdate = 0.0f;

    private void Update()
    {
        if (this.text != null)
        {
            if (Input.GetKeyDown(KeyCode.I))
            {
                this.text.enabled = !this.text.enabled;
            }

            if (this.text.enabled)
            {
                var compositor = OpenVR.Compositor;
                if (compositor != null)
                {
                    var timing = new Compositor_FrameTiming()
                    {
                        m_nSize = (uint)System.Runtime.InteropServices.Marshal.SizeOf(typeof(Compositor_FrameTiming))
                    };
                    compositor.GetFrameTiming(ref timing, 0);

                    var update = timing.m_flSystemTimeInSeconds;
                    if (update > this.lastUpdate)
                    {
                        var framerate = (this.lastUpdate > 0.0f) ? 1.0f / (update - this.lastUpdate) : 0.0f;
                        this.lastUpdate = update;
                        this.text.text = string.Format("framerate: {0:N0}\ndropped frames: {1}", framerate, (int)timing.m_nNumDroppedFrames);
                    }
                    else
                    {
                        this.lastUpdate = update;
                    }
                }
            }
        }
    }
}