﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Simple two bone ik solver.
//
//=============================================================================

using UnityEngine;

public class SteamVR_IK : MonoBehaviour
{
    public Transform target;
    public Transform start, joint, end;
    public Transform poleVector, upVector;

    public float blendPct = 1.0f;

    [HideInInspector]
    public Transform startXform, jointXform, endXform;

    private void LateUpdate()
    {
        const float epsilon = 0.001f;
        if (this.blendPct < epsilon)
            return;

        var preUp = this.upVector ? this.upVector.up : Vector3.Cross(this.end.position - this.start.position, this.joint.position - this.start.position).normalized;

        var targetPosition = this.target.position;
        var targetRotation = this.target.rotation;

        Vector3 forward, up, result = this.joint.position;
        Solve(this.start.position, targetPosition, this.poleVector.position,
            (this.joint.position - this.start.position).magnitude,
            (this.end.position - this.joint.position).magnitude,
            ref result, out forward, out up);

        if (up == Vector3.zero)
            return;

        var startPosition = this.start.position;
        var jointPosition = this.joint.position;
        var endPosition = this.end.position;

        var startRotationLocal = this.start.localRotation;
        var jointRotationLocal = this.joint.localRotation;
        var endRotationLocal = this.end.localRotation;

        var startParent = this.start.parent;
        var jointParent = this.joint.parent;
        var endParent = this.end.parent;

        var startScale = this.start.localScale;
        var jointScale = this.joint.localScale;
        var endScale = this.end.localScale;

        if (this.startXform == null)
        {
            this.startXform = new GameObject("startXform").transform;
            this.startXform.parent = this.transform;
        }

        this.startXform.position = startPosition;
        this.startXform.LookAt(this.joint, preUp);
        this.start.parent = this.startXform;

        if (this.jointXform == null)
        {
            this.jointXform = new GameObject("jointXform").transform;
            this.jointXform.parent = this.startXform;
        }

        this.jointXform.position = jointPosition;
        this.jointXform.LookAt(this.end, preUp);
        this.joint.parent = this.jointXform;

        if (this.endXform == null)
        {
            this.endXform = new GameObject("endXform").transform;
            this.endXform.parent = this.jointXform;
        }

        this.endXform.position = endPosition;
        this.end.parent = this.endXform;

        this.startXform.LookAt(result, up);
        this.jointXform.LookAt(targetPosition, up);
        this.endXform.rotation = targetRotation;

        this.start.parent = startParent;
        this.joint.parent = jointParent;
        this.end.parent = endParent;

        this.end.rotation = targetRotation; // optionally blend?

        // handle blending in/out
        if (this.blendPct < 1.0f)
        {
            this.start.localRotation = Quaternion.Slerp(startRotationLocal, this.start.localRotation, this.blendPct);
            this.joint.localRotation = Quaternion.Slerp(jointRotationLocal, this.joint.localRotation, this.blendPct);
            this.end.localRotation = Quaternion.Slerp(endRotationLocal, this.end.localRotation, this.blendPct);
        }

        // restore scale so it doesn't blow out
        this.start.localScale = startScale;
        this.joint.localScale = jointScale;
        this.end.localScale = endScale;
    }

    public static bool Solve(
        Vector3 start, // shoulder / hip
        Vector3 end, // desired hand / foot position
        Vector3 poleVector, // point to aim elbow / knee toward
        float jointDist, // distance from start to elbow / knee
        float targetDist, // distance from joint to hand / ankle
        ref Vector3 result, // original and output elbow / knee position
        out Vector3 forward, out Vector3 up) // plane formed by root, joint and target
    {
        var totalDist = jointDist + targetDist;
        var start2end = end - start;
        var poleVectorDir = (poleVector - start).normalized;
        var baseDist = start2end.magnitude;

        result = start;

        const float epsilon = 0.001f;
        if (baseDist < epsilon)
        {
            // move jointDist toward jointTarget
            result += poleVectorDir * jointDist;

            forward = Vector3.Cross(poleVectorDir, Vector3.up);
            up = Vector3.Cross(forward, poleVectorDir).normalized;
        }
        else
        {
            forward = start2end * (1.0f / baseDist);
            up = Vector3.Cross(forward, poleVectorDir).normalized;

            if (baseDist + epsilon < totalDist)
            {
                // calculate the area of the triangle to determine its height
                var p = (totalDist + baseDist) * 0.5f; // half perimeter
                if (p > jointDist + epsilon && p > targetDist + epsilon)
                {
                    var A = Mathf.Sqrt(p * (p - jointDist) * (p - targetDist) * (p - baseDist));
                    var height = 2.0f * A / baseDist; // distance of joint from line between root and target

                    var dist = Mathf.Sqrt((jointDist * jointDist) - (height * height));
                    var right = Vector3.Cross(up, forward); // no need to normalized - already orthonormal

                    result += (forward * dist) + (right * height);
                    return true; // in range
                }
                else
                {
                    // move jointDist toward jointTarget
                    result += poleVectorDir * jointDist;
                }
            }
            else
            {
                // move elboDist toward target
                result += forward * jointDist;
            }
        }

        return false; // edge cases
    }
}