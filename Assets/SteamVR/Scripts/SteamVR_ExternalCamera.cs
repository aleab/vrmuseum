﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Used to render an external camera of vr player (split front/back).
//
//=============================================================================

using UnityEngine;
using UnityEngine.Rendering;
using Valve.VR;

public class SteamVR_ExternalCamera : MonoBehaviour
{
    public struct Config
    {
        public float x, y, z;
        public float rx, ry, rz;
        public float fov;
        public float near, far;
        public float sceneResolutionScale;
        public float frameSkip;
        public float nearOffset, farOffset;
        public float hmdOffset;
        public bool disableStandardAssets;
    }

    public Config config;
    public string configPath;

    public void ReadConfig()
    {
        try
        {
            var mCam = new HmdMatrix34_t();
            var readCamMatrix = false;

            object c = this.config; // box
            var lines = System.IO.File.ReadAllLines(this.configPath);
            foreach (var line in lines)
            {
                var split = line.Split('=');
                if (split.Length == 2)
                {
                    var key = split[0];
                    if (key == "m")
                    {
                        var values = split[1].Split(',');
                        if (values.Length == 12)
                        {
                            mCam.m0 = float.Parse(values[0]);
                            mCam.m1 = float.Parse(values[1]);
                            mCam.m2 = float.Parse(values[2]);
                            mCam.m3 = float.Parse(values[3]);
                            mCam.m4 = float.Parse(values[4]);
                            mCam.m5 = float.Parse(values[5]);
                            mCam.m6 = float.Parse(values[6]);
                            mCam.m7 = float.Parse(values[7]);
                            mCam.m8 = float.Parse(values[8]);
                            mCam.m9 = float.Parse(values[9]);
                            mCam.m10 = float.Parse(values[10]);
                            mCam.m11 = float.Parse(values[11]);
                            readCamMatrix = true;
                        }
                    }
                    else if (key == "disableStandardAssets")
                    {
                        var field = c.GetType().GetField(key);
                        if (field != null)
                            field.SetValue(c, bool.Parse(split[1]));
                    }
                    else
                    {
                        var field = c.GetType().GetField(key);
                        if (field != null)
                            field.SetValue(c, float.Parse(split[1]));
                    }
                }
            }
            this.config = (Config)c; //unbox

            // Convert calibrated camera matrix settings.
            if (readCamMatrix)
            {
                var t = new SteamVR_Utils.RigidTransform(mCam);
                this.config.x = t.pos.x;
                this.config.y = t.pos.y;
                this.config.z = t.pos.z;
                var angles = t.rot.eulerAngles;
                this.config.rx = angles.x;
                this.config.ry = angles.y;
                this.config.rz = angles.z;
            }
        }
        catch { }
    }

    private Camera cam;
    private Transform target;
    private GameObject clipQuad;
    private Material clipMaterial;

    public void AttachToCamera(SteamVR_Camera vrcam)
    {
        if (this.target == vrcam.head)
            return;

        this.target = vrcam.head;

        var root = this.transform.parent;
        var origin = vrcam.head.parent;
        root.parent = origin;
        root.localPosition = Vector3.zero;
        root.localRotation = Quaternion.identity;
        root.localScale = Vector3.one;

        // Make a copy of the eye camera to pick up any camera fx.
        vrcam.enabled = false;
        var go = Instantiate(vrcam.gameObject);
        vrcam.enabled = true;
        go.name = "camera";

        DestroyImmediate(go.GetComponent<SteamVR_Camera>());

        this.cam = go.GetComponent<Camera>();
        this.cam.fieldOfView = this.config.fov;
        this.cam.useOcclusionCulling = false;
        this.cam.enabled = false; // manually rendered

        this.colorMat = new Material(Shader.Find("Custom/SteamVR_ColorOut"));
        this.alphaMat = new Material(Shader.Find("Custom/SteamVR_AlphaOut"));
        this.clipMaterial = new Material(Shader.Find("Custom/SteamVR_ClearAll"));

        var offset = go.transform;
        offset.parent = this.transform;
        offset.localPosition = new Vector3(this.config.x, this.config.y, this.config.z);
        offset.localRotation = Quaternion.Euler(this.config.rx, this.config.ry, this.config.rz);
        offset.localScale = Vector3.one;

        // Strip children of cloned object (AudioListener in particular).
        while (offset.childCount > 0)
            DestroyImmediate(offset.GetChild(0).gameObject);

        // Setup clipping quad (using camera clip causes problems with shadows).
        this.clipQuad = GameObject.CreatePrimitive(PrimitiveType.Quad);
        this.clipQuad.name = "ClipQuad";
        DestroyImmediate(this.clipQuad.GetComponent<MeshCollider>());

        var clipRenderer = this.clipQuad.GetComponent<MeshRenderer>();
        clipRenderer.material = this.clipMaterial;
        clipRenderer.shadowCastingMode = ShadowCastingMode.Off;
        clipRenderer.receiveShadows = false;
        clipRenderer.lightProbeUsage = LightProbeUsage.Off;
        clipRenderer.reflectionProbeUsage = ReflectionProbeUsage.Off;

        var clipTransform = this.clipQuad.transform;
        clipTransform.parent = offset;
        clipTransform.localScale = new Vector3(1000.0f, 1000.0f, 1.0f);
        clipTransform.localRotation = Quaternion.identity;

        this.clipQuad.SetActive(false);
    }

    public float GetTargetDistance()
    {
        if (this.target == null)
            return this.config.near + 0.01f;

        var offset = this.cam.transform;
        var forward = new Vector3(offset.forward.x, 0.0f, offset.forward.z).normalized;
        var targetPos = this.target.position + new Vector3(this.target.forward.x, 0.0f, this.target.forward.z).normalized * this.config.hmdOffset;

        var distance = -(new Plane(forward, targetPos)).GetDistanceToPoint(offset.position);
        return Mathf.Clamp(distance, this.config.near + 0.01f, this.config.far - 0.01f);
    }

    private Material colorMat, alphaMat;

    public void RenderNear()
    {
        var w = Screen.width / 2;
        var h = Screen.height / 2;

        if (this.cam.targetTexture == null || this.cam.targetTexture.width != w || this.cam.targetTexture.height != h)
        {
            this.cam.targetTexture = new RenderTexture(w, h, 24, RenderTextureFormat.ARGB32)
            {
                antiAliasing = QualitySettings.antiAliasing == 0 ? 1 : QualitySettings.antiAliasing
            };
        }

        this.cam.nearClipPlane = this.config.near;
        this.cam.farClipPlane = this.config.far;

        var clearFlags = this.cam.clearFlags;
        var backgroundColor = this.cam.backgroundColor;

        this.cam.clearFlags = CameraClearFlags.Color;
        this.cam.backgroundColor = Color.clear;

        float dist = Mathf.Clamp(GetTargetDistance() + this.config.nearOffset, this.config.near, this.config.far);
        var clipParent = this.clipQuad.transform.parent;
        this.clipQuad.transform.position = clipParent.position + clipParent.forward * dist;

        MonoBehaviour[] behaviours = null;
        bool[] wasEnabled = null;
        if (this.config.disableStandardAssets)
        {
            behaviours = this.cam.gameObject.GetComponents<MonoBehaviour>();
            wasEnabled = new bool[behaviours.Length];
            for (int i = 0; i < behaviours.Length; i++)
            {
                var behaviour = behaviours[i];
                if (behaviour.enabled && behaviour.GetType().ToString().StartsWith("UnityStandardAssets."))
                {
                    behaviour.enabled = false;
                    wasEnabled[i] = true;
                }
            }
        }

        this.clipQuad.SetActive(true);
        this.cam.Render();
        this.clipQuad.SetActive(false);

        if (behaviours != null)
        {
            for (int i = 0; i < behaviours.Length; i++)
            {
                if (wasEnabled[i])
                {
                    behaviours[i].enabled = true;
                }
            }
        }

        this.cam.clearFlags = clearFlags;
        this.cam.backgroundColor = backgroundColor;

        Graphics.DrawTexture(new Rect(0, 0, w, h), this.cam.targetTexture, this.colorMat);
        Graphics.DrawTexture(new Rect(w, 0, w, h), this.cam.targetTexture, this.alphaMat);
    }

    public void RenderFar()
    {
        this.cam.nearClipPlane = this.config.near;
        this.cam.farClipPlane = this.config.far;
        this.cam.Render();

        var w = Screen.width / 2;
        var h = Screen.height / 2;
        Graphics.DrawTexture(new Rect(0, h, w, h), this.cam.targetTexture, this.colorMat);
    }

    private void OnGUI()
    {
        // Necessary for Graphics.DrawTexture to work even though we don't do anything here.
    }

    private Camera[] cameras;
    private Rect[] cameraRects;
    private float sceneResolutionScale;

    private void OnEnable()
    {
        // Move game view cameras to lower-right quadrant.
        this.cameras = FindObjectsOfType<Camera>() as Camera[];
        if (this.cameras != null)
        {
            var numCameras = this.cameras.Length;
            this.cameraRects = new Rect[numCameras];
            for (int i = 0; i < numCameras; i++)
            {
                var cam = this.cameras[i];
                this.cameraRects[i] = cam.rect;

                if (cam == this.cam)
                    continue;

                if (cam.targetTexture != null)
                    continue;

                if (cam.GetComponent<SteamVR_Camera>() != null)
                    continue;

                cam.rect = new Rect(0.5f, 0.0f, 0.5f, 0.5f);
            }
        }

        if (this.config.sceneResolutionScale > 0.0f)
        {
            this.sceneResolutionScale = SteamVR_Camera.sceneResolutionScale;
            SteamVR_Camera.sceneResolutionScale = this.config.sceneResolutionScale;
        }
    }

    private void OnDisable()
    {
        // Restore game view cameras.
        if (this.cameras != null)
        {
            var numCameras = this.cameras.Length;
            for (int i = 0; i < numCameras; i++)
            {
                var cam = this.cameras[i];
                if (cam != null)
                    cam.rect = this.cameraRects[i];
            }
            this.cameras = null;
            this.cameraRects = null;
        }

        if (this.config.sceneResolutionScale > 0.0f)
        {
            SteamVR_Camera.sceneResolutionScale = this.sceneResolutionScale;
        }
    }
}