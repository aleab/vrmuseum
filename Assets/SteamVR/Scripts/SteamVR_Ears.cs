﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Handles aligning audio listener when using speakers.
//
//=============================================================================

using UnityEngine;
using Valve.VR;

[RequireComponent(typeof(AudioListener))]
public class SteamVR_Ears : MonoBehaviour
{
    public SteamVR_Camera vrcam;

    private bool usingSpeakers;
    private Quaternion offset;

    private void OnNewPosesApplied()
    {
        var origin = this.vrcam.origin;
        var baseRotation = origin != null ? origin.rotation : Quaternion.identity;
        this.transform.rotation = baseRotation * this.offset;
    }

    private void OnEnable()
    {
        this.usingSpeakers = false;

        var settings = OpenVR.Settings;
        if (settings != null)
        {
            var error = EVRSettingsError.None;
            if (settings.GetBool(OpenVR.k_pch_SteamVR_Section, OpenVR.k_pch_SteamVR_UsingSpeakers_Bool, ref error))
            {
                this.usingSpeakers = true;

                var yawOffset = settings.GetFloat(OpenVR.k_pch_SteamVR_Section, OpenVR.k_pch_SteamVR_SpeakersForwardYawOffsetDegrees_Float, ref error);
                this.offset = Quaternion.Euler(0.0f, yawOffset, 0.0f);
            }
        }

        if (this.usingSpeakers)
            SteamVR_Events.NewPosesApplied.Listen(this.OnNewPosesApplied);
    }

    private void OnDisable()
    {
        if (this.usingSpeakers)
            SteamVR_Events.NewPosesApplied.Remove(this.OnNewPosesApplied);
    }
}