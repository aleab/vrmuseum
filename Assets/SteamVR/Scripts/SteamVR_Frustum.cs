﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Generates a mesh based on field of view.
//
//=============================================================================

using UnityEngine;
using Valve.VR;

[ExecuteInEditMode, RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
public class SteamVR_Frustum : MonoBehaviour
{
    public SteamVR_TrackedObject.EIndex index;

    public float fovLeft = 45, fovRight = 45, fovTop = 45, fovBottom = 45, nearZ = 0.5f, farZ = 2.5f;

    public void UpdateModel()
    {
        this.fovLeft = Mathf.Clamp(this.fovLeft, 1, 89);
        this.fovRight = Mathf.Clamp(this.fovRight, 1, 89);
        this.fovTop = Mathf.Clamp(this.fovTop, 1, 89);
        this.fovBottom = Mathf.Clamp(this.fovBottom, 1, 89);
        this.farZ = Mathf.Max(this.farZ, this.nearZ + 0.01f);
        this.nearZ = Mathf.Clamp(this.nearZ, 0.01f, this.farZ - 0.01f);

        var lsin = Mathf.Sin(-this.fovLeft * Mathf.Deg2Rad);
        var rsin = Mathf.Sin(this.fovRight * Mathf.Deg2Rad);
        var tsin = Mathf.Sin(this.fovTop * Mathf.Deg2Rad);
        var bsin = Mathf.Sin(-this.fovBottom * Mathf.Deg2Rad);

        var lcos = Mathf.Cos(-this.fovLeft * Mathf.Deg2Rad);
        var rcos = Mathf.Cos(this.fovRight * Mathf.Deg2Rad);
        var tcos = Mathf.Cos(this.fovTop * Mathf.Deg2Rad);
        var bcos = Mathf.Cos(-this.fovBottom * Mathf.Deg2Rad);

        var corners = new Vector3[] {
            new Vector3(lsin * this.nearZ / lcos, tsin * this.nearZ / tcos, this.nearZ), //tln
			new Vector3(rsin * this.nearZ / rcos, tsin * this.nearZ / tcos, this.nearZ), //trn
			new Vector3(rsin * this.nearZ / rcos, bsin * this.nearZ / bcos, this.nearZ), //brn
			new Vector3(lsin * this.nearZ / lcos, bsin * this.nearZ / bcos, this.nearZ), //bln
			new Vector3(lsin * this.farZ  / lcos, tsin * this.farZ  / tcos, this.farZ ), //tlf
			new Vector3(rsin * this.farZ  / rcos, tsin * this.farZ  / tcos, this.farZ ), //trf
			new Vector3(rsin * this.farZ  / rcos, bsin * this.farZ  / bcos, this.farZ ), //brf
			new Vector3(lsin * this.farZ  / lcos, bsin * this.farZ  / bcos, this.farZ ), //blf
		};

        var triangles = new int[] {
		//	0, 1, 2, 0, 2, 3, // near
		//	0, 2, 1, 0, 3, 2, // near
		//	4, 5, 6, 4, 6, 7, // far
		//	4, 6, 5, 4, 7, 6, // far
			0, 4, 7, 0, 7, 3, // left
			0, 7, 4, 0, 3, 7, // left
			1, 5, 6, 1, 6, 2, // right
			1, 6, 5, 1, 2, 6, // right
			0, 4, 5, 0, 5, 1, // top
			0, 5, 4, 0, 1, 5, // top
			2, 3, 7, 2, 7, 6, // bottom
			2, 7, 3, 2, 6, 7, // bottom
		};

        int j = 0;
        var vertices = new Vector3[triangles.Length];
        var normals = new Vector3[triangles.Length];
        for (int i = 0; i < triangles.Length / 3; i++)
        {
            var a = corners[triangles[i * 3 + 0]];
            var b = corners[triangles[i * 3 + 1]];
            var c = corners[triangles[i * 3 + 2]];
            var n = Vector3.Cross(b - a, c - a).normalized;
            normals[i * 3 + 0] = n;
            normals[i * 3 + 1] = n;
            normals[i * 3 + 2] = n;
            vertices[i * 3 + 0] = a;
            vertices[i * 3 + 1] = b;
            vertices[i * 3 + 2] = c;
            triangles[i * 3 + 0] = j++;
            triangles[i * 3 + 1] = j++;
            triangles[i * 3 + 2] = j++;
        }

        var mesh = new Mesh()
        {
            vertices = vertices,
            normals = normals,
            triangles = triangles
        };
        GetComponent<MeshFilter>().mesh = mesh;
    }

    private void OnDeviceConnected(int i, bool connected)
    {
        if (i != (int)this.index)
            return;

        GetComponent<MeshFilter>().mesh = null;

        if (connected)
        {
            var system = OpenVR.System;
            if (system != null && system.GetTrackedDeviceClass((uint)i) == ETrackedDeviceClass.TrackingReference)
            {
                var error = ETrackedPropertyError.TrackedProp_Success;
                var result = system.GetFloatTrackedDeviceProperty((uint)i, ETrackedDeviceProperty.Prop_FieldOfViewLeftDegrees_Float, ref error);
                if (error == ETrackedPropertyError.TrackedProp_Success)
                    this.fovLeft = result;

                result = system.GetFloatTrackedDeviceProperty((uint)i, ETrackedDeviceProperty.Prop_FieldOfViewRightDegrees_Float, ref error);
                if (error == ETrackedPropertyError.TrackedProp_Success)
                    this.fovRight = result;

                result = system.GetFloatTrackedDeviceProperty((uint)i, ETrackedDeviceProperty.Prop_FieldOfViewTopDegrees_Float, ref error);
                if (error == ETrackedPropertyError.TrackedProp_Success)
                    this.fovTop = result;

                result = system.GetFloatTrackedDeviceProperty((uint)i, ETrackedDeviceProperty.Prop_FieldOfViewBottomDegrees_Float, ref error);
                if (error == ETrackedPropertyError.TrackedProp_Success)
                    this.fovBottom = result;

                result = system.GetFloatTrackedDeviceProperty((uint)i, ETrackedDeviceProperty.Prop_TrackingRangeMinimumMeters_Float, ref error);
                if (error == ETrackedPropertyError.TrackedProp_Success)
                    this.nearZ = result;

                result = system.GetFloatTrackedDeviceProperty((uint)i, ETrackedDeviceProperty.Prop_TrackingRangeMaximumMeters_Float, ref error);
                if (error == ETrackedPropertyError.TrackedProp_Success)
                    this.farZ = result;

                UpdateModel();
            }
        }
    }

    private void OnEnable()
    {
        GetComponent<MeshFilter>().mesh = null;
        SteamVR_Events.DeviceConnected.Listen(this.OnDeviceConnected);
    }

    private void OnDisable()
    {
        SteamVR_Events.DeviceConnected.Remove(this.OnDeviceConnected);
        GetComponent<MeshFilter>().mesh = null;
    }

#if UNITY_EDITOR

    private void Update()
    {
        if (!Application.isPlaying)
            UpdateModel();
    }

#endif
}