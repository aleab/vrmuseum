﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Adds SteamVR render support to existing camera objects
//
//=============================================================================

using System.Collections;
using System.Reflection;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class SteamVR_Camera : MonoBehaviour
{
    [SerializeField]
    private Transform _head;

    public Transform head { get { return this._head; } }
    public Transform offset { get { return this._head; } } // legacy
    public Transform origin { get { return this._head.parent; } }

    public new Camera camera { get; private set; }

    [SerializeField]
    private Transform _ears;

    public Transform ears { get { return this._ears; } }

    public Ray GetRay()
    {
        return new Ray(this._head.position, this._head.forward);
    }

    public bool wireframe = false;

    static public float sceneResolutionScale
    {
        get { return UnityEngine.VR.VRSettings.renderScale; }
        set { UnityEngine.VR.VRSettings.renderScale = value; }
    }

    #region Enable / Disable

    private void OnDisable()
    {
        SteamVR_Render.Remove(this);
    }

    private void OnEnable()
    {
        // Bail if no hmd is connected
        var vr = SteamVR.instance;
        if (vr == null)
        {
            if (this.head != null)
            {
                this.head.GetComponent<SteamVR_TrackedObject>().enabled = false;
            }

            this.enabled = false;
            return;
        }

        // Convert camera rig for native OpenVR integration.
        var t = this.transform;
        if (this.head != t)
        {
            Expand();

            t.parent = this.origin;

            while (this.head.childCount > 0)
                this.head.GetChild(0).parent = t;

            // Keep the head around, but parent to the camera now since it moves with the hmd
            // but existing content may still have references to this object.
            this.head.parent = t;
            this.head.localPosition = Vector3.zero;
            this.head.localRotation = Quaternion.identity;
            this.head.localScale = Vector3.one;
            this.head.gameObject.SetActive(false);

            this._head = t;
        }

        if (this.ears == null)
        {
            var e = this.transform.GetComponentInChildren<SteamVR_Ears>();
            if (e != null)
                this._ears = e.transform;
        }

        if (this.ears != null)
            this.ears.GetComponent<SteamVR_Ears>().vrcam = this;

        SteamVR_Render.Add(this);
    }

    #endregion Enable / Disable

    #region Functionality to ensure SteamVR_Camera component is always the last component on an object

    private void Awake()
    {
        this.camera = GetComponent<Camera>(); // cached to avoid runtime lookup
        ForceLast();
    }

    private static Hashtable values;

    public void ForceLast()
    {
        if (values != null)
        {
            // Restore values on new instance
            foreach (DictionaryEntry entry in values)
            {
                var f = entry.Key as FieldInfo;
                f.SetValue(this, entry.Value);
            }
            values = null;
        }
        else
        {
            // Make sure it's the last component
            var components = GetComponents<Component>();

            // But first make sure there aren't any other SteamVR_Cameras on this object.
            for (int i = 0; i < components.Length; i++)
            {
                var c = components[i] as SteamVR_Camera;
                if (c != null && c != this)
                {
                    DestroyImmediate(c);
                }
            }

            components = GetComponents<Component>();

            if (this != components[components.Length - 1])
            {
                // Store off values to be restored on new instance
                values = new Hashtable();
                var fields = GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                foreach (var f in fields)
                    if (f.IsPublic || f.IsDefined(typeof(SerializeField), true))
                        values[f] = f.GetValue(this);

                var go = this.gameObject;
                DestroyImmediate(this);
                go.AddComponent<SteamVR_Camera>().ForceLast();
            }
        }
    }

    #endregion Functionality to ensure SteamVR_Camera component is always the last component on an object

    #region Expand / Collapse object hierarchy

#if UNITY_EDITOR
    public bool isExpanded { get { return this.head != null && this.transform.parent == this.head; } }
#endif
    private const string eyeSuffix = " (eye)";
    private const string earsSuffix = " (ears)";
    private const string headSuffix = " (head)";
    private const string originSuffix = " (origin)";
    public string baseName { get { return this.name.EndsWith(eyeSuffix) ? this.name.Substring(0, this.name.Length - eyeSuffix.Length) : this.name; } }

    // Object hierarchy creation to make it easy to parent other objects appropriately,
    // otherwise this gets called on demand at runtime. Remaining initialization is
    // performed at startup, once the hmd has been identified.
    public void Expand()
    {
        var _origin = this.transform.parent;
        if (_origin == null)
        {
            _origin = new GameObject(this.name + originSuffix).transform;
            _origin.localPosition = this.transform.localPosition;
            _origin.localRotation = this.transform.localRotation;
            _origin.localScale = this.transform.localScale;
        }

        if (this.head == null)
        {
            this._head = new GameObject(this.name + headSuffix, typeof(SteamVR_TrackedObject)).transform;
            this.head.parent = _origin;
            this.head.position = this.transform.position;
            this.head.rotation = this.transform.rotation;
            this.head.localScale = Vector3.one;
            this.head.tag = this.tag;
        }

        if (this.transform.parent != this.head)
        {
            this.transform.parent = this.head;
            this.transform.localPosition = Vector3.zero;
            this.transform.localRotation = Quaternion.identity;
            this.transform.localScale = Vector3.one;

            while (this.transform.childCount > 0)
                this.transform.GetChild(0).parent = this.head;

            var guiLayer = GetComponent<GUILayer>();
            if (guiLayer != null)
            {
                DestroyImmediate(guiLayer);
                this.head.gameObject.AddComponent<GUILayer>();
            }

            var audioListener = GetComponent<AudioListener>();
            if (audioListener != null)
            {
                DestroyImmediate(audioListener);
                this._ears = new GameObject(this.name + earsSuffix, typeof(SteamVR_Ears)).transform;
                this.ears.parent = this._head;
                this.ears.localPosition = Vector3.zero;
                this.ears.localRotation = Quaternion.identity;
                this.ears.localScale = Vector3.one;
            }
        }

        if (!this.name.EndsWith(eyeSuffix))
            this.name += eyeSuffix;
    }

    public void Collapse()
    {
        this.transform.parent = null;

        // Move children and components from head back to camera.
        while (this.head.childCount > 0)
            this.head.GetChild(0).parent = this.transform;

        var guiLayer = this.head.GetComponent<GUILayer>();
        if (guiLayer != null)
        {
            DestroyImmediate(guiLayer);
            this.gameObject.AddComponent<GUILayer>();
        }

        if (this.ears != null)
        {
            while (this.ears.childCount > 0)
                this.ears.GetChild(0).parent = this.transform;

            DestroyImmediate(this.ears.gameObject);
            this._ears = null;

            this.gameObject.AddComponent(typeof(AudioListener));
        }

        if (this.origin != null)
        {
            // If we created the origin originally, destroy it now.
            if (this.origin.name.EndsWith(originSuffix))
            {
                // Reparent any children so we don't accidentally delete them.
                var _origin = this.origin;
                while (_origin.childCount > 0)
                    _origin.GetChild(0).parent = _origin.parent;

                DestroyImmediate(_origin.gameObject);
            }
            else
            {
                this.transform.parent = this.origin;
            }
        }

        DestroyImmediate(this.head.gameObject);
        this._head = null;

        if (this.name.EndsWith(eyeSuffix))
            this.name = this.name.Substring(0, this.name.Length - eyeSuffix.Length);
    }

    #endregion Expand / Collapse object hierarchy
}