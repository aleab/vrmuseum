﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: For controlling in-game objects with tracked devices.
//
//=============================================================================

using UnityEngine;
using Valve.VR;

public class SteamVR_TrackedObject : MonoBehaviour
{
    public enum EIndex
    {
        None = -1,
        Hmd = (int)OpenVR.k_unTrackedDeviceIndex_Hmd,
        Device1,
        Device2,
        Device3,
        Device4,
        Device5,
        Device6,
        Device7,
        Device8,
        Device9,
        Device10,
        Device11,
        Device12,
        Device13,
        Device14,
        Device15
    }

    public EIndex index;
    public Transform origin; // if not set, relative to parent
    public bool isValid = false;

    private void OnNewPoses(TrackedDevicePose_t[] poses)
    {
        if (this.index == EIndex.None)
            return;

        var i = (int)this.index;

        this.isValid = false;
        if (poses.Length <= i)
            return;

        if (!poses[i].bDeviceIsConnected)
            return;

        if (!poses[i].bPoseIsValid)
            return;

        this.isValid = true;

        var pose = new SteamVR_Utils.RigidTransform(poses[i].mDeviceToAbsoluteTracking);

        if (this.origin != null)
        {
            this.transform.position = this.origin.transform.TransformPoint(pose.pos);
            this.transform.rotation = this.origin.rotation * pose.rot;
        }
        else
        {
            this.transform.localPosition = pose.pos;
            this.transform.localRotation = pose.rot;
        }
    }

    private SteamVR_Events.Action newPosesAction;

    private void Awake()
    {
        this.newPosesAction = SteamVR_Events.NewPosesAction(this.OnNewPoses);
    }

    private void OnEnable()
    {
        var render = SteamVR_Render.instance;
        if (render == null)
        {
            this.enabled = false;
            return;
        }

        this.newPosesAction.enabled = true;
    }

    private void OnDisable()
    {
        this.newPosesAction.enabled = false;
        this.isValid = false;
    }

    public void SetDeviceIndex(int index)
    {
        if (System.Enum.IsDefined(typeof(EIndex), index))
            this.index = (EIndex)index;
    }
}