﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Example menu using OnGUI with SteamVR_Camera's overlay support
//
//=============================================================================

using UnityEngine;
using Valve.VR;

public class SteamVR_Menu : MonoBehaviour
{
    public Texture cursor, background, logo;
    public float logoHeight, menuOffset;

    public Vector2 scaleLimits = new Vector2(0.1f, 5.0f);
    public float scaleRate = 0.5f;

    private SteamVR_Overlay overlay;
    private Camera overlayCam;
    private Vector4 uvOffset;
    private float distance;

    public RenderTexture texture { get { return this.overlay ? this.overlay.texture as RenderTexture : null; } }
    public float scale { get; private set; }

    private string scaleLimitX, scaleLimitY, scaleRateText;

    private CursorLockMode savedCursorLockState;
    private bool savedCursorVisible;

    private void Awake()
    {
        this.scaleLimitX = string.Format("{0:N1}", this.scaleLimits.x);
        this.scaleLimitY = string.Format("{0:N1}", this.scaleLimits.y);
        this.scaleRateText = string.Format("{0:N1}", this.scaleRate);

        var overlay = SteamVR_Overlay.instance;
        if (overlay != null)
        {
            this.uvOffset = overlay.uvOffset;
            this.distance = overlay.distance;
        }
    }

    private void OnGUI()
    {
        if (this.overlay == null)
            return;

        var texture = this.overlay.texture as RenderTexture;

        var prevActive = RenderTexture.active;
        RenderTexture.active = texture;

        if (Event.current.type == EventType.Repaint)
            GL.Clear(false, true, Color.clear);

        var area = new Rect(0, 0, texture.width, texture.height);

        // Account for screen smaller than texture (since mouse position gets clamped)
        if (Screen.width < texture.width)
        {
            area.width = Screen.width;
            this.overlay.uvOffset.x = -(float)(texture.width - Screen.width) / (2 * texture.width);
        }
        if (Screen.height < texture.height)
        {
            area.height = Screen.height;
            this.overlay.uvOffset.y = (float)(texture.height - Screen.height) / (2 * texture.height);
        }

        GUILayout.BeginArea(area);

        if (this.background != null)
        {
            GUI.DrawTexture(new Rect(
                (area.width - this.background.width) / 2,
                (area.height - this.background.height) / 2,
                this.background.width, this.background.height), this.background);
        }

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.BeginVertical();

        if (this.logo != null)
        {
            GUILayout.Space(area.height / 2 - this.logoHeight);
            GUILayout.Box(this.logo);
        }

        GUILayout.Space(this.menuOffset);

        bool bHideMenu = GUILayout.Button("[Esc] - Close menu");

        GUILayout.BeginHorizontal();
        GUILayout.Label(string.Format("Scale: {0:N4}", this.scale));
        {
            var result = GUILayout.HorizontalSlider(this.scale, this.scaleLimits.x, this.scaleLimits.y);
            if (result != this.scale)
            {
                SetScale(result);
            }
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label(string.Format("Scale limits:"));
        {
            var result = GUILayout.TextField(this.scaleLimitX);
            if (result != this.scaleLimitX)
            {
                if (float.TryParse(result, out this.scaleLimits.x))
                    this.scaleLimitX = result;
            }
        }
        {
            var result = GUILayout.TextField(this.scaleLimitY);
            if (result != this.scaleLimitY)
            {
                if (float.TryParse(result, out this.scaleLimits.y))
                    this.scaleLimitY = result;
            }
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label(string.Format("Scale rate:"));
        {
            var result = GUILayout.TextField(this.scaleRateText);
            if (result != this.scaleRateText)
            {
                if (float.TryParse(result, out this.scaleRate))
                    this.scaleRateText = result;
            }
        }
        GUILayout.EndHorizontal();

        if (SteamVR.active)
        {
            var vr = SteamVR.instance;

            GUILayout.BeginHorizontal();
            {
                var t = SteamVR_Camera.sceneResolutionScale;
                int w = (int)(vr.sceneWidth * t);
                int h = (int)(vr.sceneHeight * t);
                int pct = (int)(100.0f * t);
                GUILayout.Label(string.Format("Scene quality: {0}x{1} ({2}%)", w, h, pct));
                var result = Mathf.RoundToInt(GUILayout.HorizontalSlider(pct, 50, 200));
                if (result != pct)
                {
                    SteamVR_Camera.sceneResolutionScale = (float)result / 100.0f;
                }
            }
            GUILayout.EndHorizontal();
        }

        this.overlay.highquality = GUILayout.Toggle(this.overlay.highquality, "High quality");

        if (this.overlay.highquality)
        {
            this.overlay.curved = GUILayout.Toggle(this.overlay.curved, "Curved overlay");
            this.overlay.antialias = GUILayout.Toggle(this.overlay.antialias, "Overlay RGSS(2x2)");
        }
        else
        {
            this.overlay.curved = false;
            this.overlay.antialias = false;
        }

        var tracker = SteamVR_Render.Top();
        if (tracker != null)
        {
            tracker.wireframe = GUILayout.Toggle(tracker.wireframe, "Wireframe");

            var render = SteamVR_Render.instance;
            if (render.trackingSpace == ETrackingUniverseOrigin.TrackingUniverseSeated)
            {
                if (GUILayout.Button("Switch to Standing"))
                    render.trackingSpace = ETrackingUniverseOrigin.TrackingUniverseStanding;
                if (GUILayout.Button("Center View"))
                {
                    var system = OpenVR.System;
                    if (system != null)
                        system.ResetSeatedZeroPose();
                }
            }
            else
            {
                if (GUILayout.Button("Switch to Seated"))
                    render.trackingSpace = ETrackingUniverseOrigin.TrackingUniverseSeated;
            }
        }

#if !UNITY_EDITOR
		if (GUILayout.Button("Exit"))
			Application.Quit();
#endif
        GUILayout.Space(this.menuOffset);

        var env = System.Environment.GetEnvironmentVariable("VR_OVERRIDE");
        if (env != null)
        {
            GUILayout.Label("VR_OVERRIDE=" + env);
        }

        GUILayout.Label("Graphics device: " + SystemInfo.graphicsDeviceVersion);

        GUILayout.EndVertical();
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.EndArea();

        if (this.cursor != null)
        {
            float x = Input.mousePosition.x, y = Screen.height - Input.mousePosition.y;
            float w = this.cursor.width, h = this.cursor.height;
            GUI.DrawTexture(new Rect(x, y, w, h), this.cursor);
        }

        RenderTexture.active = prevActive;

        if (bHideMenu)
            HideMenu();
    }

    public void ShowMenu()
    {
        var overlay = SteamVR_Overlay.instance;
        if (overlay == null)
            return;

        var texture = overlay.texture as RenderTexture;
        if (texture == null)
        {
            Debug.LogError("Menu requires overlay texture to be a render texture.");
            return;
        }

        SaveCursorState();

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        this.overlay = overlay;
        this.uvOffset = overlay.uvOffset;
        this.distance = overlay.distance;

        // If an existing camera is rendering into the overlay texture, we need
        // to temporarily disable it to keep it from clearing the texture on us.
        var cameras = Object.FindObjectsOfType(typeof(Camera)) as Camera[];
        foreach (var cam in cameras)
        {
            if (cam.enabled && cam.targetTexture == texture)
            {
                this.overlayCam = cam;
                this.overlayCam.enabled = false;
                break;
            }
        }

        var tracker = SteamVR_Render.Top();
        if (tracker != null)
            this.scale = tracker.origin.localScale.x;
    }

    public void HideMenu()
    {
        RestoreCursorState();

        if (this.overlayCam != null)
            this.overlayCam.enabled = true;

        if (this.overlay != null)
        {
            this.overlay.uvOffset = this.uvOffset;
            this.overlay.distance = this.distance;
            this.overlay = null;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Joystick1Button7))
        {
            if (this.overlay == null)
            {
                ShowMenu();
            }
            else
            {
                HideMenu();
            }
        }
        else if (Input.GetKeyDown(KeyCode.Home))
        {
            SetScale(1.0f);
        }
        else if (Input.GetKey(KeyCode.PageUp))
        {
            SetScale(Mathf.Clamp(this.scale + this.scaleRate * Time.deltaTime, this.scaleLimits.x, this.scaleLimits.y));
        }
        else if (Input.GetKey(KeyCode.PageDown))
        {
            SetScale(Mathf.Clamp(this.scale - this.scaleRate * Time.deltaTime, this.scaleLimits.x, this.scaleLimits.y));
        }
    }

    private void SetScale(float scale)
    {
        this.scale = scale;

        var tracker = SteamVR_Render.Top();
        if (tracker != null)
            tracker.origin.localScale = new Vector3(scale, scale, scale);
    }

    private void SaveCursorState()
    {
        this.savedCursorVisible = Cursor.visible;
        this.savedCursorLockState = Cursor.lockState;
    }

    private void RestoreCursorState()
    {
        Cursor.visible = this.savedCursorVisible;
        Cursor.lockState = this.savedCursorLockState;
    }
}