﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Enables/disables objects based on connectivity and assigned roles.
//
//=============================================================================

using UnityEngine;
using Valve.VR;

public class SteamVR_ControllerManager : MonoBehaviour
{
    public GameObject left, right;
    public GameObject[] objects; // populate with objects you want to assign to additional controllers

    public bool assignAllBeforeIdentified; // set to true if you want objects arbitrarily assigned to controllers before their role (left vs right) is identified

    private uint[] indices; // assigned
    private bool[] connected = new bool[OpenVR.k_unMaxTrackedDeviceCount]; // controllers only

    // cached roles - may or may not be connected
    private uint leftIndex = OpenVR.k_unTrackedDeviceIndexInvalid;

    private uint rightIndex = OpenVR.k_unTrackedDeviceIndexInvalid;

    // This needs to be called if you update left, right or objects at runtime (e.g. when dyanmically spawned).
    public void UpdateTargets()
    {
        // Add left and right entries to the head of the list so we only have to operate on the list itself.
        var additional = (this.objects != null) ? this.objects.Length : 0;
        var objects = new GameObject[2 + additional];
        this.indices = new uint[2 + additional];
        objects[0] = this.right;
        this.indices[0] = OpenVR.k_unTrackedDeviceIndexInvalid;
        objects[1] = this.left;
        this.indices[1] = OpenVR.k_unTrackedDeviceIndexInvalid;
        for (int i = 0; i < additional; i++)
        {
            objects[2 + i] = this.objects[i];
            this.indices[2 + i] = OpenVR.k_unTrackedDeviceIndexInvalid;
        }
        this.objects = objects;
    }

    private SteamVR_Events.Action inputFocusAction, deviceConnectedAction, trackedDeviceRoleChangedAction;

    private void Awake()
    {
        UpdateTargets();
        this.inputFocusAction = SteamVR_Events.InputFocusAction(this.OnInputFocus);
        this.deviceConnectedAction = SteamVR_Events.DeviceConnectedAction(this.OnDeviceConnected);
        this.trackedDeviceRoleChangedAction = SteamVR_Events.SystemAction(EVREventType.VREvent_TrackedDeviceRoleChanged, this.OnTrackedDeviceRoleChanged);
    }

    private void OnEnable()
    {
        for (int i = 0; i < this.objects.Length; i++)
        {
            var obj = this.objects[i];
            if (obj != null)
                obj.SetActive(false);
        }

        Refresh();

        for (int i = 0; i < SteamVR.connected.Length; i++)
            if (SteamVR.connected[i])
                OnDeviceConnected(i, true);

        this.inputFocusAction.enabled = true;
        this.deviceConnectedAction.enabled = true;
        this.trackedDeviceRoleChangedAction.enabled = true;
    }

    private void OnDisable()
    {
        this.inputFocusAction.enabled = false;
        this.deviceConnectedAction.enabled = false;
        this.trackedDeviceRoleChangedAction.enabled = false;
    }

    private static string[] labels = { "left", "right" };

    // Hide controllers when the dashboard is up.
    private void OnInputFocus(bool hasFocus)
    {
        if (hasFocus)
        {
            for (int i = 0; i < this.objects.Length; i++)
            {
                var obj = this.objects[i];
                if (obj != null)
                {
                    var label = (i < 2) ? labels[i] : (i - 1).ToString();
                    ShowObject(obj.transform, "hidden (" + label + ")");
                }
            }
        }
        else
        {
            for (int i = 0; i < this.objects.Length; i++)
            {
                var obj = this.objects[i];
                if (obj != null)
                {
                    var label = (i < 2) ? labels[i] : (i - 1).ToString();
                    HideObject(obj.transform, "hidden (" + label + ")");
                }
            }
        }
    }

    // Reparents to a new object and deactivates that object (this allows
    // us to call SetActive in OnDeviceConnected independently.
    private void HideObject(Transform t, string name)
    {
        var hidden = new GameObject(name).transform;
        hidden.parent = t.parent;
        t.parent = hidden;
        hidden.gameObject.SetActive(false);
    }

    private void ShowObject(Transform t, string name)
    {
        var hidden = t.parent;
        if (hidden.gameObject.name != name)
            return;
        t.parent = hidden.parent;
        Destroy(hidden.gameObject);
    }

    private void SetTrackedDeviceIndex(int objectIndex, uint trackedDeviceIndex)
    {
        // First make sure no one else is already using this index.
        if (trackedDeviceIndex != OpenVR.k_unTrackedDeviceIndexInvalid)
        {
            for (int i = 0; i < this.objects.Length; i++)
            {
                if (i != objectIndex && this.indices[i] == trackedDeviceIndex)
                {
                    var obj = this.objects[i];
                    if (obj != null)
                        obj.SetActive(false);

                    this.indices[i] = OpenVR.k_unTrackedDeviceIndexInvalid;
                }
            }
        }

        // Only set when changed.
        if (trackedDeviceIndex != this.indices[objectIndex])
        {
            this.indices[objectIndex] = trackedDeviceIndex;

            var obj = this.objects[objectIndex];
            if (obj != null)
            {
                if (trackedDeviceIndex == OpenVR.k_unTrackedDeviceIndexInvalid)
                    obj.SetActive(false);
                else
                {
                    obj.SetActive(true);
                    obj.BroadcastMessage("SetDeviceIndex", (int)trackedDeviceIndex, SendMessageOptions.DontRequireReceiver);
                }
            }
        }
    }

    // Keep track of assigned roles.
    private void OnTrackedDeviceRoleChanged(VREvent_t vrEvent)
    {
        Refresh();
    }

    // Keep track of connected controller indices.
    private void OnDeviceConnected(int index, bool connected)
    {
        bool changed = this.connected[index];
        this.connected[index] = false;

        if (connected)
        {
            var system = OpenVR.System;
            if (system != null)
            {
                var deviceClass = system.GetTrackedDeviceClass((uint)index);
                if (deviceClass == ETrackedDeviceClass.Controller ||
                    deviceClass == ETrackedDeviceClass.GenericTracker)
                {
                    this.connected[index] = true;
                    changed = !changed; // if we clear and set the same index, nothing has changed
                }
            }
        }

        if (changed)
            Refresh();
    }

    public void Refresh()
    {
        int objectIndex = 0;

        var system = OpenVR.System;
        if (system != null)
        {
            this.leftIndex = system.GetTrackedDeviceIndexForControllerRole(ETrackedControllerRole.LeftHand);
            this.rightIndex = system.GetTrackedDeviceIndexForControllerRole(ETrackedControllerRole.RightHand);
        }

        // If neither role has been assigned yet, try hooking up at least the right controller.
        if (this.leftIndex == OpenVR.k_unTrackedDeviceIndexInvalid && this.rightIndex == OpenVR.k_unTrackedDeviceIndexInvalid)
        {
            for (uint deviceIndex = 0; deviceIndex < this.connected.Length; deviceIndex++)
            {
                if (objectIndex >= this.objects.Length)
                    break;

                if (!this.connected[deviceIndex])
                    continue;

                SetTrackedDeviceIndex(objectIndex++, deviceIndex);

                if (!this.assignAllBeforeIdentified)
                    break;
            }
        }
        else
        {
            SetTrackedDeviceIndex(objectIndex++, (this.rightIndex < this.connected.Length && this.connected[this.rightIndex]) ? this.rightIndex : OpenVR.k_unTrackedDeviceIndexInvalid);
            SetTrackedDeviceIndex(objectIndex++, (this.leftIndex < this.connected.Length && this.connected[this.leftIndex]) ? this.leftIndex : OpenVR.k_unTrackedDeviceIndexInvalid);

            // Assign out any additional controllers only after both left and right have been assigned.
            if (this.leftIndex != OpenVR.k_unTrackedDeviceIndexInvalid && this.rightIndex != OpenVR.k_unTrackedDeviceIndexInvalid)
            {
                for (uint deviceIndex = 0; deviceIndex < this.connected.Length; deviceIndex++)
                {
                    if (objectIndex >= this.objects.Length)
                        break;

                    if (!this.connected[deviceIndex])
                        continue;

                    if (deviceIndex != this.leftIndex && deviceIndex != this.rightIndex)
                    {
                        SetTrackedDeviceIndex(objectIndex++, deviceIndex);
                    }
                }
            }
        }

        // Reset the rest.
        while (objectIndex < this.objects.Length)
        {
            SetTrackedDeviceIndex(objectIndex++, OpenVR.k_unTrackedDeviceIndexInvalid);
        }
    }
}