﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
//
// Purpose: Provides access to video feed and poses of tracked cameras.
//
// Usage:
//			var source = SteamVR_TrackedCamera.Distorted();
//			var source = SteamVR_TrackedCamera.Undistorted();
// or
//			var undistorted = true; // or false
//			var source = SteamVR_TrackedCamera.Source(undistorted);
//
// - Distorted feeds are the decoded images from the camera.
// - Undistorted feeds correct for the camera lens distortion (a.k.a. fisheye)
//   to make straight lines straight.
//
// VideoStreamTexture objects must be symmetrically Acquired and Released to
// ensure the video stream is activated, and shutdown properly once there are
// no more consumers.  You only need to Acquire once when starting to use a
// stream, and Release when you are done using it (as opposed to every frame).
//
//=============================================================================

using UnityEngine;
using Valve.VR;

public class SteamVR_TrackedCamera
{
    public class VideoStreamTexture
    {
        public VideoStreamTexture(uint deviceIndex, bool undistorted)
        {
            this.undistorted = undistorted;
            this.videostream = Stream(deviceIndex);
        }

        public bool undistorted { get; private set; }
        public uint deviceIndex { get { return this.videostream.deviceIndex; } }
        public bool hasCamera { get { return this.videostream.hasCamera; } }
        public bool hasTracking { get { Update(); return this.header.standingTrackedDevicePose.bPoseIsValid; } }
        public uint frameId { get { Update(); return this.header.nFrameSequence; } }
        public VRTextureBounds_t frameBounds { get; private set; }
        public EVRTrackedCameraFrameType frameType { get { return this.undistorted ? EVRTrackedCameraFrameType.Undistorted : EVRTrackedCameraFrameType.Distorted; } }

        private Texture2D _texture;
        public Texture2D texture { get { Update(); return this._texture; } }

        public SteamVR_Utils.RigidTransform transform { get { Update(); return new SteamVR_Utils.RigidTransform(this.header.standingTrackedDevicePose.mDeviceToAbsoluteTracking); } }
        public Vector3 velocity { get { Update(); var pose = this.header.standingTrackedDevicePose; return new Vector3(pose.vVelocity.v0, pose.vVelocity.v1, -pose.vVelocity.v2); } }
        public Vector3 angularVelocity { get { Update(); var pose = this.header.standingTrackedDevicePose; return new Vector3(-pose.vAngularVelocity.v0, -pose.vAngularVelocity.v1, pose.vAngularVelocity.v2); } }

        public TrackedDevicePose_t GetPose()
        {
            Update(); return this.header.standingTrackedDevicePose;
        }

        public ulong Acquire()
        {
            return this.videostream.Acquire();
        }

        public ulong Release()
        {
            var result = this.videostream.Release();

            if (this.videostream.handle == 0)
            {
                Object.Destroy(this._texture);
                this._texture = null;
            }

            return result;
        }

        private int prevFrameCount = -1;

        private void Update()
        {
            if (Time.frameCount == this.prevFrameCount)
                return;

            this.prevFrameCount = Time.frameCount;

            if (this.videostream.handle == 0)
                return;

            var vr = SteamVR.instance;
            if (vr == null)
                return;

            var trackedCamera = OpenVR.TrackedCamera;
            if (trackedCamera == null)
                return;

            var nativeTex = System.IntPtr.Zero;
            var deviceTexture = this._texture ?? new Texture2D(2, 2);
            var headerSize = (uint)System.Runtime.InteropServices.Marshal.SizeOf(this.header.GetType());

            if (vr.textureType == ETextureType.OpenGL)
            {
                if (this.glTextureId != 0)
                    trackedCamera.ReleaseVideoStreamTextureGL(this.videostream.handle, this.glTextureId);

                if (trackedCamera.GetVideoStreamTextureGL(this.videostream.handle, this.frameType, ref this.glTextureId, ref this.header, headerSize) != EVRTrackedCameraError.None)
                    return;

                nativeTex = (System.IntPtr)this.glTextureId;
            }
            else if (vr.textureType == ETextureType.DirectX)
            {
                if (trackedCamera.GetVideoStreamTextureD3D11(this.videostream.handle, this.frameType, deviceTexture.GetNativeTexturePtr(), ref nativeTex, ref this.header, headerSize) != EVRTrackedCameraError.None)
                    return;
            }

            if (this._texture == null)
            {
                this._texture = Texture2D.CreateExternalTexture((int)this.header.nWidth, (int)this.header.nHeight, TextureFormat.RGBA32, false, false, nativeTex);

                uint width = 0, height = 0;
                var frameBounds = new VRTextureBounds_t();
                if (trackedCamera.GetVideoStreamTextureSize(this.deviceIndex, this.frameType, ref frameBounds, ref width, ref height) == EVRTrackedCameraError.None)
                {
                    // Account for textures being upside-down in Unity.
                    frameBounds.vMin = 1.0f - frameBounds.vMin;
                    frameBounds.vMax = 1.0f - frameBounds.vMax;
                    this.frameBounds = frameBounds;
                }
            }
            else
            {
                this._texture.UpdateExternalTexture(nativeTex);
            }
        }

        private uint glTextureId;
        private VideoStream videostream;
        private CameraVideoStreamFrameHeader_t header;
    }

    #region Top level accessors.

    public static VideoStreamTexture Distorted(int deviceIndex = (int)OpenVR.k_unTrackedDeviceIndex_Hmd)
    {
        if (distorted == null)
            distorted = new VideoStreamTexture[OpenVR.k_unMaxTrackedDeviceCount];
        if (distorted[deviceIndex] == null)
            distorted[deviceIndex] = new VideoStreamTexture((uint)deviceIndex, false);
        return distorted[deviceIndex];
    }

    public static VideoStreamTexture Undistorted(int deviceIndex = (int)OpenVR.k_unTrackedDeviceIndex_Hmd)
    {
        if (undistorted == null)
            undistorted = new VideoStreamTexture[OpenVR.k_unMaxTrackedDeviceCount];
        if (undistorted[deviceIndex] == null)
            undistorted[deviceIndex] = new VideoStreamTexture((uint)deviceIndex, true);
        return undistorted[deviceIndex];
    }

    public static VideoStreamTexture Source(bool undistorted, int deviceIndex = (int)OpenVR.k_unTrackedDeviceIndex_Hmd)
    {
        return undistorted ? Undistorted(deviceIndex) : Distorted(deviceIndex);
    }

    private static VideoStreamTexture[] distorted, undistorted;

    #endregion Top level accessors.

    #region Internal class to manage lifetime of video streams (per device).

    private class VideoStream
    {
        public VideoStream(uint deviceIndex)
        {
            this.deviceIndex = deviceIndex;
            var trackedCamera = OpenVR.TrackedCamera;
            if (trackedCamera != null)
                trackedCamera.HasCamera(deviceIndex, ref this._hasCamera);
        }

        public uint deviceIndex { get; private set; }

        private ulong _handle;
        public ulong handle { get { return this._handle; } }

        private bool _hasCamera;
        public bool hasCamera { get { return this._hasCamera; } }

        private ulong refCount;

        public ulong Acquire()
        {
            if (this._handle == 0 && this.hasCamera)
            {
                var trackedCamera = OpenVR.TrackedCamera;
                if (trackedCamera != null)
                    trackedCamera.AcquireVideoStreamingService(this.deviceIndex, ref this._handle);
            }
            return ++this.refCount;
        }

        public ulong Release()
        {
            if (this.refCount > 0 && --this.refCount == 0 && this._handle != 0)
            {
                var trackedCamera = OpenVR.TrackedCamera;
                if (trackedCamera != null)
                    trackedCamera.ReleaseVideoStreamingService(this._handle);
                this._handle = 0;
            }
            return this.refCount;
        }
    }

    private static VideoStream Stream(uint deviceIndex)
    {
        if (videostreams == null)
            videostreams = new VideoStream[OpenVR.k_unMaxTrackedDeviceCount];
        if (videostreams[deviceIndex] == null)
            videostreams[deviceIndex] = new VideoStream(deviceIndex);
        return videostreams[deviceIndex];
    }

    private static VideoStream[] videostreams;

    #endregion Internal class to manage lifetime of video streams (per device).
}