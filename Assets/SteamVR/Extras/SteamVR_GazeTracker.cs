﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
using UnityEngine;

public struct GazeEventArgs
{
    public float distance;
}

public delegate void GazeEventHandler(object sender, GazeEventArgs e);

public class SteamVR_GazeTracker : MonoBehaviour
{
    public bool isInGaze = false;

    public event GazeEventHandler GazeOn;

    public event GazeEventHandler GazeOff;

    public float gazeInCutoff = 0.15f;
    public float gazeOutCutoff = 0.4f;

    // Contains a HMD tracked object that we can use to find the user's gaze
    private Transform hmdTrackedObject = null;

    // Use this for initialization
    private void Start()
    {
    }

    public virtual void OnGazeOn(GazeEventArgs e)
    {
        if (GazeOn != null)
            GazeOn(this, e);
    }

    public virtual void OnGazeOff(GazeEventArgs e)
    {
        if (GazeOff != null)
            GazeOff(this, e);
    }

    // Update is called once per frame
    private void Update()
    {
        // If we haven't set up hmdTrackedObject find what the user is looking at
        if (this.hmdTrackedObject == null)
        {
            SteamVR_TrackedObject[] trackedObjects = FindObjectsOfType<SteamVR_TrackedObject>();
            foreach (SteamVR_TrackedObject tracked in trackedObjects)
            {
                if (tracked.index == SteamVR_TrackedObject.EIndex.Hmd)
                {
                    this.hmdTrackedObject = tracked.transform;
                    break;
                }
            }
        }

        if (this.hmdTrackedObject)
        {
            Ray r = new Ray(this.hmdTrackedObject.position, this.hmdTrackedObject.forward);
            Plane p = new Plane(this.hmdTrackedObject.forward, this.transform.position);

            float enter = 0.0f;
            if (p.Raycast(r, out enter))
            {
                Vector3 intersect = this.hmdTrackedObject.position + this.hmdTrackedObject.forward * enter;
                float dist = Vector3.Distance(intersect, this.transform.position);
                //Debug.Log("Gaze dist = " + dist);
                if (dist < this.gazeInCutoff && !this.isInGaze)
                {
                    this.isInGaze = true;
                    GazeEventArgs e;
                    e.distance = dist;
                    OnGazeOn(e);
                }
                else if (dist >= this.gazeOutCutoff && this.isInGaze)
                {
                    this.isInGaze = false;
                    GazeEventArgs e;
                    e.distance = dist;
                    OnGazeOff(e);
                }
            }
        }
    }
}