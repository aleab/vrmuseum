﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
using System;
using System.Runtime.InteropServices;
using UnityEngine;
using Valve.VR;

public class ControllerEventArgs : EventArgs
{
    public uint controllerIndex;
    public uint flags;
    public float padX, padY;

    public ControllerEventArgs(uint controllerIndex, uint flags, float padX, float padY)
    {
        this.controllerIndex = controllerIndex;
        this.flags = flags;
        this.padX = padX;
        this.padY = padY;
    }
}

public enum ViveControllerComponents
{
    Base,
    Body,
    MenuButton,
    LeftGrip,
    RightGrip,
    SystemButton,
    Tip,
    Pad,
    Trigger
}

public class SteamVR_TrackedController : MonoBehaviour
{
    #region Private fields

    [SerializeField]
    private uint _controllerIndex;

    [SerializeField]
    private VRControllerState_t _controllerState;

    [SerializeField]
    private bool _isTriggerPressed = false;

    [SerializeField]
    private bool _isSteamPressed = false;

    [SerializeField]
    private bool _isMenuPressed = false;

    [SerializeField]
    private bool _isPadPressed = false;

    [SerializeField]
    private bool _isPadTouched = false;

    [SerializeField]
    private bool _isGripped = false;

    #endregion Private fields

    #region Public properties

    public uint ControllerIndex
    {
        get { return this._controllerIndex; }
        set { this._controllerIndex = value; }
    }

    public VRControllerState_t ControllerState
    {
        get { return this._controllerState; }
        private set { this._controllerState = value; }
    }

    public bool IsTriggerPressed
    {
        get { return this._isTriggerPressed; }
        private set { this._isTriggerPressed = value; }
    }

    public bool IsSteamPressed
    {
        get { return this._isSteamPressed; }
        private set { this._isSteamPressed = value; }
    }

    public bool IsMenuPressed
    {
        get { return this._isMenuPressed; }
        private set { this._isMenuPressed = value; }
    }

    public bool IsPadPressed
    {
        get { return this._isPadPressed; }
        private set { this._isPadPressed = value; }
    }

    public bool IsPadTouched
    {
        get { return this._isPadTouched; }
        private set { this._isPadTouched = value; }
    }

    public bool IsGripped
    {
        get { return this._isGripped; }
        private set { this._isGripped = value; }
    }

    #endregion Public properties

    #region Events

    public event EventHandler<ControllerEventArgs> MenuButtonClicked;

    public event EventHandler<ControllerEventArgs> MenuButtonUnclicked;

    public event EventHandler<ControllerEventArgs> TriggerClicked;

    public event EventHandler<ControllerEventArgs> TriggerUnclicked;

    public event EventHandler<ControllerEventArgs> SteamClicked;

    public event EventHandler<ControllerEventArgs> SteamUnlicked;

    public event EventHandler<ControllerEventArgs> PadClicked;

    public event EventHandler<ControllerEventArgs> PadUnclicked;

    public event EventHandler<ControllerEventArgs> PadTouched;

    public event EventHandler<ControllerEventArgs> PadUntouched;

    public event EventHandler<ControllerEventArgs> Gripped;

    public event EventHandler<ControllerEventArgs> Ungripped;

    #endregion Events

    private ControllerEventArgs CurrentClickedEventArgs
    {
        get
        {
            uint controllerIndex = this.ControllerIndex;
            uint flags = (uint)this.ControllerState.ulButtonPressed;
            float padX = this.ControllerState.rAxis0.x;
            float padY = this.ControllerState.rAxis0.y;
            return new ControllerEventArgs(controllerIndex, flags, padX, padY);
        }
    }

    private void Start()
    {
        if (this.GetComponent<SteamVR_TrackedObject>() == null)
            this.gameObject.AddComponent<SteamVR_TrackedObject>();

        if (this.ControllerIndex != 0)
        {
            this.GetComponent<SteamVR_TrackedObject>().index = (SteamVR_TrackedObject.EIndex)this.ControllerIndex;
            if (this.GetComponent<SteamVR_RenderModel>() != null)
                this.GetComponent<SteamVR_RenderModel>().index = (SteamVR_TrackedObject.EIndex)this.ControllerIndex;
        }
        else
            this.ControllerIndex = (uint)this.GetComponent<SteamVR_TrackedObject>().index;
    }

    private void Update()
    {
        var system = OpenVR.System;
        if (system != null && system.GetControllerState(this.ControllerIndex, ref this._controllerState, (uint)Marshal.SizeOf(typeof(VRControllerState_t))))
        {
            // Trigger
            ulong trigger = this.ControllerState.ulButtonPressed & SteamVR_Controller.ButtonMask.Trigger;
            if (trigger > 0L && !this.IsTriggerPressed)
            {
                this.IsTriggerPressed = true;
                this.OnTriggerClicked(this.CurrentClickedEventArgs);
            }
            else if (trigger == 0L && this.IsTriggerPressed)
            {
                this.IsTriggerPressed = false;
                this.OnTriggerUnclicked(this.CurrentClickedEventArgs);
            }

            // Grip
            ulong grip = this.ControllerState.ulButtonPressed & SteamVR_Controller.ButtonMask.Grip;
            if (grip > 0L && !this.IsGripped)
            {
                this.IsGripped = true;
                this.OnGripped(this.CurrentClickedEventArgs);
            }
            else if (grip == 0L && this.IsGripped)
            {
                this.IsGripped = false;
                this.OnUngripped(this.CurrentClickedEventArgs);
            }

            // Pad (press)
            ulong pad = this.ControllerState.ulButtonPressed & SteamVR_Controller.ButtonMask.Touchpad;
            if (pad > 0L && !this.IsPadPressed)
            {
                this.IsPadPressed = true;
                this.OnPadClicked(this.CurrentClickedEventArgs);
            }
            else if (pad == 0L && this.IsPadPressed)
            {
                this.IsPadPressed = false;
                this.OnPadUnclicked(this.CurrentClickedEventArgs);
            }

            // Pad (touch)
            pad = this.ControllerState.ulButtonTouched & SteamVR_Controller.ButtonMask.Touchpad;
            if (pad > 0L && !this.IsPadTouched)
            {
                this.IsPadTouched = true;
                this.OnPadTouched(this.CurrentClickedEventArgs);
            }
            else if (pad == 0L && this.IsPadTouched)
            {
                this.IsPadTouched = false;
                this.OnPadUntouched(this.CurrentClickedEventArgs);
            }

            // Menu
            ulong menu = this.ControllerState.ulButtonPressed & SteamVR_Controller.ButtonMask.ApplicationMenu;
            if (menu > 0L && !this.IsMenuPressed)
            {
                this.IsMenuPressed = true;
                this.OnMenuClicked(this.CurrentClickedEventArgs);
            }
            else if (menu == 0L && this.IsMenuPressed)
            {
                this.IsMenuPressed = false;
                this.OnMenuUnclicked(this.CurrentClickedEventArgs);
            }

            // System
            ulong steam = this.ControllerState.ulButtonPressed & SteamVR_Controller.ButtonMask.System;
            if (steam > 0L && !this.IsSteamPressed)
            {
                this.IsSteamPressed = true;
                this.OnSteamClicked(this.CurrentClickedEventArgs);
            }
            else if (steam == 0L && this.IsSteamPressed)
            {
                this.IsSteamPressed = false;
                this.OnSteamUnclicked(this.CurrentClickedEventArgs);
            }
        }
    }

    public Transform FindComponent(ViveControllerComponents component)
    {
        string componentName = null;

        switch (component)
        {
            case ViveControllerComponents.Base:
                componentName = "base";
                break;

            case ViveControllerComponents.Body:
                componentName = "body";
                break;

            case ViveControllerComponents.MenuButton:
                componentName = "button";
                break;

            case ViveControllerComponents.LeftGrip:
                componentName = "lgrip";
                break;

            case ViveControllerComponents.RightGrip:
                componentName = "rgrip";
                break;

            case ViveControllerComponents.SystemButton:
                componentName = "sys_button";
                break;

            case ViveControllerComponents.Tip:
                componentName = "tip";
                break;

            case ViveControllerComponents.Pad:
                componentName = "trackpad";
                break;

            case ViveControllerComponents.Trigger:
                componentName = "trigger";
                break;
        }
        
        var foundComponent = this.GetComponentInChildren<SteamVR_RenderModel>().FindComponent(componentName);
        if (foundComponent != null)
            foundComponent = foundComponent.FindChild(SteamVR_RenderModel.k_localTransformName);
        return foundComponent;
    }

    #region Event handlers

    protected virtual void OnTriggerClicked(ControllerEventArgs e)
    {
        if (this.TriggerClicked != null)
            this.TriggerClicked(this, e);
    }

    protected virtual void OnTriggerUnclicked(ControllerEventArgs e)
    {
        if (this.TriggerUnclicked != null)
            this.TriggerUnclicked(this, e);
    }

    protected virtual void OnMenuClicked(ControllerEventArgs e)
    {
        if (this.MenuButtonClicked != null)
            this.MenuButtonClicked(this, e);
    }

    protected virtual void OnMenuUnclicked(ControllerEventArgs e)
    {
        if (this.MenuButtonUnclicked != null)
            this.MenuButtonUnclicked(this, e);
    }

    protected virtual void OnSteamClicked(ControllerEventArgs e)
    {
        if (this.SteamClicked != null)
            this.SteamClicked(this, e);
    }

    protected virtual void OnSteamUnclicked(ControllerEventArgs e)
    {
        if (this.SteamUnlicked != null)
            this.SteamUnlicked(this, e);
    }

    protected virtual void OnPadClicked(ControllerEventArgs e)
    {
        if (this.PadClicked != null)
            this.PadClicked(this, e);
    }

    protected virtual void OnPadUnclicked(ControllerEventArgs e)
    {
        if (this.PadUnclicked != null)
            this.PadUnclicked(this, e);
    }

    protected virtual void OnPadTouched(ControllerEventArgs e)
    {
        if (this.PadTouched != null)
            this.PadTouched(this, e);
    }

    protected virtual void OnPadUntouched(ControllerEventArgs e)
    {
        if (this.PadUntouched != null)
            this.PadUntouched(this, e);
    }

    protected virtual void OnGripped(ControllerEventArgs e)
    {
        if (this.Gripped != null)
            this.Gripped(this, e);
    }

    protected virtual void OnUngripped(ControllerEventArgs e)
    {
        if (this.Ungripped != null)
            this.Ungripped(this, e);
    }

    #endregion Event handlers
}