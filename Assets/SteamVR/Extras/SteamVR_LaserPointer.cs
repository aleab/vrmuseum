﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
using UnityEngine;

public struct PointerEventArgs
{
    public uint controllerIndex;
    public uint flags;
    public float distance;
    public Transform target;
}

public delegate void PointerEventHandler(object sender, PointerEventArgs e);

public class SteamVR_LaserPointer : MonoBehaviour
{
    public bool active = true;
    public Color color;
    public float thickness = 0.002f;
    public GameObject holder;
    public GameObject pointer;
    private bool isActive = false;
    public bool addRigidBody = false;
    public Transform reference;

    public event PointerEventHandler PointerIn;

    public event PointerEventHandler PointerOut;

    private Transform previousContact = null;

    // Use this for initialization
    private void Start()
    {
        this.holder = new GameObject();
        this.holder.transform.parent = this.transform;
        this.holder.transform.localPosition = Vector3.zero;
        this.holder.transform.localRotation = Quaternion.identity;

        this.pointer = GameObject.CreatePrimitive(PrimitiveType.Cube);
        this.pointer.transform.parent = this.holder.transform;
        this.pointer.transform.localScale = new Vector3(this.thickness, this.thickness, 100f);
        this.pointer.transform.localPosition = new Vector3(0f, 0f, 50f);
        this.pointer.transform.localRotation = Quaternion.identity;
        BoxCollider collider = this.pointer.GetComponent<BoxCollider>();
        if (this.addRigidBody)
        {
            if (collider)
            {
                collider.isTrigger = true;
            }
            Rigidbody rigidBody = this.pointer.AddComponent<Rigidbody>();
            rigidBody.isKinematic = true;
        }
        else
        {
            if (collider)
            {
                Object.Destroy(collider);
            }
        }
        Material newMaterial = new Material(Shader.Find("Unlit/Color"));
        newMaterial.SetColor("_Color", this.color);
        this.pointer.GetComponent<MeshRenderer>().material = newMaterial;
    }

    public virtual void OnPointerIn(PointerEventArgs e)
    {
        if (PointerIn != null)
            PointerIn(this, e);
    }

    public virtual void OnPointerOut(PointerEventArgs e)
    {
        if (PointerOut != null)
            PointerOut(this, e);
    }

    // Update is called once per frame
    private void Update()
    {
        if (!this.isActive)
        {
            this.isActive = true;
            this.transform.GetChild(0).gameObject.SetActive(true);
        }

        float dist = 100f;

        SteamVR_TrackedController controller = GetComponent<SteamVR_TrackedController>();

        Ray raycast = new Ray(this.transform.position, this.transform.forward);
        RaycastHit hit;
        bool bHit = Physics.Raycast(raycast, out hit);

        if (this.previousContact && this.previousContact != hit.transform)
        {
            PointerEventArgs args = new PointerEventArgs();
            if (controller != null)
            {
                args.controllerIndex = controller.ControllerIndex;
            }
            args.distance = 0f;
            args.flags = 0;
            args.target = this.previousContact;
            OnPointerOut(args);
            this.previousContact = null;
        }
        if (bHit && this.previousContact != hit.transform)
        {
            PointerEventArgs argsIn = new PointerEventArgs();
            if (controller != null)
            {
                argsIn.controllerIndex = controller.ControllerIndex;
            }
            argsIn.distance = hit.distance;
            argsIn.flags = 0;
            argsIn.target = hit.transform;
            OnPointerIn(argsIn);
            this.previousContact = hit.transform;
        }
        if (!bHit)
        {
            this.previousContact = null;
        }
        if (bHit && hit.distance < 100f)
        {
            dist = hit.distance;
        }

        if (controller != null && controller.IsTriggerPressed)
        {
            this.pointer.transform.localScale = new Vector3(this.thickness * 5f, this.thickness * 5f, dist);
        }
        else
        {
            this.pointer.transform.localScale = new Vector3(this.thickness, this.thickness, dist);
        }
        this.pointer.transform.localPosition = new Vector3(0f, 0f, dist / 2f);
    }
}