﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
using UnityEngine;

[RequireComponent(typeof(SteamVR_TrackedObject))]
public class SteamVR_TestThrow : MonoBehaviour
{
    public GameObject prefab;
    public Rigidbody attachPoint;

    private SteamVR_TrackedObject trackedObj;
    private FixedJoint joint;

    private void Awake()
    {
        this.trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    private void FixedUpdate()
    {
        var device = SteamVR_Controller.Input((int)this.trackedObj.index);
        if (this.joint == null && device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger))
        {
            var go = GameObject.Instantiate(this.prefab);
            go.transform.position = this.attachPoint.transform.position;

            this.joint = go.AddComponent<FixedJoint>();
            this.joint.connectedBody = this.attachPoint;
        }
        else if (this.joint != null && device.GetTouchUp(SteamVR_Controller.ButtonMask.Trigger))
        {
            var go = this.joint.gameObject;
            var rigidbody = go.GetComponent<Rigidbody>();
            Object.DestroyImmediate(this.joint);
            this.joint = null;
            Object.Destroy(go, 15.0f);

            // We should probably apply the offset between trackedObj.transform.position
            // and device.transform.pos to insert into the physics sim at the correct
            // location, however, we would then want to predict ahead the visual representation
            // by the same amount we are predicting our render poses.

            var origin = this.trackedObj.origin ? this.trackedObj.origin : this.trackedObj.transform.parent;
            if (origin != null)
            {
                rigidbody.velocity = origin.TransformVector(device.velocity);
                rigidbody.angularVelocity = origin.TransformVector(device.angularVelocity);
            }
            else
            {
                rigidbody.velocity = device.velocity;
                rigidbody.angularVelocity = device.angularVelocity;
            }

            rigidbody.maxAngularVelocity = rigidbody.angularVelocity.magnitude;
        }
    }
}