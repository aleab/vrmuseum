# VRMuseum #

(https://bitbucket.org/aleab/vrmuseum)

A virtual gallery realized in Unity3D for the HTC Vive headset for a university course.

Currently, the application allows you to freely walk and teleport around the room; there are 16 Degas' paintings and for each one of them, there is tablet beside it displaying a description.

Future improvements may concern:

* a better illumination system; current one is *meh*!
* other rooms (with doors and windows!)
* implementation/use of an embedded web browser to read paintings' information directly from a museum's website (the MET Museum for instance); see also: [*Chromium Embedded Framework*](https://bitbucket.org/chromiumembedded/cef), [*CefGlue*](https://bitbucket.org/xilium/xilium.cefglue/wiki/Home) and my (Alessandro AB) own attempt at it: [*cef-unity-sample*](https://github.com/aleab/cef-unity-sample).


### Who do I talk to? ###

* Alessandro Attard Barbini: `alessandro.attardbarbini [at] gmail.com`
* Leonardo Nucciarelli: `leo.nucciarelli [at] gmail.com`